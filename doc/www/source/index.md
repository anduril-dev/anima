<p align="center"><img src="img/anima-logo.png"/></p>

Anima is an image analysis workflow platfrom built on top of [Anduril](https://anduril.org).

Anima acts as a superplatform for the existing imaging applications, and can run components and software written in Java, Bash (i.e. any binary), Perl, Python, R, or Matlab. In addition Anima provides API to the batch environments of Fiji, CellProfiler and ImageMagick.

## Citing

Please cite us if using Anima:

* Anima: Modular workflow system for comprehensive image data analysis. Ville Rantanen, Miko Valori and Sampsa Hautaniemi, Front. Bioeng. Biotechnol. doi: 10.3389/fbioe.2014.00025
* [Publication at Frontiers](https://journal.frontiersin.org/Journal/10.3389/fbioe.2014.00025/abstract)
* [BibTeX entry](anima_2014.bib)

## Example

Visualization of an analysis measuring the intensity profile of a protein marker on the blue line (nucleus-to-nucleus):

<p align="center"><img src="img/example.png"/></p>

*Anima* is a Latin word, describing breath, wind, air, the breath of life, vital principle, and soul. 
Read more at [Wikipedia](https://en.wikipedia.org/wiki/On_the_Soul). In addition, it's an abbreviation from ANduril IMage Analysis.


