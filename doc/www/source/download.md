# Download

Anima is built on [Anduril](https://anduril.org/), which works on 
Linux. You may test an installation with 
[Docker](https://hub.docker.com/u/anduril/full). Alternatively, you 
can install natively.

Anima [source code](https://bitbucket.org/anduril-dev/anima/) is hosted at [Bitbucket](https://bitbucket.org/).

More detailed installation instructions: 
[Anduril User Guide](https://anduril.org/userguide/install/install/).

## Docker

`docker run -ti --rm anduril/full anduril -h`

This prints the help message of the Anduril command line tool. Next 
step: [Hello world](https://anduril.org/userguide/workflows/hello-world/).

## Native installation: Ubuntu

*Install time: 30-60 min*

```sh
sudo apt-get install ant git default-jdk python r-base-dev
export ANDURIL_HOME=~/anduril
git clone https://bitbucket.org/anduril-dev/anduril --branch stable "$ANDURIL_HOME"
export PATH="$ANDURIL_HOME/bin:$PATH"
anduril build
anduril install tools anima
```

