# Documentation

## Essentials

- [Anduril User Guide](https://anduril.org/userguide/)
- [Anduril Component documentation](https://www.anduril.org/anduril/bundles/all/doc) (for all bundles)
- [Anima Component documentation](https://www.anduril.org/anduril/bundles/anima/doc) (just Anima bundle)

## Tutorials

* Example analysis pipeline:
  [Analysis.scala](https://bitbucket.org/anduril-dev/anima/src/anduril2/doc/tutorial/analysis.scala)
* Example analysis pipeline using only Free OpenSource Software (FOSS) tools: 
  [Analysis_FOSS.scala](https://bitbucket.org/anduril-dev/anima/src/anduril2/doc/tutorial/analysis_FOSS.scala)

## Anima presentation


<p align="center"><img src="../img/presentation.gif"></p>

[Download](https://bitbucket.org/anduril-dev/anima/raw/anduril2/doc/presentation/introduction.md) the presentation source



