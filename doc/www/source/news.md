# News

Most of the changes are written in the [Change log](https://bitbucket.org/anduril-dev/anima/src/anduril2/doc/ChangeLog.txt).

List of [Publications](https://bitbucket.org/anduril-dev/anima/wiki/Publications) with results analysed with Anima.


## 2016/06/07

Anima is now ready to be used with Anduril2. Anima for Anduril1.x components are not developed further. All improvements only in Anduril2.x.

## 2015/04/24

[A Doctoral Dissertation](https://helda.helsinki.fi/handle/10138/153857) concerning software intergration and Anima.

## 2014/07/16

Anima got accepted in [Frontiers in bioengineering and biotechnology](https://journal.frontiersin.org/Journal/10.3389/fbioe.2014.00025/abstract).
Here is how to [cite us](anima_2014.bib)


