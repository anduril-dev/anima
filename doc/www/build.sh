#!/bin/bash

cd $( dirname $0 )
# Generate graphics

convert ../logo/logoSmall.png  \
      \( -clone 0 -resize 16x16 \) \
      \( -clone 0 -resize 32x32 \) \
      \( -clone 0 -resize 48x48 \) \
      \( -clone 0 -resize 64x64 \) \
      -delete 0 source/img/favicon.ico
      
rsvg-convert -f png -w 200 -o source/img/anima-logo.png ../logo/logo.svg      

# build site

mkdocs build -c
