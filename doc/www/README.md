# Readme


This is the contents of anduril.org/anima site. Online site is updated automatically.

# Requirements

* python package mkdocs

Install with `pip install mkdocs`.
You may encounter trouble with pyyaml libraries, install with
`sudo apt-get install libyaml-cpp-dev`

* Some markdown extensions are separate packages, like

`pip install python-markdown-math`

# Show the website

run `mkdocs serve`  and go to the link that is shown

# Other notes

Link checker:

`wget --spider -r -nd -nv -H -l 1 -w 2 -o run1.log  http://your_server_ip/`

