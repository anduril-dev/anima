Presentation is made for the markslider engine, found in bitbucket.org/Moonq/tools/
# Introduction to Anima

${M}       ▀██████░███░▄          
${M}     ${bk}   ▀██████░███░██████▄        ${Z}
${M}     ${bk}    ███████░██░█████░░${m}${bk}▄       ${Z}
${M}     ${bk}     ███████░░░███░░${m}${bk}████      ${Z}
${M}     ${bk}     ▀█████████░█░${m}${bk}██████      ${Z}
${M}     ${bk}      ██████████░${m}${bk}███████      ${Z}
${M}     ${bk}      ███████░░${m}${bk}█░████▀        ${Z}
${M}     ${bk}      ▀████░░${m}${bk}████░██▀         ${Z}
${M}     ${bk}       ▀██░${m}${bk}███████░▀${y}${bk}▄         ${Z}
${m}     ${bk}             ▀▀▀▀ ${y}${bk} ▀█         ${Z}
${y}     ${bk}                     ▀▄       ${Z}
${y}     ${bk}                       █      ${Z}
${W}     ${bk}    ▄▀▄  █▄ █ █ █▄ ▄█  ▄▀▄ ${y}${bk}  ▄${y}▀   
${W}     ${bk}   █ ▀▀█ █ ▀█ █ █ ▀ █ █▀▀ █ ${y}${bk}█ ${Z}
${y}     ${bk}                           █  ${Z}
${y}     ${bk}                         ▄▀   ${Z}
${y}     ${bk}                        █     ${Z}
${y}     ${bk}                        █     ${Z}
${y}                            ▀  


== Image Analysis platform built on top ==
== of pipeline workflow engine Anduril  ==
== ${W}     INTENDED FOR DEVELOPERS!    ${Z}    ==
        http://anduril.org/anima

# Why a new platform (for the developers)

* Increase Usability as explained in
  [A call for bioimaging software usability](http://dx.doi.org/10.1038/nmeth.2073)

* API for several languages directly
* Software integration: reuse code
    * No reimplementation errors
      due to using original implementation
* API forces to document inputs, outputs and parameters
    
* Published with source code, 3rd party involvement possible
    * https://bitbucket.org/anduril-dev/anima/
* Automated testing of components
    * Run at our concurrent testing site
    * "anduril test" command

# Integration

## Anima/Anduril runs components as separate processes      

* Automated parallelization/cluster run               
* Fast reruns by saving all intermediate steps
    * Rapid development                               
* Components in any language                          
* Direct APIs for: Fiji, CellProfiler, Matlab, Python,
                   ImageMagick, Bash, Perl, Java, R   
* Basically any scriptable environment

## Ease of analysis

* Integrate analysis with other fields  
  Statistics, Machine Learning, Visualization.. etc 
* Abstracts algorithms and methods from analysis flow 
* Does not reinvent the wheel:  
    * Standard image formats, CSV files for data, 
      PDF, HTML+js for complex visualization   
    * Easy to include in publications, web pages 


# Typical use

* Develop new algorithm in your favorite language
* Use existing tools already in any platform
* Flow the data with Anima

* Reuse parts of scripts later!

# Example pipeline

```#!/usr/bin/env anduril
import anduril.builtin._
import anduril.tools._
import anduril.anima._
import org.anduril.runtime._

object Tutorial {
// refer to a directory with images, names prefixed either "case" or "ctrl"
  val source_images=INPUT(path="folder")
// Extract 3rd channel as grayscale PNG
  val gray_dapi=BFConvert(source_images, switches="-channel 0", gray=true)
// Use GraphCut algorithm to create nucleus mask
  val cells=SegmentGraphCut(gray_dapi,minScale=2,maxScale=10)
// Read morphological features from mask
  val features=MorphologyFeatures(cells.mask)
// Split morphological data table to 2 CSV files, by
// 4 first characters of the image file name
  val split_data=CSVSplit(features.out, labelCol="File",
                       regexp="(....)")
// Here, we assume that the 4 first characters were either
// 'ctrl'  or 'case'. Then calculate the t-test on the nucleus areas.
  val t_test=StatisticalTest(matrix=split_data.out("ctrl").force(),
                       matrix2=split_data.out("case").force(),
                       byRow=false,
                       referenceColumns="Area",
                       targetColumns="Area")
// Create a visualization of segmentation. Note that this component is
// independent of many previous ones, and would be run in parallel.
  val visualization=MergeImage(in=makeArray(gray_dapi.out, cells.perimeter), 
                              colors="B,Y")
// Create an HTML web gallery of the visualizations
  val gallery=ImageGallery(inRoot=visualization)
}
```

# Example with C. Elegans

* BBBC 10, benchmark data set of 100 images,
  mostly dead / alive worms in an image.
  Annotation at image level.
* Extract features
* Calculate Length / end-to-end distance ratio
* Create classifier
* 6 minutes running time on 2-core laptop
* Pipeline in the supplementary of 
  http://journal.frontiersin.org/Journal/10.3389/fbioe.2014.00025/

# Anima Citing
${M}       ▀██████░███░▄          
${M}     ${bk}   ▀██████░███░██████▄        ${Z}
${M}     ${bk}    ███████░██░█████░░${m}${bk}▄       ${Z}
${M}     ${bk}     ███████░░░███░░${m}${bk}████      ${Z}
${M}     ${bk}     ▀█████████░█░${m}${bk}██████      ${Z}
${M}     ${bk}      ██████████░${m}${bk}███████      ${Z}
${M}     ${bk}      ███████░░${m}${bk}█░████▀        ${Z}
${M}     ${bk}      ▀████░░${m}${bk}████░██▀         ${Z}
${M}     ${bk}       ▀██░${m}${bk}███████░▀${y}${bk}▄         ${Z}
${m}     ${bk}             ▀▀▀▀ ${y}${bk} ▀█         ${Z}
${y}     ${bk}                     ▀▄       ${Z}
${y}     ${bk}                       █      ${Z}
${W}     ${bk}    ▄▀▄  █▄ █ █ █▄ ▄█  ▄▀▄ ${y}${bk}  ▄${y}▀   
${W}     ${bk}   █ ▀▀█ █ ▀█ █ █ ▀ █ █▀▀ █ ${y}${bk}█ ${Z}
${y}     ${bk}                           █  ${Z}
${y}     ${bk}                         ▄▀   ${Z}
${y}     ${bk}                        █     ${Z}
${y}     ${bk}                        █     ${Z}
${y}                            ▀  

>  http://journal.frontiersin.org/Journal/10.3389/fbioe.2014.00025/
>  Anima: Modular workflow system for comprehensive image data analysis. 
>  Ville Rantanen, Miko Valori and Sampsa Hautaniemi 
>  Front. Bioeng. Biotechnol. doi: 10.3389/fbioe.2014.00025

