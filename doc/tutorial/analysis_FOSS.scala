#!/usr/bin/env anduril
import anduril.builtin._
import anduril.tools._
import anduril.anima._
import org.anduril.runtime._

object Tutorial {
/*  Tutorial for simple image analysis 
 Author: Ville.Rantanen@helsinki.fi
 */

/* Read a directory of multi color images 
 (may be png,tif or other microscope manufacturer 
 specific format, preferably raw data */
  //val images=INPUT(path="images")
  // We can also download the images from internet
  val images=QuickBash(script="mkdir $out && cd $out && wget -O- http://anduril.org/pub/anduril_static/component_data/Anima/tutorial/tutorial_images.tgz | tar xvz")

/* Extract the single gray scale images from the multi color source.
  */
  val gray = BFConvert(images, name="%o_%w", png=true, gray=true)
  val blue = FolderCleaner(gray, filePattern=".*_DAPI.*").out.force()
  val green= FolderCleaner(gray, filePattern=".*_Alexa 488.*",rename="name.replace('_Alexa 488','')").out.force()
  val red  = FolderCleaner(gray, filePattern=".*_Alexa 568.*",rename="name.replace('_Alexa 568','')").out.force()

  /* Segmentation based on the blue channel */
  val blue_preprocess = ImageMagick(in1=blue, command="@in1@ -auto-level @out@")
  val nuclei = SegmentGraphCut(in     = blue_preprocess,
                    minScale        = 4.0,
                    maxScale        = 6.0)

/* Extract the morphological features and red channel intensity 
 features and join them as single file */
  val red_ints=FijiFeatures(mask=nuclei.mask,
                            images=red,
                            fourConnected=true,
                            minArea=50)
  val green_ints=FijiFeatures(mask=nuclei.mask,
                              images=green,
                              fourConnected=true,
                              minArea=50)


/* Create a visualization to confirm the success of segmentation 
  and show the erosion size for margination analysis. Use all 
  extracted gray scale images and the segmented mask. */
  val visualization=ImageMagick(in=
                        makeArray(red,green,blue,nuclei.perimeter),
                command=" -compose Plus \\( @a1@ -auto-level +level-colors ,Red \\) "+
                        " \\( @a2@ -auto-level +level-colors ,Green \\) -composite "+
                        " \\( @a3@ -auto-level +level-colors ,Blue \\) -composite "+
                        " \\( @a4@ -auto-level +level-colors ,White \\) -composite "+
                        "@out@") 


/* Create a brief summary, averaging the features from all the cells per image.
 Write a single line for each image. */
  val green_summary=CSVSummary(green_ints,clusterCol="File")
/* Create a plot showing differences between images */
  val green_intensities_by_file=Labels2Columns(green_ints,useValueName=false,
                                              label="File",values="Mean")
  val red_intensities_by_file=Labels2Columns(red_ints,useValueName=false,
                                            label="File",values="Mean")
  val plot=Plot2D(x=green_intensities_by_file, y=red_intensities_by_file,
                  title="Green vs Red intensities / image",
                  xTransformation="log(x)",yTransformation="log(y)",
                  imageType="single", legendPosition="topright")

/* Copy important results to output folder */
  OUTPUT(green_ints)
  OUTPUT(green_summary)
  OUTPUT(visualization)
  OUTPUT(plot)

}
