#!/usr/bin/env anduril
import anduril.builtin._
import anduril.tools._
import anduril.anima._
import org.anduril.runtime._

object Tutorial {
/*  Tutorial for simple image analysis 
 Author: Ville.Rantanen@helsinki.fi
 */

/* Read a directory of multi color images 
 (may be png,tif or other microscope manufacturer 
 specific format, preferably raw data */
  //val images=INPUT(path="images")
  // We can also download the images from internet
  val images=QuickBash(script="mkdir $out && cd $out && wget -O- http://anduril.org/pub/anduril_static/component_data/Anima/tutorial/tutorial_images.tgz | tar xvz")

/* Extract the single gray scale images from the multi color source.
  */
  val gray = BFConvert(images, name="%o_C%c_%w", png=true, gray=true)
  val blue = FolderCleaner(gray, filePattern=".*_DAPI.*").out.force()
  val green= FolderCleaner(gray, filePattern=".*_Alexa 488.*").out.force()
  val red  = FolderCleaner(gray, filePattern=".*_Alexa 568.*").out.force()

  /* Segmentation based on the blue channel */
  val blue_preprocess = ImageMagick(in1=blue, command="@in1@ -auto-level @out@")
  val nuclei = SegmentGraphCut(in     = blue_preprocess,
                    minScale        = 4.0,
                    maxScale        = 6.0)

/* Extract the morphological features and red channel intensity 
 features and join them as single file */
  val morph=MorphologyFeatures(mask=nuclei.mask,
                               names=images)
  val red_ints=IntensityFeatures(mask=nuclei.mask,  // object data as masks.
                                 images=red, // intensity data for measurements
                                 names=images, // File name in the result list is based on these filenames
                                 prefix="Red")
  val green_ints=IntensityFeatures(mask=nuclei.mask,
                                   images=green, 
                                   names=images, 
                                   prefix="Green")
  val all_features=CSVJoin(morph.out,red_ints.out,green_ints.out,keyColumnNames="RowId,RedRowId,GreenRowId")
  val features=CSVFilter(all_features, 
                includeColumns="RowId,File,Area,GreenMean,RedMean")

/* Create a visualization to confirm the success of segmentation 
 and show the erosion size for margination analysis. Use all 
 extracted gray scale images and the segmented mask. */
  val visualization=MergeImage(in=makeArray(red,green,blue,nuclei.perimeter),
                                  colors="620,510,430,W", stretch=true)
                     // red signal with 620nm dominant wavelength color
                     // green data with 510nm color
                     // blue signal with 430nm color
                     // object mask perimeter lines with white color


/* Create a brief summary, averaging the features from all the cells per image.
 Write a single line for each image. */
  val summary=CSVSummary(features,clusterCol="File")
/* Create a plot showing differences between images */
  val green_intensities_by_file=Labels2Columns(features,useValueName=false,
                                              label="File",values="GreenMean")
  val red_intensities_by_file=Labels2Columns(features,useValueName=false,
                                            label="File",values="RedMean")
  val plot=Plot2D(x=green_intensities_by_file, y=red_intensities_by_file,
                  title="Green vs Red intensities / image",
                  xTransformation="log(x)",yTransformation="log(y)",
                  imageType="single", legendPosition="topright")

/* Copy important results to output folder */
  OUTPUT(features)
  OUTPUT(summary)
  OUTPUT(visualization)
  OUTPUT(plot)

}
