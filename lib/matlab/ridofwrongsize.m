function mask=ridofwrongsize(mask,mina,maxa)
% Returns a binary mask, with objects that fit area filters
% MASK=RIDOFWRONGSIZE(MASK,MINA,MAXA)
%    MASK = binary image
%    MINA = minimum accepted area
%    MAXA = maximum accepted area
% If values between 0-1 they are considered as ratio to mask area

    if and(mina==0,maxa==0)
        return
    end
    masklab=bwlabel(mask,4);
    props=regionprops(masklab,'Area');
    if maxa==0
        maxarea=1*numel(mask);
    elseif maxa<1
        maxarea=maxa*numel(mask);
    else
        maxarea=maxa;
    end
    if mina<1
        mina=mina*numel(mask);
    end
    Areas=[props.Area];
    goodareas=intersect(find(Areas>mina), find(Areas<maxarea));
    mask=ismember(masklab,goodareas);
end
