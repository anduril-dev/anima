#!/bin/bash

if [ -z $( which matlab ) ]
then 
   mkdir -p "$2"
   echo "Matlab was not available during documentation generation." > "$2"/index.html
else 
    matlab -nosplash -nojvm -nodesktop -r "addpath('matlab/m2html'); m2html('mFiles','"$1"','recursive','off','htmlDir','"$2"','source','off'); exit;"
fi
