function out=writeonimage(img, string, x,y, scale,scalemethod, charset)
% Returns a logical of size IMG, with string written at the coordinates X Y. can be added to the original image
% for visualization of data on image.
% OUT=WRITEONIMAGE(IMG, STRING, X,Y,SCALE,SCALEMETHOD)
%    IMG = image to write on
%    STRING = string to write
%    Y,X = coordinate points
%    SCALE = scaling factor, must be > 1
%    SCALEMETHOD = one of imresize methods (nearest,bilinear,bicubic ...)
%    CHARSET = a function that returns a bitmap when given a character. ( @getcalculatorchar or @getphonechar )

if (nargin<5)
   scale=1;
end
if (nargin<6)
   scalemethod='nearest';
end
if (nargin<7)
    charset=@getcalculatorchar;
end
fontsize=charset('size');
fonth=fontsize(1);
fontw=fontsize(2);
out=zeros(size(img));
[h w]=size(out);
x=round(x);
y=round(y);
n=numel(string);

x=max(x,1);
y=max(y,1);

x1=round(n*fontw*scale)+x;
y1=round(fonth*scale)+y;

pix=logical([]);
if x1>w
    x=x-x1+w;
    x1=w;
end
if y1>h
    y=y-y1+h;
    y1=h;
end

% does not print chars if it doesn't fit.
if x>0 && y>0
    for c=1:size(string,2)
        pix=[pix charset(string(c))];
    end
    if scale~=1
        pix=imresize(double(pix),scale,scalemethod);
    end
    out(y:y1-1, x:x1-1)=pix;
end  % fits inside image
 
