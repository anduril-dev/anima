function imglist=getimagelist(imdir)
% Returns a cell string array of image names from directory IMDIR
% IMGLIST=GETIMAGELIST(IMDIR);
%
% The function only returns "normal" image extensions suitable for Anduril image analysis:
% PNG, TIF, JPG, GIF, BMP
%
% Author: ville.rantanen@helsinki.fi

filelist=struct2cell(dir(imdir));
filelist=filelist(1,:);
imglist=getimages(filelist);
