%Helper function to extract the characters from the image.
% remember to add additional ' to the ' character... :)

im=mat2gray(max(im2double(imread('nokiafont.png')),[],3));
[h w]=size(im);
ascii=32;
im=logical(im2bw(imcomplement(im)));
fid=fopen('nokiafont.txt','w');
for y=1:8:h
for x=1:6:w
    c=im(y:y+7,x:x+5);
    %imshow(c);drawnow;pause(0.001)
    %disp(char(ascii));
    stringout=['        case ''' char(ascii) ''', cout = ['];
    for cy=1:8
        stringout=[stringout num2str(c(cy,:)) '; '];
    end
    stringout=[stringout ']; % ' num2str(ascii) ];
    disp(stringout)
    fprintf(fid,'%s\n',stringout);
    ascii=ascii+1;
end
end
fclose(fid);


