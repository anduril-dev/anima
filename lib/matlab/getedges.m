function [Xbar,Ybar]=getedges(cx,cy,alpha,lx,ly,ellipseprec,width,height,arrow)
% returns pixel locations for ellipses defined by vector parameters
% [Xbar,Ybar]=getedges(cx,cy,alpha,lx,ly,ellipseprec,width,height,arrow)
%     cx = vector of center X coordinates
%     cy = vector of center Y coordinates
%     alpha = vector of ellipse orientations in radians
%     lx = vector of major axis lengths
%     ly = vector of minor axis lengths
%  ellipseprec = how many pixels will be used to draw the ellipse
%     width = width of image
%     height = height of image
%     arrow = boolean for changing the ellipses in to oriented arrows

if nargin<9
   arrow=false;
end

if numel(cx)==0 | numel(cy)==0 | numel(alpha)==0 | numel(lx)==0 | numel(ly)==0
    Xbar=[];
    Ybar=[];
    return
end

lx=max(0,lx);
ly=max(0,ly);

alphas = repmat(alpha,[1 ellipseprec]);
lxs = repmat(lx,[1 ellipseprec]);
if arrow
    arrow=imresize([1 0 1],[1 ellipseprec],'bilinear');
    lxs = lxs.*repmat(arrow,[numel(lx) 1]);
end
lys = repmat(ly, [1 ellipseprec]);

if numel(cx)==0
    Xbar=[];
    Ybar=[];
    return
end
theta = repmat(linspace(0,2*pi,ellipseprec),[numel(cx) 1]);

cxs = repmat(cx,[1 ellipseprec]);
cys = repmat(cy,[1 ellipseprec]);
%theta = theta(ones(size(cx1,1),1),:);
X = cos(theta).*lxs;
Y = sin(theta).*lys;
Xbar = round(cxs+X.*cos(alphas)+Y.*sin(alphas));
Ybar = round(cys+Y.*cos(alphas)-X.*sin(alphas));

Xbar=max(Xbar,1);
Xbar=min(Xbar,width);
Ybar=max(Ybar,1);
Ybar=min(Ybar,height);
