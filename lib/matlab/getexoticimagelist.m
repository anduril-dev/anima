function imglist=getexoticimagelist(imdir)
% Returns a cell string array of image names from directory IMDIR
% IMGLIST=GETEXOTICIMAGELIST(IMDIR);
%
% The function only returns "exotic" image extensions suitable for Anduril image analysis:
%
% Author: ville.rantanen@helsinki.fi

filelist=struct2cell(dir(imdir));
filelist=filelist(1,:);
imglist=filterexotic(filelist);
