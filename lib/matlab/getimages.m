function imglist=getimages(filelist)
% Returns the filtered cell string array containing
% valid image files 'tif$|tiff$|png$|jpg$|jpeg$|gif$|bmp$'
% IMAGELIST=GETIMAGES(FILELIST)
%     FILELIST=Cell array of filenames
%
% Author: ville.rantanen@helsinki.fi

validlist=regexpi(filelist, 'tif$|tiff$|png$|jpg$|jpeg$|gif$|bmp$');
validlist=not(cellfun(@(x) isempty(x),validlist));
imglist=filelist(validlist);
