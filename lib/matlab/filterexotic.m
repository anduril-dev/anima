function imglist=filterexotic(filelist)
% Returns the filtered cell string array containing
% valid exotic image files zvi gif tif tiff png lsm lif jpg jpeg c01 bmp
% IMAGELIST=GETEXOTICIMAGES(FILELIST)
%             FILELIST=Cell array of filenames
%
% Author: ville.rantanen@helsinki.fi

validlist=regexpi(filelist, 'zvi$|gif$|tif$|tiff$|png$|lsm$|lif$|jpg$|jpeg$|c01$|bmp$');
validlist=not(cellfun(@(x) isempty(x),validlist));
imglist=filelist(validlist);
