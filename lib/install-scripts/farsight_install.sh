#!/bin/bash
set -e
cd "$( dirname $0 )"/..

NAME=Farsight
VERSION=0.4.4-Linux
#URL="http://www.farsight-toolkit.org/mw/scripts/download.php?downloadType=Release&operatingSystem=linux-x86_64&downloadFile=Farsight-0.4.4-Linux.tar.gz&submit=Download"
URL="http://anduril.org/pub/anduril_static/component_data/Anima/Farsight-0.4.4-Linux.tar.gz"

[[ -d "${NAME}-${VERSION}" ]] && {
    echo $NAME already installed
    exit 0
}
echo Downloading...
wget -O - $URL | tar zx
chmod -R ugo+rX "${NAME}-${VERSION}"
ln -Tsf "${NAME}-${VERSION}" $NAME
echo $VERSION > $NAME/VERSION


