#!/bin/bash
set -e

NAME=vlfeat
VERSION="0.9.20"
URL="http://www.vlfeat.org/download/vlfeat-0.9.20-bin.tar.gz"
cd "$( dirname $0 )"
[[ -d ../"${NAME}-${VERSION}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O - $URL | tar zx
ln -Tsf "${NAME}-${VERSION}" $NAME
