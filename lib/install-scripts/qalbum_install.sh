#!/bin/bash
set -e

NAME=qalbum
[[ $UID -eq 0 ]] && {
  pip3 install --upgrade  https://bitbucket.org/MoonQ/qalbum/get/master.tar.gz
} || {
  pip3 install --user --upgrade  https://bitbucket.org/MoonQ/qalbum/get/master.tar.gz
}
