#!/bin/bash
set -e

NAME=Fiji.app
VERSION=""
URL="https://downloads.imagej.net/fiji/latest/fiji-linux64.zip"
cd "$( dirname $0 )"/..
[[ -d "${NAME}" ]] && {
    echo "${NAME} already installed"
    # Update
    #cd ${NAME}
    #./ImageJ-linux64 --update update
    #chmod ugo+rX .
    exit 0
}

rm -rf "${NAME}"
wget -O "${NAME}".zip $URL
unzip "${NAME}".zip
chmod ugo+rX "${NAME}"
rm -f "${NAME}".zip

#ln -Tsf "${NAME}-${VERSION}" $NAME

