#!/bin/bash
set -e
function iscmd {
    which "$1" > /dev/null || {
        echo Command: $1 not found >&2
        return 1
    }
}

cd "$( dirname $0 )"/../

NAME=bftools
VERSION=5.3.2
URL="http://downloads.openmicroscopy.org/bio-formats/${VERSION}/artifacts/bftools.zip"

[[ $( cat "$NAME"/VERSION ) = $VERSION ]] && {
    echo $NAME already installed
    exit 0
}
iscmd unzip || exit 1
iscmd wget || exit 1

echo Downloading...
wget -O "$NAME".zip "$URL" 
rm -rf "$NAME"
unzip "$NAME".zip
echo $VERSION > "$NAME"/VERSION
echo Change access rights...
chmod ugo+rX "$NAME"
rm "$NAME".zip


