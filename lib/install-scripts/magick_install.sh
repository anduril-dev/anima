#!/bin/bash
set -ex

NAME=magick
VERSION="7"
URL="http://ftp.acc.umu.se/mirror/imagemagick.org/ftp/ImageMagick.tar.gz"
cd "$( dirname $0 )"
[[ -d ../"${NAME}-${VERSION}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
LIBROOT=$( pwd )
trap "cd $LIBROOT; rm -rf $NAME.source" 0 1 9 15
mkdir -p $NAME.source
cd $NAME.source
wget -O - $URL | tar zx --strip-components=1
./configure --prefix="$LIBROOT/${NAME}-${VERSION}" 
make
rm -rf "$LIBROOT/${NAME}-${VERSION}"
make install
cd "$LIBROOT"
ln -Tsf "${NAME}-${VERSION}" "${NAME}"
rm -r $NAME.source
