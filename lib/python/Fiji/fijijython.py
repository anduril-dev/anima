import component_skeleton.main
from component_skeleton.Array import ArrayReader
import os
import re
from ij import IJ
from fijisave import save_image
import csv
import ij.Prefs
ij.Prefs.blackBackground = True

def get_imagelist(directory):
     """Return the filenames of images in a directory as a list.
     Image formats not known are removed (based on the extension.)
     """
     imglist=[]
     accepted='tif$|jpg$|jpeg$|tiff$|png$|gif$'
     for filename in os.listdir(directory):
          imtype=re.search(accepted,filename.lower())
          if imtype:
               imglist.append(filename)
     imglist.sort()             
     return imglist

def zip_(*arg): # zip that adds padding if inputs differ in length
    return map(None, *arg)

def imageCopy(imgIn):
    ''' Create a copy of an image object '''
    imgOut=imgIn.createImagePlus()
    improc = imgIn.getProcessor().duplicate()
    imgOut.setProcessor("image copy", improc)
    return imgOut

def execute(cf):
    # Read parameters etc
    script = cf.get_parameter('script', 'string')
    if not script:
        scriptfile = cf.get_input('command')
        script = open(scriptfile).read()
    tblfile = cf.get_input('table')
    if tblfile:     
        reader = csv.reader(open(tblfile), delimiter='\t',quotechar='"', quoting=csv.QUOTE_NONNUMERIC)              
        table = list(reader)          
    extension = cf.get_parameter('extension', 'string')
    files = cf.get_parameter('files', 'int')
    param1 = cf.get_parameter('param1', 'string')
    param2 = cf.get_parameter('param2', 'string')
    param3 = cf.get_parameter('param3', 'string')
    param4 = cf.get_parameter('param4', 'string')
    param5 = cf.get_parameter('param5', 'string')
    param6 = cf.get_parameter('param6', 'string')
    param7 = cf.get_parameter('param7', 'string')
    param8 = cf.get_parameter('param8', 'string')
    param9 = cf.get_parameter('param9', 'string')
    param10 = cf.get_parameter('param10', 'string')
    dirout = cf.get_output('out')
    os.mkdir(dirout)
    dirout2 = cf.get_output('out2')
    os.mkdir(dirout2)
    dirtblout = cf.get_output('tableOut')
    os.mkdir(dirtblout)    
    
    # Get listings of images in the input directories
    dir1 = cf.get_input('in1')
    dir2 = cf.get_input('in2')
    dir3 = cf.get_input('in3')
    dir4 = cf.get_input('in4')
    dir5 = cf.get_input('in5')
    l1=[]
    l2=[]
    l3=[]
    l4=[]
    l5=[]
    if dir1: l1 = get_imagelist(dir1)
    if dir2: l2 = get_imagelist(dir2)
    if dir3: l3 = get_imagelist(dir3)
    if dir4: l4 = get_imagelist(dir4)
    if dir5: l5 = get_imagelist(dir5)
    
    fdir1 = cf.get_input('file1')
    if fdir1: file1 = IJ.openImage(os.path.join(fdir1, get_imagelist(fdir1)[0]))
    fdir2 = cf.get_input('file2')
    if fdir2: file2 = IJ.openImage(os.path.join(fdir2, get_imagelist(fdir2)[0]))
    la = []
    if cf.get_input('inArray'):
        adirs = list(ArrayReader(cf, "inArray"))
        afilelists = map(get_imagelist, adirs)
        la = zip_(*afilelists)

    # Iterate over the all the image files in the directory listings 
    nf = range(1, files + 1)
    filenumber=1
    filelistlength=max(len(l1),len(l2),len(l3),len(l4),len(l5),len(nf))
    for f1, f2, f3, f4, f5, fileno, fa in zip_(l1, l2, l3, l4, l5, nf, la):
        # read image files
        out = None
        out2 = None
        tableout = None
        if f1: in1 = IJ.openImage(os.path.join(dir1, f1))
        if f2: in2 = IJ.openImage(os.path.join(dir2, f2))
        if f3: in3 = IJ.openImage(os.path.join(dir3, f3))
        if f4: in4 = IJ.openImage(os.path.join(dir4, f4))
        if f5: in5 = IJ.openImage(os.path.join(dir5, f5))
        if fa: # the component's array input
            ina = []
            for i in range(len(fa)):
                ina.append(IJ.openImage(os.path.join(adirs[i], fa[i])))
        if fa:
            outfilename = os.path.splitext(fa[0])[0]
        if f1:
            outfilename = os.path.splitext(f1)[0]
        if files:
            outfilename = "file%d" % fileno
        tbloutfilepath = os.path.join(dirtblout, outfilename + ".csv")             
        
        # run the script supplied by user
        exec script
        
        # write output
        print 'Writing: ',outfilename+extension,' %i/%i'%(filenumber,filelistlength)
        if out:
            save_image(out, dirout, outfilename, extension)
        if out2:
            save_image(out2, dirout2, outfilename, extension)
        if tableout:
            writer = csv.writer(open(tbloutfilepath,'wb'),delimiter='\t',quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            writer.writerows(tableout)            
        filenumber+=1
    
    return 0
    
    
