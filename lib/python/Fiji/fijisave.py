import os
from ij.io import FileSaver

def save_image(imout, dirout, name, ext):
	"""Save an ImagePlus image in a format determined by ext.
    """
	filename = os.path.join(dirout, name + ext)
	if ext == ".tif" or ext == ".tiff":
		FileSaver(imout).saveAsTiff(filename)
	elif ext == ".zip":
		FileSaver(imout).saveAsZip(filename)
	elif ext == ".gif":
		FileSaver(imout).saveAsGif(filename)
	elif ext == ".jpg" or ext == ".jpeg":
		FileSaver(imout).saveAsJpeg(filename)
	elif ext == ".bmp":
		FileSaver(imout).saveAsBmp(filename)
	elif ext == ".pgm":
		FileSaver(imout).saveAsPgm(filename)
	elif ext == ".png":
		FileSaver(imout).saveAsPng(filename)
	elif ext == ".fits":
		FileSaver(imout).saveAsFits(filename)
	elif ext == ".raw":
		FileSaver(imout).saveAsRaw(filename)
	elif ext == ".txt":
		FileSaver(imout).saveAsText(filename)
	elif ext == ".lut":
		FileSaver(imout).saveAsLut(filename)
	else:
		raise Exception("Unknown file extension")
