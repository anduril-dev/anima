
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
# make the list separator a rowchange
IFS=$'\n' 

indir1=$( getinput in1 )
indir2=$( getinput in2 )
infile1=$( getinput f1 )
infile2=$( getinput f2 )
inarraydirs=( $( getarrayfiles in ) )
outdir=$( getoutput out )
switches=$( getparameter command ) 
switchfile=$( getinput commandFile ) 
outext=$( getparameter extension )
inext=$( getparameter oldExtension ) 
infilter=$( getparameter filter )
magickbin=$( getparameter binary )
tempdir=$( gettempdir )
failmiss=$( getparameter failOnMissing )
files=$( getparameter files )
parallel=$( getmetadata custom_cpu )
[[ -z "$parallel" ]] && parallel=1

CONVERT=$( eval which $magickbin )
VERSION=$( "$CONVERT" -version | grep -i imagemagick )
if [ -z "$VERSION" ]; then
    msg="ImageMagick $magickbin not found. ( $CONVERT )"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
else
    echo "ImageMagick found."
    writelog "$VERSION"
fi

if [ -f "$switchfile" ]
    then cp "$switchfile" "$tempdir"/magicksource.sh
else    
    echo -e "$switches" > "$tempdir"/magicksource.sh
fi

if ( grep "@out@" "$tempdir"/magicksource.sh > /dev/null )
    then echo "@out@ port found"
else
    msg="@out@ not present in the script!"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi

# list files
if [ -d "$input_ina1" ]
then inafiles1=( $( getarrayfiles ina1 ) ) || echo "error reading input ina1" >> "$errorfile"
fi
if [ -d "$input_ina2" ]
then inafiles2=( $( getarrayfiles ina2 ) ) || echo "error reading input ina2" >> "$errorfile"
fi
if [ -d "$indir1" ]
then infiles1=( $( ls -v -1 "$indir1" | grep -i "$infilter" ) ) || echo "error reading input in1" >> "$errorfile"
fi
if [ -d "$indir2" ]
then infiles2=( $( ls -v -1 "$indir2" | grep -i "$infilter" ) ) || echo "error reading input in2" >> "$errorfile"
fi
if [ -d "$infile1" ]
then infilelist1=( $( ls -v -1 "$infile1" | grep -i "$infilter" ) )
fi
if [ -d "$infile2" ]
then infilelist2=( $( ls -v -1 "$infile2" | grep -i "$infilter" ) )
fi

# loop over array inputs and list files
for (( a=0; a<${#inarraydirs[@]}; a++ ))
do aidx=$(( $a + 1 ))
   thisfiles=( $( ls -v -1 "${inarraydirs[$a]}" | grep -i "$infilter" ) )
   #echo input $aidx , ${inarraydirs[$a]} : ${#thisfiles[@]}
   for (( f=0; f<${#thisfiles[@]}; f++ ))
   do eval arrayfiles${aidx}[$f]=\"${thisfiles[$f]}\"
   done
done
# Filenaming based on in1, ina1 or in[1] (inarraydirs)
# dirArray is the source
for (( f=0; f<${#arrayfiles1[@]}; f++ ))
do srcfiles[$f]="${outdir}/${arrayfiles1[$f]}"
done
if [ -d "$input_ina1" ]
then # array1 is the source
    srcfiles=( $( getarraykeys ina1 ) )
    for (( f=0; f<${#srcfiles[@]}; f++ ))
    do srcfiles[$f]="${outdir}/${srcfiles[$f]}"
    done
fi
if [ -d "$indir1" ]
then # dir1 is the source
    for (( f=0; f<${#infiles1[@]}; f++ ))
    do srcfiles[$f]="${outdir}/${infiles1[$f]}"
    done
fi

if [ $files -gt 0 ];
then for (( f=0; f<$files; f++ ))
     do [ -z "srcfiles[$f]" ] || srcfiles[$f]="${outdir}/file"$( printf "%0${#files}d" $(( $f + 1 )) )
     done
else files=${#srcfiles[@]}
fi

# create directories
mkdir "${outdir}" || echo error creating directory ${dir[$d]} >> "$errorfile"
pad=${#files}
# loop over images
for (( f=0; f<$files; f++ ))
    do newname="${srcfiles[$f]/%$inext/}${outext}"
       newbase=$( basename "${newname}" )
       oldbase=$( basename "${srcfiles[$f]}" )
    fpad=$( printf "%0${pad}d" $f )
    fpadplus=$( printf "%0${pad}d" $(( $f+1 )) )
    echo echo "\"${fpadplus} / ${files} : ${oldbase} -> ${newbase}\"" > "${tempdir}/magickrun.sh"
    echo -n \"${CONVERT}\"" " >> "${tempdir}/magickrun.sh"
    sed -e 's,@in1@,"'"${indir1}/${infiles1[$f]}"'",g' \
    -e 's,@in2@,"'"${indir2}/${infiles2[$f]}"'",g' \
    -e 's,@ina1@,"'"${inafiles1[$f]}"'",g' \
    -e 's,@ina2@,"'"${inafiles2[$f]}"'",g' \
    -e 's,@dir1@,"'"${indir1}/"'",g' \
    -e 's,@dir2@,"'"${indir2}/"'",g' \
    -e 's,@f1@,"'"${infile1}/${infilelist1[0]}"'",g' \
    -e 's,@f2@,"'"${infile2}/${infilelist2[0]}"'",g' \
    -e 's,@out@,"'"${newname}"'",g' \
    -e 's,@filenr@,'$(( $f + 1 ))',g' \
    -e 's,\r$,,' "${tempdir}/magicksource.sh" >> "${tempdir}/magickrun.sh"
    inall=""
    for (( a=0; a<${#inarraydirs[@]}; a++ ))
    do aidx=$(( $a + 1 ))
        mv "${tempdir}/magickrun.sh" "${tempdir}/magickrun.sh.tmp"
        # dirty trick to use dynamic array variables...
        sed -e 's,@a'$aidx'@,"'"${inarraydirs[$a]}/"$(eval "echo \${$(echo arrayfiles${aidx}[$f])}")'",g' \
        "${tempdir}/magickrun.sh.tmp" >> "${tempdir}/magickrun.sh"
        mv "${tempdir}/magickrun.sh" "${tempdir}/magickrun.sh.tmp"
        sed -e 's,@dira'$aidx'@,"'"${inarraydirs[$a]}/"'",g' \
        "${tempdir}/magickrun.sh.tmp" >> "${tempdir}/magickrun.sh"
        inall=$inall" \"${inarraydirs[$a]}/$(eval "echo \${$(echo arrayfiles${aidx}[$f])}")\""
    done    
    sed 's,@inall@,'"$inall"',g' -i "${tempdir}/magickrun.sh"
    #cat "${tempdir}/magickrun.sh" >> "$logfile"
    mv "${tempdir}/magickrun.sh" "${tempdir}/magickrun${fpad}.sh"
    rm -f "${tempdir}/magickrun.sh.tmp"

    echo $slsep >> "$logfile"
done

fpad=$( printf "%0${pad}d" 0 )
cat "${tempdir}/magickrun${fpad}.sh"  
rm "${tempdir}/magicksource.sh"
cd "$tempdir"
# parallelization
ls -v | xargs -I SCRIPT --max-procs=$parallel bash SCRIPT

for (( f=0; f<$files; f++ ))
    do newname="${srcfiles[$f]/%$inext/}${outext}"
    if [ "$failmiss" = "true" ]
    then if [ ! -f "${newname}" ]
            then echo "error converting file (${srcfiles[$f]})" >> "$errorfile"
         fi
    fi
done

