function list=skelwalker(gray,skel,maxdist)
    warning('off','MATLAB:polyfit:PolyNotUnique');
    if maxdist==0
        maxdist=inf;
    end
    imsize=size(skel);
    skel=cleanborders(skel,imsize);
    list=zeros(0,28);
    startps=zeros(0);
    %'Edge','Length','X1','Y1','X2','Y2',
    %'Alpha','Endpoint','Branchpoint'
    
    endimg=logical(bwmorph(skel,'endpoints'));
    braimg=logical(bwmorph(skel,'branchpoints'));
    skelout=skel;

    endps=find(endimg);
    branchps=find(braimg);
    if numel(endps)==0
        return
    end
    newskel=skel;
    seg=1;
    for e=1:numel(endps)
        newskel([endps;branchps])=true;
        [pair,newskel,walked,props,midpos]=firstpair(gray,newskel,endps,branchps,endps(e),maxdist);
        if pair==endps(e)
            continue
        end
        list(seg,1)=seg;
        list(seg,2)=walked;
        list(seg,3:4)=ind2coo(imsize,endps(e));
        list(seg,5:6)=ind2coo(imsize,pair);
        list(seg,8)=any(ismember([endps(e) pair],endps));
        list(seg,9)=any(ismember([endps(e) pair],branchps));
        list(seg,10:11)=ind2coo(imsize,midpos);
        list(seg,12)=sqrt(sum( (list(seg,3:4)-list(seg,5:6)).^2 ));
        list(seg,13:end)=props;
        startps(seg)=endps(e);
        seg=seg+1;
        if walked>maxdist
           % Generated branch gets added
            branchps=[pair; branchps];
        end
    end
    newskel(endps)=false;
    while numel(branchps)>0
        newskel(branchps)=true;
        [pair,newskel,walked,props,midpos]=firstpair(gray,newskel,endps,branchps,branchps(1),maxdist);
        if walked==1
            branchps(1)=[];
            continue
        end
        list(seg,1)=seg;
        list(seg,2)=walked;
        list(seg,3:4)=ind2coo(imsize,branchps(1));
        list(seg,5:6)=ind2coo(imsize,pair);
        list(seg,8)=any(ismember([branchps(1) pair],endps));
        list(seg,9)=any(ismember([branchps(1) pair],branchps));
        list(seg,10:11)=ind2coo(imsize,midpos);
        list(seg,12)=sqrt(sum( (list(seg,3:4)-list(seg,5:6)).^2 ));
        list(seg,13:end)=props;
        startps(seg)=branchps(1);
        seg=seg+1;
        if walked>maxdist
           % Generated branch gets added
            branchps=[pair; branchps];
        end
    end
    [sortstarts,sortidx]=sort(startps);
    list=list(sortidx,:);
    list(:,1)=1:size(list,1);
    list(:,7)=(-atan2(list(:,6)-list(:,4),list(:,5)-list(:,3)));
end

function coords=ind2coo(s,ind)
    [y,x]=ind2sub(s,ind);
    coords=[x y];
end

function bw=cleanborders(bw,siz)
    bw(1,:)=0;
    bw(:,1)=0;
    bw(siz(1),:)=0;
    bw(:,siz(2))=0;
end

function [pair,skel,walked,props,midpos]=firstpair(gray,skel,endps,branchps,p,maxdist)
    found=false;
    pos=p;
    %values=gray(pos);
    oldpos=pos;
    M=size(skel,1);
    N=[-1 M-1 M M+1 +1 -M+1 -M -M-1];
    Norient=N;
    endps=endps(p~=endps);
    branchps=branchps(p~=branchps);
    possible_hits=[endps;branchps];
    walked=1;
    while ~found
        skel(pos)=false;
        %imshow(skel); drawnow;
        neighbors=pos+Norient;
        if any(ismember(neighbors,possible_hits))
            found=true;
            pos=neighbors(ismember(neighbors,possible_hits));
            pos=pos(1);
            break;
        end
        newhit=find(skel(neighbors));
        if numel(newhit)==0
            pos=p;
            break;
        end
        pos=neighbors(newhit(1));
        if newhit(1)~=1
            Norient=circshift(Norient,[1 1-newhit(1)]);
        end
        if walked>maxdist
            break;
        end
        walked=walked+1;
        %values(end+1)=gray(pos);
        oldpos(end+1)=pos;
    end
    %values(end+1)=gray(pos);
    oldpos(end+1)=pos;
    values=gray(oldpos);
    numvalues=1:numel(values);
    midpos=oldpos(ceil(numvalues(end)/2));
    pair=pos;
    if numvalues(end)>2
       midpos=oldpos(ceil(numvalues(end)/2));
    else
       midpos=pos;
    end
    [vMin, vMinI]=min(values);
    [vMax, vMaxI]=max(values);
    props=[mean(values), median(values), sum(values), std(values), vMin,vMinI,vMax,vMaxI,0,0,0,0,0,0,0,0];
    [P,S]=polyfit(numvalues,values,2);
    valueshat=polyval(P,numvalues);
    props(9:11)=P(1:3);
    props(12)=sqrt(mean((values-valueshat).^2));
    if (numvalues(end))>2
        try
            [gfit,gerr]=fit(numvalues.',values.'-min(values),'gauss1');
            props(13:16)=[gfit.a1 gfit.b1 gfit.c1/sqrt(2) gerr.rmse];
        catch fitErr
            disp('Gaussian fitting error')
            disp(fitErr)
        end
    end
    %[m,id]=lastwarn
end
