function maskfilter(cf)
% cf = Anduril command file

tic
maskdir=getinput(cf,'mask');
origdir=getinput(cf,'names');
objectfile=getinput(cf,'objects');

outfile=getoutput(cf,'out');

filecolname=getparameter(cf,'fileId','string');
xcolname=getparameter(cf,'xId','string');
ycolname=getparameter(cf,'yId','string');

masklist=getimagelist(maskdir);
if strcmp(origdir,'')
    origlist=masklist;
    origdir=maskdir;
else
    origlist=getexoticimagelist(origdir);
end

if numel(masklist)==0
    writeerror(cf,'No images in source directory')
    return
end
if numel(masklist)~=numel(origlist)
    writeerror(cf,'Source directories have different number of images')
    return
end

objectdata=readcsvcell(objectfile,0,char(9));

files=getcellcol(objectdata,filecolname,'string');
xs=floor(getcellcol(objectdata,xcolname,'float'));
ys=floor(getcellcol(objectdata,ycolname,'float'));
outdata.columnheads=objectdata.columnheads;
for file=1:numel(masklist)
    idorig=origlist{file};
    idmask=[maskdir filesep masklist{file}];
    mask=logical(im2double(imread(idmask)));
    if ndims(mask)>2
        writeerror(cf,'Mask image dimensions exceed 2 (not BW image)');
    end
    disp(['Read image: ' masklist{file} ' (' num2str(file) '/' num2str(numel(masklist)) ')'])
    filerows=strcmp(files, idorig);
    if sum(filerows)==0
        writelog(cf,['File not found in list: ' idorig]);
    end
    this.xs=xs(filerows);
    this.ys=ys(filerows);
    this.idxs=sub2ind(size(mask),this.ys,this.xs);
    this.inmask=mask(this.idxs);
    this.data=objectdata.data(filerows,:);
    outdata.data=this.data(this.inmask,:);
    appendcsv(outfile,outdata);
end
toc
end

