#!/usr/bin/env python3
# http://www.jpathinformatics.org/article.asp?issn=2153-3539;year=2013;volume=4;issue=1;spage=27;epage=27;aulast=Goode
import openslide,math,os,sys,subprocess,numpy
from argparse import ArgumentParser

parser=ArgumentParser(description="Convert .mrxs to open image format(s).")
parser.add_argument("-f",action="store_true",dest="force",default=False,
                     help="Force overwriting of files.")
parser.add_argument("-l",action="store",dest="level",default=1,type=int,
                     help="input image resolution: 0=original, 1=half, 2=quarter... etc, [Default %(default)s]")
parser.add_argument("-t",action="store_true",dest="transparency",default=False,
                     help="If set, nonexisting pixels are transparent. If not set, those pixels are white.")
parser.add_argument("-a",action="store_true",dest="writeAll",default=False,
                     help="If set, write tiles even if there is no content.")
parser.add_argument("-w",action="store",dest="width",default=1250, type=int,
                      help="Width (and height) of output images. [Default %(default)s]")
parser.add_argument("-v",action="store_true",dest="verbose",default=False,
                     help="More verbose output.")
parser.add_argument('MRXS', action="store", help="The .mrxs file & folder to convert.")
parser.add_argument('output_name', action="store", help="Output folder. Images exported in the folder. Metadata exported to folder_metadata.jpg")
options=parser.parse_args()

image=options.MRXS
px=options.width
level=options.level
scaledown=int(math.pow(2,level))
out_dir=os.path.realpath(options.output_name)

def found_data(img):
    return any( lo != 0 or hi != 0 for lo, hi in img.getextrema() )

def write_if_data(img,name):
    if not options.writeAll:
        has_data=found_data(img)
        if not has_data:
            return False
    if options.verbose:
        print("Found data, write image: "+name)
    img.save(name)
    if not options.transparency:
        subprocess.call("mogrify -background white -flatten \"%s\""%(name,), shell=True)
    return True

if os.path.isdir(out_dir):
    if not options.force:
        print("path %s exist!"%(out_dir,))
        sys.exit(1)
else:
    os.mkdir(out_dir)

reader=openslide.OpenSlide(image)
for ass in reader.associated_images:
    fname="%s_%s.jpg" % (out_dir,ass)
    if options.verbose:
        print( fname )
    reader.associated_images[ass].convert('RGB').save(fname)

dims=reader.dimensions
dim_pad=str(max( [len(str(x)) for x in dims] ))
out_dims=(float(dims[0])/scaledown, float(dims[1])/scaledown)
out_blocks=( int(math.ceil( out_dims[0]/px )), int(math.ceil( out_dims[1]/px )) )

for y in range(0,out_blocks[1]):
    for x in range(0,out_blocks[0]):
        y_px=y*px
        x_px=x*px

        if options.verbose:
            sys.stdout.write("%d/%d\n" % (x+out_blocks[0]*y, out_blocks[0]*out_blocks[1] ))

        wrote=write_if_data(reader.read_region((scaledown*x*px,scaledown*y*px),level,(px,px)),
                            os.path.join(out_dir,("%s_-_y%0"+dim_pad+"d_x%0"+dim_pad+"d.png") % (os.path.basename(image),y_px,x_px))
                            )
        if not options.verbose:
            if wrote:
                sys.stdout.write("o")
            else:
                sys.stdout.write(".")
        sys.stdout.flush()
    sys.stdout.write("\n")  
