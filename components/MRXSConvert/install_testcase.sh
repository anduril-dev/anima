#!/bin/bash

cd $( dirname $BASH_SOURCE )
if [ ! -d testcases/case1 ]; then 
    wget -qO - http://www.anduril.org/pub/anduril_static/component_data/MRXSConvert/MRXSConvert_case1.tar | \
      tar xvf -
fi
