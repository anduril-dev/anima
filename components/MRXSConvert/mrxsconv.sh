
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
cat ../../doc/logo/logo.ansi.color

# make the list separator a newline
IFS=$'\n' 

# list files
if [ -d "$input_in" ]
then infiles=( $( ls -1 "$input_in" | grep -i "mrxs$" ) ) || echo "error reading input dir" >> "$errorfile"
fi

# create directories
mkdir "${output_out}" || writeerror "error creating directory ${output_out}"
mkdir "${output_label}" || writeerror "error creating directory ${output_label}"

[[ $parameter_transparent = "true" ]] && transparent="-t"
[[ $parameter_writeAll = "true" ]] && writeAll="-a"

# loop over images
for (( f=0; f<${#infiles[@]}; f++ )); do
    echo "$(( $f+1 )) / ${#infiles[@]} : ${infiles[$f]}"
    newbase="${infiles[$f]%.*}"
    ./mrxs_export.py -l $parameter_level $transparent $writeAll -w $parameter_width \
        "${input_in}/${infiles[$f]}" "${output_out}/${newbase}" || exit 1
    mkdir "${output_label}/${newbase}"
    mv "${output_out}/"*jpg "${output_label}/${newbase}"
    addarray out "${newbase}" "${newbase}"
    addarray label "${newbase}" "${newbase}"
done

