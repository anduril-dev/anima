#!/bin/bash
set +e
source "$ANDURIL_HOME/lang/bash/functions.sh"
LIBPATH=$( readlink -f ../../lib/python/Fiji )
export PYTHONPATH=$ANDURIL_HOME/lang/python:.:$LIBPATH:$PYTHONPATH
export JYTHONPATH=$ANDURIL_HOME/lang/python:.:$LIBPATH:$JYTHONPATH

FIJIPATH=$( readlink -f ../../lib/Fiji.app )

export PATH=$PATH:$FIJIPATH

which fiji &> /dev/null && FIJI=$( which fiji )
which ImageJ-linux64 &> /dev/null && FIJI=$( which ImageJ-linux64 )
echo Found Fiji: $FIJI
ls $( dirname $( readlink -f "$FIJI" ) )/jars | grep "^ij-"  >> ${logfile}

NOVIRTUAL=$( getparameter display )
if [ "$NOVIRTUAL" = "false" ]
then
    which Xvfb > /dev/null || {
        NOVIRTUAL=true
    }
fi

if [ "$NOVIRTUAL" = "false" ]
then 
    export DISPLAY=:$$
    rm -f /tmp/.X$$-lock
    ( Xvfb :$$ & ) 2> /dev/null
fi

"$FIJI" --mem=$( getparameter javaHeap )m --jython fijistart.py --allow-multiple --no-splash "$1" 
#> "$logfile"
exit_status=$?

if [ "$exit_status" -gt "0" ]
then echo "Component failed. Exit status: $exit_status" >> "$errorfile"
fi

if [ "$NOVIRTUAL" = "false" ]
then
    cat /tmp/.X$$-lock | xargs kill 
    rm -f /tmp/.X$$-lock
fi
