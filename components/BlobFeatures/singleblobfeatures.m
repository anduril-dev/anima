function dataout=singleblobfeatures(gray,mask,maskc,masker)

dataout=nan(1,8);
if sum(mask(:))>0
%    props=regionprops(mask,'FilledImage','BoundingBox');
%    cds=ceil(props(1).BoundingBox);  
    objvals=gray(mask==1);
    dataout(1,1)=mean(objvals);
    dataout(1,2)=median(objvals);
    dataout(1,3)=std(objvals);
    dataout(1,4)=sum(objvals);
    [inthist, bins]=imhist(objvals,4096);
    intcum=cumsum(inthist);
    lowlevs=find(intcum<0.01*sum(inthist));
    lowlevs=flipud(lowlevs);
    if size(lowlevs,1)==0
       lowlevs=1;
    end
    highlevs=find(intcum>0.99*sum(inthist));
    dataout(1,5)=bins(lowlevs(1));
    dataout(1,6)=bins(highlevs(1));    
    
    centermean=mean(gray(maskc>0));
    perimmean=mean(gray(masker>0));
    if ~isnan(perimmean)
        dataout(1,7)=centermean/perimmean;
        dataout(1,8)=centermean-perimmean;
    end
    end
    
end
