function blobvisu(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file struct
disp('Reading data...')
imdir=getinput(cf,'images');
csvout=getoutput(cf,'out');
csvin=getinput(cf,'objects');
imorigdir=getinput(cf,'names');

erodewidth=getparameter(cf,'erodeWidth','float');
erodelengthproportion=getparameter(cf,'isRatio','boolean');
colprefix=getparameter(cf,'prefix','string');
if strcmp(colprefix,' ')
    colprefix='';
end
calcoverlap=getparameter(cf,'overlap','boolean');
tic
imglist=getimagelist(imdir);
if numel(imorigdir)<2
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Image name list and gray scale list numbers do not match');
    disp('Image name list and gray scale list numbers do not match');
    return;
end


blobdata=readcsvcell(csvin);
data.rowid=getcellcol(blobdata,'RowId','string');
data.file=getcellcol(blobdata,'File','string');
data.obj=getcellcol(blobdata,'Object','float');
data.alpha=getcellcol(blobdata,'Alpha','float');
data.lx=getcellcol(blobdata,'Lx','float');
data.ly=getcellcol(blobdata,'Ly','float');
data.cy=getcellcol(blobdata,'Y','float');
data.cx=getcellcol(blobdata,'X','float');
data.rad=getcellcol(blobdata,'Rad','float');
oldheads=blobdata.columnheads;
colrowid=find(strcmp(blobdata.columnheads,'RowId'));
colfile=find(strcmp(blobdata.columnheads,'File'));
colobj=find(strcmp(blobdata.columnheads,'Object'));
colalpha=find(strcmp(blobdata.columnheads,'Alpha'));
collx=find(strcmp(blobdata.columnheads,'Lx'));
colly=find(strcmp(blobdata.columnheads,'Ly'));
colcy=find(strcmp(blobdata.columnheads,'Y'));
colcx=find(strcmp(blobdata.columnheads,'X'));
colrad=find(strcmp(blobdata.columnheads,'Rad'));
colecc=find(strcmp(blobdata.columnheads,'Eccentricity'));

olddatacols=[colrowid colfile colobj colalpha collx colly colcx colcy colrad colecc];
olddatacols=sort(olddatacols);

blobdata.columnheads=blobdata.columnheads(olddatacols);
blobdata.data=blobdata.data(:,olddatacols);

outdata=cell(numel(imglist),1);

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    im=im2double(imread(id));
    
    rows=strcmp(origimglist{file},data.file);
    
    objs=data.obj(rows);
    alpha=data.alpha(rows);
    lx=data.lx(rows);
    ly=data.ly(rows);
    cy=data.cy(rows);
    cx=data.cx(rows);
    rad=data.rad(rows);
    
    ellipseprec=max(16,round(5*max(rad)));
    
    %[data.cx data.cy data.rad data.Xbar data.Ybar data.alpha data.lx data.ly] = BlobDetect(im,0.01,15,50,true,0.005,10,true); 
    [height,width] = size(im);
        
    [Xbar,Ybar]=getedges(cx,cy,alpha,lx,ly,ellipseprec,width,height);
    if erodelengthproportion
        erodew=(erodewidth/100).*rad;
    else
        erodew=erodewidth;
    end
    [Xbarer,Ybarer]=getedges(cx,cy,alpha,lx-erodew,ly-erodew,ellipseprec,width,height);
    [imX,imY]=meshgrid(1:width,1:height);
        
    intensities=zeros(8,numel(objs));
    s=warning('off','MATLAB:inpolygon:ModelingWorld');
    
    if calcoverlap
        disp('Calculating overlap mask');
        labelmask=zeros(size(im));
        for o=find(lx>0)'
             xl=min(Xbar(o,:)); xh=max(Xbar(o,:));
             yl=min(Ybar(o,:)); yh=max(Ybar(o,:));
             labelmask(yl:yh,xl:xh)=labelmask(yl:yh,xl:xh)+inpolygon(imX(yl:yh,xl:xh),imY(yl:yh,xl:xh),Xbar(o,:),Ybar(o,:));
        end
    else
        labelmask=zeros(size(im));
    end
    overlaparea=nan(numel(cx),1);
    %progress=floor(numel(lx)*(1:10)/10);
    disp('Extracting features...')
    for o=find(lx>0)'
        xl=min(Xbar(o,:)); xh=max(Xbar(o,:));
        yl=min(Ybar(o,:)); yh=max(Ybar(o,:));
        mask=zeros(size(im));
        mask(yl:yh,xl:xh)=inpolygon(imX(yl:yh,xl:xh),imY(yl:yh,xl:xh),Xbar(o,:),Ybar(o,:));
        xl=min(Xbarer(o,:)); xh=max(Xbarer(o,:));
        yl=min(Ybarer(o,:)); yh=max(Ybarer(o,:));
        masker=zeros(size(im));
        masker(yl:yh,xl:xh)=inpolygon(imX(yl:yh,xl:xh),imY(yl:yh,xl:xh),Xbarer(o,:),Ybarer(o,:));
        masker=xor(mask,masker);
        maskc=mask-masker;
        if erodew<=0
            masker=(masker-labelmask-mask)>0;
        end
        area=sum(mask(:));
        overlap=sum(sum(and(labelmask-mask,mask)));
        overlaparea(o)=overlap/area;
        %intensities(:,o)=[singleblobfeatures(im,mask,erodewidth,erodelengthproportion,ly(o))'];
        intensities(:,o)=singleblobfeatures(im,mask,maskc,masker)';
        %if ismember(o,progress)
        %    fprintf('...%g%%',find(ismember(progress,o))*10)
        %end
    end
    if ~calcoverlap
        overlaparea=nan(numel(cx),1);
    end
    outdata{file}=[intensities' overlaparea];
    %fprintf('%d of %d done.\n',file,numel(imglist))
end

writeout.columnheads=[blobdata.columnheads {'Mean','Median','StDev', ...
  'Sum','Low 1%','High 1%',[colprefix 'Margination'],[colprefix 'Margination Difference'] 'Overlap'}];

dataout=cell2mat(outdata); clear outdata;
writeout.data=[blobdata.data num2csvcell(dataout)];

writefilecsv(csvout,writeout);    

toc
end
