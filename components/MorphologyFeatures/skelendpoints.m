shortest=0;
[branch_y,branch_x]=find(braimg==1);
[ends_y,ends_x]=find(endimg==1);
[branch_y_orig,branch_x_orig]=find(braimg==1);
[ends_y_orig,ends_x_orig]=find(endimg==1);

% branches too near end points
while shortest<bramin
    ends_y_sq=repmat(ends_y,[1 numel(branch_y)]);
    ends_x_sq=repmat(ends_x,[1 numel(branch_x)]);
    branch_y_sq=repmat(branch_y',[numel(ends_y) 1]);
    branch_x_sq=repmat(branch_x',[numel(ends_x) 1]);
    distmatrix=sqrt( (ends_y_sq - branch_y_sq ).^2 + (ends_x_sq - branch_x_sq ).^2 );
    [shortest,shortidx]=nanmin(distmatrix(:));
%    disp(num2str(shortest))
    if shortest<bramin
        [min_e,min_b]=ind2sub(size(distmatrix),shortidx);
        braimg(branch_y(min_b),branch_x(min_b))=0.8;
        endimg(ends_y(min_e),ends_x(min_e))=0.9;
        branch_y(min_b)=nan;
        branch_x(min_b)=nan;
        ends_y(min_e)=nan;
        ends_x(min_e)=nan;
    end
end
%branchremoved=sum(isnan(branch_y))
%endsremoved=sum(isnan(ends_y))

% branches too near other branches

if sum(sum(braimg==1))>1
    shortest=0;
    [branch_y,branch_x]=find(braimg==1);
    while shortest<bramin
        distmatrix=pdist([branch_y,branch_x]);
        shortest=nanmin(distmatrix);
        [min_1,min_2] = find(squareform(distmatrix)==shortest,1,'first');
%    disp(num2str(shortest))
        if shortest<bramin
            braimg(branch_y(min_1),branch_x(min_1))=0.8;
            branch_y(min_1)=nan;
            branch_x(min_1)=nan;
        end
    end
%branchremoved=sum(isnan(branch_y))
end
