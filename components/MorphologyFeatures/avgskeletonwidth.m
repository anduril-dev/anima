function [avgs,stds]=avgskeletonwidth(perim,skel)
% function avg=avgskeletonwidth(perim,skel)
% Calculates the average width of an object by providing the perimeter mask and the skeleton line mask of the object. Average is the mean distance from nearest perimeter point to the skeleton line.


dists=bwdist(perim,'euclidean');
skeldists=2*dists(skel==1);  % these are half distances.
avgs=mean(skeldists);
stds=std(skeldists);

