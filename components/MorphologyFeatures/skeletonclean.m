function skelout=skeletonclean(skel,mindist)
    
    skelout=skel;
    % pad by 1 pixel
    skel=padarray(skel,[1 1]);
    imsize=size(skel);
    startps=zeros(0);
    
    endimg=logical(bwmorph(skel,'endpoints'));
    braimg=logical(bwmorph(skel,'branchpoints'));
    endps=find(endimg);
    branchps=find(braimg);
    if numel(endps)==0
        return
    end
    if numel(endps)==2
        return
    end
    skelout=skel;
    newskel=skel;
    %for e=1:numel(endps)
    while numel(endps)>0
        endps_this=endps(1);
        newskel([endps;branchps])=true;
        [pair,newskel,walked,poslist]=firstpair(newskel,[endps;branchps],endps_this);
        if walked < mindist
            skelout(poslist)=0;
        end
        endimg(pair)=0;
        endimg(endps_this)=0;
        endps=find(endimg);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %newskel=zeros(size(skelout));
    %while true
        %skelout=bwmorph(skelout,'thin',Inf);
        %braimg=logical(bwmorph(skelout,'branchpoints'));
        %branchps=find(braimg);
        %endimg=logical(bwmorph(skelout,'endpoints'));
        %endps=find(endimg);
        %newskel=skelout;
        %%disp(numel(branchps))
        %if numel(branchps)==0
            %break
        %end
        %lengths=zeros(numel(branchps),1);
        %paths=cell(numel(branchps),1);
        %seg=1
        %%for e=1:numel(branchps)
        %while numel(branchps)>0
            %newskel(branchps)=true;
            %braps_this=branchps(1);
            %[pair,newskel,walked,poslist]=firstpair(newskel,branchps,braps_this);
            %if walked==1
                %branchps(1)=[];
                %continue
            %end
            %lengths(seg)=walked;
            %paths{seg}=poslist;
            %seg=seg+1;
            %%if walked < mindist
            %%    skelout(poslist)=0;
            %%    break
            %%end
        %end
        %if min(lengths)>mindist
            %break
        %end
        %[lengths,sortidx]=sort(lengths,'descend');
        %longest=find(lengths<mindist);
        %skelout(paths{sortidx(longest(1))})=0;
        
    %end

    % Crop back to original size
    skelout=bwmorph(skelout,'thin',Inf);
    skelout=skelout(2:end-1,2:end-1);
    
end

function coords=ind2coo(s,ind)
    [y,x]=ind2sub(s,ind);
    coords=[x y];
end

function [pair,skel,walked,oldpos]=firstpair(skel,endps,p)
    found=false;
    pos=p;
    oldpos=pos;
    M=size(skel,1);
    Norient=[-1 M-1 M M+1 +1 -M+1 -M -M-1];
    possible_hits=endps(~ismember(endps,p));
    walked=1;
    while ~found
        skel(pos)=false;
        %imshow(skel); drawnow;
        neighbors=pos+Norient;
        if any(ismember(neighbors,possible_hits))
            found=true;
            pos=neighbors(ismember(neighbors,possible_hits));
            pos=pos(1);
            break;
        end
        newhit=find(skel(neighbors));
        if numel(newhit)==0
            pos=p;
            break;
        end
        pos=neighbors(newhit(1));
        if newhit(1)~=1
            Norient=circshift(Norient,[1 1-newhit(1)]);
        end
        walked=walked+1;
        oldpos(end+1)=pos;
    end
    pair=pos;
end
