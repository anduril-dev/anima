function data = blobdetect(I,thresh,startradius,endradius,mineig,steps)
%function data = blobdetect(name,thresh,startradius,endradius,affine,mineig,steps,accelerate)
%I = image data
%thresh = local minimum threshold
%startradius = The smallest size blob to find (radius)
%endradius = The largest size blob to find (radius)
%mineig = minimum eiginvalue allowed in a (affine) blob
%steps = steps between min and max blob radius
%
% data=[cx cy rad alpha lx ly axisratio];
% Original implementation by Stephen J. Guy <sjguy@cs.unc.edu>
% Anduril implementation by Ville Rantanen ville.rantanen@helsinki.fi
%
% Implements the Blob feature detection as presented in:
% Feature Detection with Automatic Scale Selection
% Tony Lindeberg
% Technical report ISRN KTH NA/P--96/18--SE. Department of Numerical Analysis and Computing Science, Royal Institute of Technology, S-100 44 Stockholm, Sweden, May 1996.
% International Journal of Computer Vision, vol 30, number 2, pp 77--116, 1998.

if ~exist('steps','var')
   steps = 12;
end
if ~exist('mineig','var')
    mineig = .001;
end
if ~exist('endradius','var')
    endradius = 12;
end
if ~exist('startradius','var')
    startradius = 6;
end
if ~exist('thresh','var')
    thresh = .01;
end

startsigma = startradius/sqrt(2);
k = (endradius/startradius)^(1/(steps-1));

[h w] = size(I);
scale_space = zeros(h,w,steps); % [h,w] - dimensions of image, steps - number of levels in scale space
max_space = zeros(h,w,steps);

s = startsigma;
fprintf('Building Scale Space\n')

for i = 1:steps
   %if (~accelerate)
   %     disp(['radius ' num2str(s)])
%		%TODO: I should enforce an odd kernel
%        scale_space(:,:,i) = imfilter(I, s^2*fspecial('log', [round(min(5*s,100)), round(min(5*s,100))],s),'symmetric').^2;
%        max_space(:,:,i) = ordfilt2(scale_space(:,:,i),25,true(5));
%        s = s*k;
%   else
        scale = k^(i-1);
        fprintf('Sigma of %f\n',scale*startsigma)
        Ismall = imresize(I,1/scale,'bicubic');
        tmp = imfilter(Ismall, s^2*fspecial('log', [round(5*s), round(5*s)],s),'symmetric').^2;
        scale_space(:,:,i) = imresize(tmp,[h w],'bicubic');
%   end
   max_space(:,:,i) = ordfilt2(scale_space(:,:,i),81,true(9));
end

disp('Find local min')

scaleoverthresh=scale_space > thresh;
scaleismax=scale_space == max_space;
scaleisgtmaxplus1=scale_space(:,:,1:(end-1)) > max_space(:,:,2:end);
scaleisgtmaxplus1(:,:,steps)=true; %scaleoverthresh(:,:,steps);
scaleisltmaxminu1=zeros(size(scaleisgtmaxplus1));
scaleisltmaxminu1(:,:,2:end)=scale_space(:,:,2:end) > max_space(:,:,1:(end-1));
scaleisltmaxminu1(:,:,1)=true; %scaleoverthresh(:,:,1);

scale_localmax=and(scaleisgtmaxplus1,scaleisltmaxminu1);
scaleoverandmax=and(scaleismax,scaleoverthresh);
supressed=and(scale_localmax,scaleoverandmax);

[qind]=find(supressed);
[cy cx rad]=ind2sub(size(supressed),qind);
% remove objects touching top border
borderhit=zeros([numel(cy) 4]);
borderhit(:,1)=cy==1;
borderhit(:,2)=cx==1;
borderhit(:,3)=cy==h;
borderhit(:,4)=cx==w;
borderhit=~max(borderhit,[],2);
cy=cy(borderhit);
cx=cx(borderhit);
rad=rad(borderhit);

rad=startsigma.*sqrt(2).*(k).^(rad-1);

Iblur = imfilter(I, fspecial('gaussian',[25 25], 5));
disp('Computing 2nd Moment Matrix')

[Ix Iy] = gradient(Iblur);

alpha = zeros(1,numel(cx));
lx = rad';
ly = rad';
for n = 1:numel(cx)
    j = cx(n);
    i = cy(n);
    k = round(min([ rad(n)/1.4 i-1 j-1 w-j h-i ]));

    Ixw = Ix(i+(-k:k),j+(-k:k)); %window
    Iyw = Iy(i+(-k:k),j+(-k:k));
    ix2 = sum(Ixw(:).^2);
    iy2 = sum(Iyw(:).^2);
    ixy = sum(Ixw(:).*Iyw(:));
    M = [ix2 ixy; ixy iy2]; %2nd moment matrix
    [R e] = eig(M);
    alpha(n) = atan2(R(3),R(4)); %angle of ellipse
    if (min(e(1),e(4)) > mineig)
        xlen = 1/sqrt(e(1));
        ylen = 1/sqrt(e(4));
        sumlen = (xlen + ylen);
        lx(n) = xlen*(2*rad(n)/sumlen);
        ly(n) = ylen*(2*rad(n)/sumlen);
    end
end
elratio=lx;
elratio(lx>0)=ly(lx>0)./lx(lx>0);
data=[cx cy rad alpha' lx' ly' elratio'];
%data.cx=cx;
%data.cy=cy;
%data.rad=rad;
%data.alpha=alpha';
%data.lx=lx';
%data.ly=ly';
