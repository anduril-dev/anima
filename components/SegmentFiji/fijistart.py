from component_skeleton.main import main
from fijisave import save_image
import fijijython
import sys,os
from ij import IJ
from ij.process import ImageProcessor
import ij.Prefs

print("Fiji relies on the old API")
ij.Prefs.blackBackground = True

def execute(cf):
    # Read parameters etc
    method = cf.get_parameter('method', 'string')
    correction = cf.get_parameter('corr', 'float')

    images = cf.get_input('in')
    maskout= cf.get_output('mask')
    perimout= cf.get_output('perimeter')
    os.mkdir(maskout)
    os.mkdir(perimout)
    l_images=fijijython.get_imagelist(images)
    for i, img in enumerate(l_images):
        print("%s %d/%d"% (img, i+1,len(l_images)))
        imgI = IJ.openImage(os.path.join(images, img))
        IJ.setAutoThreshold(imgI, method)
        ip = imgI.getProcessor()
        #threshold = ip.getMinThreshold() * correction
        #print(threshold, correction,ip.getMinThreshold(),ip.getMaxThreshold())
        ip.setThreshold(0.0, ip.getMaxThreshold()*correction, ImageProcessor.NO_LUT_UPDATE)
        IJ.run(imgI, "Convert to Mask", "")
        
        outfilename = os.path.splitext(img)[0]
        save_image(imgI, maskout, outfilename, ".png")
        # perimeters
        IJ.run(imgI, "Invert", "")
        IJ.run(imgI, "Outline", "")
        IJ.run(imgI, "Invert", "")
        save_image(imgI, perimout, outfilename, ".png")

    return 0

main(execute)
