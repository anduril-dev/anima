#!/usr/bin/python
import sys,os
import anduril.imagetools as ait
from anduril.args import *
import anduril
from Tkinter import *
import tkMessageBox
import Image, ImageTk, math, ImageDraw

class Click:
    def __init__(self, options):
        
        self.title=options["title"]
        self.annotation=options["annotation"]
        self.drawCircle=options["drawCircle"]
        self.saveArea=options["saveArea"]
        if self.saveArea:
            options["zoom"]=True
        self.root=Tk()
        self.root.geometry("1024x768+0+0")
        self.root.bind("<Escape>", self._quit)
        self.root.bind("Q", self._quit)
        self.root.bind("F", self._quit)
        self.root.bind("h", self._help)
        self.root.bind("-", self._increase_area)
        self.root.bind("<KP_Subtract>", self._increase_area)
        self.root.bind("+", self._decrease_area)
        self.root.bind("<KP_Add>", self._decrease_area)
        
        self.image = Image.open(options["image_file"])
        self.image_name = os.path.basename(options["image_file"])
        self.img_orig = self.image.copy()
        self.img_copy= self.image.copy()
        self.pixels = self.img_orig.load()
        self.background_image = ImageTk.PhotoImage(self.image)
        self.click=0
        self.max_click=options["clicks"]
        self.break_loop=False
        self.fail_component=False
        self.zoom_radius=5
       
        self.background = Label(self.root, image=self.background_image, cursor='cross')
        self.background.pack(fill="both", expand=True, side="left")
        self.background.bind('<Configure>', self._resize_image)
        self.background.bind("<Button 1>", self._write_coords)
        
        if options["zoom"]:
            self.img_zoom= self.img_orig.crop((0,0,2*self.zoom_radius,2*self.zoom_radius)).resize((220,220))
            self.zoom_image = ImageTk.PhotoImage(self.img_zoom)
            self.background.bind("<Motion>", self._zoom_update)
            self.top=Toplevel(self.root)
            self.zoom = Label(self.top, image=self.zoom_image)
            self.zoom.pack(fill="both", expand=True, side="left")
            self.top.geometry("220x220+1024+0")
            self.fill=tuple([255 for i in range(len(self.image.split()))])
            if len(self.fill)==1:
                self.fill=self.fill[0]
        
        self._add_click()
        self._resize_init()
        self.root.mainloop()

    def _zoom_update(self,event):
        r=self.zoom_radius
        zoom_offset=int(self.root.geometry().split("x")[0])+int(self.root.geometry().split("+")[1])
        root_y=int(self.root.geometry().split("+")[2])
        self.top.geometry("220x220+%d+%d" %( zoom_offset, root_y ))
        x=int(math.floor(self.img_copy.size[0]*float(event.x)/self.image.size[0]))
        y=int(math.floor(self.img_copy.size[1]*float(event.y)/self.image.size[1]))
        self.img_zoom = self.img_orig.crop(
                                (x-r,y-r,
                                 x+r+1,y+r+1))
        draw=ImageDraw.Draw(self.img_zoom)
        draw.ellipse((0,0,2*r,2*r),outline='white')
        draw.arc((0,0,2*r,2*r),0,90,'black')
        draw.arc((0,0,2*r,2*r),180,270,'black')
        
        self.img_zoom = self.img_zoom.resize((220, 220))
        self.zoom_image=ImageTk.PhotoImage(self.img_zoom)
        self.zoom.configure(image = self.zoom_image )
        self.top.title("R=%d"%(r,))

    def _resize_image(self,event):
        new_width = event.width
        new_height = event.height
        self.image = self.img_copy.resize((new_width, new_height))
        self.background_image = ImageTk.PhotoImage(self.image)
        self.background.configure(image =  self.background_image)
    
    def _resize_init(self):
        event = self._get_max_size()
        self.root.geometry("%dx%d+0+0"% (event.width, event.height))
        self._resize_image(event)
    
    def _get_max_size(self,refer_size=None):
        """ return max size for image that fits the refer_size,.
            otherwise, return max size for the screen """
        if refer_size:
            refer_width=refer_size[0]
            refer_height=refer_size[1]
        else:
            refer_width=self.root.winfo_screenwidth()-50
            refer_height=self.root.winfo_screenheight()-50
        new_height=refer_height
        new_width= int(float(self.img_copy.size[0])/float(self.img_copy.size[1])*new_height)
        if new_width>refer_width:
            new_width=refer_width
            new_height=int(float(self.img_copy.size[1])/float(self.img_copy.size[0])*new_width)
        event = type('eventclass', (object,), 
                 {'width':new_width, 'height':new_height})()
        return event
    
    def _add_click(self):
        self.click+=1
        self.root.title("%s %d/%d"%(self.title.replace("%f",self.image_name),self.click,self.max_click))
        
    def _write_coords(self,event):
        if self.saveArea:
            self._write_area_coords(event)
        else:
            self._write_single_coords(event)
        if self.max_click>0:
            if self.click>=self.max_click:
                self.root.destroy()
                return
        self._add_click()

    def _write_area_coords(self,event):
        x=int(math.floor(self.img_copy.size[0]*float(event.x)/self.image.size[0]))
        y=int(math.floor(self.img_copy.size[1]*float(event.y)/self.image.size[1]))
        
        if self.drawCircle:
            r=self.zoom_radius
            draw=ImageDraw.Draw(self.image)
            eX=int(event.x)
            eY=int(event.y)
            draw.ellipse((eX-r,eY-r,eX+r,eY+r),outline='white')
            draw.arc((eX-r,eY-r,eX+r,eY+r),0,90,'black')
            draw.arc((eX-r,eY-r,eX+r,eY+r),180,270,'black')
            self.background_image = ImageTk.PhotoImage(self.image)
            self.background.configure(image =  self.background_image)
            draw=ImageDraw.Draw(self.img_copy)
            draw.ellipse((x-r,y-r,x+r,y+r),outline='white')
            draw.arc((x-r,y-r,x+r,y+r),0,90,'black')
            draw.arc((x-r,y-r,x+r,y+r),180,270,'black')
            del draw
        # Only take values INSIDE the circle
        rad=self.zoom_radius
        mask=Image.new("L",(2*rad+1,2*rad+1),0)
        draw=ImageDraw.Draw(mask)
        draw.ellipse((0,0,2*rad,2*rad),outline=0,fill=1)
        values=[]
        for dy in range(-rad,rad+1,1):
            for dx in range(-rad,rad+1,1):
                #~ sys.stdout.write("%01d"%(mask.getpixel((dx+rad,dy+rad)),))
                if mask.getpixel((dx+rad,dy+rad))==0:
                    continue
                xi=x+dx
                yi=y+dy
                try:
                    (r,g,b)=self._get_pixel_value(xi,yi)
                    values.append((xi,yi,r,g,b))
                except IndexError:
                    continue
            #~ sys.stdout.write("\n")
        for pixel in values:
            writer.writerow({'File': self.image_name,
                         'Click': self.click,
                         'X': pixel[0],
                         'Y': pixel[1],
                         'R': pixel[2],
                         'G': pixel[3],
                         'B': pixel[4],
                         'Annotation':self.annotation})


    def _write_single_coords(self,event):
        x=int(math.floor(self.img_copy.size[0]*float(event.x)/self.image.size[0]))
        y=int(math.floor(self.img_copy.size[1]*float(event.y)/self.image.size[1]))
        (r,g,b)=self._get_pixel_value(x,y)
        if self.drawCircle:
            draw=ImageDraw.Draw(self.image)
            eX=int(event.x)
            eY=int(event.y)
            draw.ellipse((eX-10,eY-10,eX+10,eY+10),outline='white')
            draw.arc((eX-10,eY-10,eX+10,eY+10),0,90,'black')
            draw.arc((eX-10,eY-10,eX+10,eY+10),180,270,'black')
            self.background_image = ImageTk.PhotoImage(self.image)
            self.background.configure(image =  self.background_image)
            draw=ImageDraw.Draw(self.img_copy)
            draw.ellipse((x-10,y-10,x+10,y+10),outline='white')
            draw.arc((x-10,y-10,x+10,y+10),0,90,'black')
            draw.arc((x-10,y-10,x+10,y+10),180,270,'black')
            del draw
        writer.writerow({'File': self.image_name,
                         'Click': self.click,
                         'X': x,
                         'Y': y,
                         'R':r,
                         'G':g,
                         'B':b,
                         'Annotation':self.annotation})

    def _help(self,event):
        help_message="""
Click on image to save coordinates & color value.
Keyboard commands:
Escape  Skip to next image
Q       Quit, but continue workflow
F       Quit and fail component
+/-     Zoom radius
h       Show this help message

Click OK to continue
"""
        tkMessageBox.showinfo("ImageClick help", help_message)
    def _decrease_area(self,event):
        self.zoom_radius-=1
        if self.zoom_radius<1:
            self.zoom_radius=1
        self._zoom_update(event)
            
    def _increase_area(self,event):
        self.zoom_radius+=1
        self._zoom_update(event)

    def _quit(self,event):
        if event.char=="Q":
            self.break_loop=True
        if event.char=="F":
            self.break_loop=True
            self.fail_component=True
        self.root.destroy()
    
    def _get_pixel_value(self,x,y):
        i=self.pixels[x,y]
        if type(i)==int:
            (r,g,b)=(i,i,i)
        else:
            if len(i)==2:
                (r,g,b)=(i[0],i[0],i[0])
            if len(i)>2:
                (r,g,b)=(i[0],i[1],i[2])
        return (r,g,b)

imagelist=ait.get_imagelist(input_in)
writer=anduril.TableWriter(out,fieldnames=['File','Click','X','Y','R','G','B','Annotation'])
    
for i,l in enumerate(imagelist):
    write_log( "%s %d/%d"% (l, i+1, len(imagelist)) )
    clicker = Click({
        "image_file":os.path.join(input_in, l), 
        "clicks":param_clicks, 
        "title":"[%d/%d] %s"%(i+1, len(imagelist),param_title),
        "zoom":param_zoom, 
        "annotation":param_annotation,
        "drawCircle":param_drawCircle,
        "saveArea":param_saveArea
        })
    if clicker.fail_component:
        write_log("Instance failed by user keypress")
        sys.exit(1)
    if clicker.break_loop:
        write_log("Running interrupted by user")
        break
