function [mask,perim]=jimmysegment(im,colorvalues, clusters)
% Segment an image to different color values with quadratic classifier

[h,w,c]=size(im);
uniclust=unique(clusters);
if size(colorvalues,2)~=c,
   error('Anduril:ValueError',['Color vector length ' num2str(size(colorvalues,2)) ' is different from image channel number ' num2str(c)])
end

[imclust,P]=qldc_im(floor(255*im),colorvalues,clusters,'quadratic',0);

perim=false([h w]);
for c=2:max(clusters)
    mask=imclust==c;
    perim=or(perim,bwperim(mask,4));
end
mask=(imclust-min(clusters))/(max(clusters)-min(clusters));

