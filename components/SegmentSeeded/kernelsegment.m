function [mask,perim]=knnsegment(im,colorvalues, clusters, kernel)
% Segment an image to different color values based on example color values, 
% and kernel.

[h,w,c]=size(im);
uniclust=unique(clusters);
if size(colorvalues,2)~=c,
   error('Anduril:ValueError',['Color vector length ' num2str(size(colorvalues,2)) ' is different from image channel number ' num2str(c)])
end
vect=reshape(im,h*w,[]);
nearest=zeros(h*w, numel(uniclust));
for c=uniclust'
    c_colorvalues=colorvalues(clusters==c,:);
    x=pdist2(vect,c_colorvalues)';
    nearest(:,c)=eval(kernel)';
end
[foo,idx]=max(nearest,[],2);
imclust=zeros(h*w,1);
for c=uniclust'
    imclust(idx==c)=c;
end
imclust=reshape(imclust,h ,w);

perim=false([h w]);
for c=2:max(clusters)
    mask=imclust==c;
    perim=or(perim,bwperim(mask,4));
end
mask=(imclust-min(clusters))/(max(clusters)-min(clusters));

