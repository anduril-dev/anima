function [mask,perim]=knnsegment(im,colorvalues, clusters)
% Segment an image to different color values based on example color values, 
% and KNN -search.

[h,w,c]=size(im);
vect=reshape(im,h*w,[]);
nearest=knnsearch(colorvalues, vect);

imclust=reshape(clusters(nearest),h ,w);
perim=false([h w]);
for c=2:max(clusters)
    mask=imclust==c;
    perim=or(perim,bwperim(mask,4));
end

mask=(imclust-min(clusters))/(max(clusters)-min(clusters));

