function mask=seededareasegment(gray,seed,ratio)
% bwout=seededareasegment(data,seed,ratio)
% data is a gray scale image, seed a BW mask image
% ratio is the 0-1 value that the mask foreground should cover for each object in seed.

%% force method is surely slower, but it gives absolutely the correct value. (fminbound doesnt)
% uses fminbnd to find the threshold to use.

q=96;
props=regionprops(bwlabel(seed,4),gray,'Centroid','BoundingBox','Image','PixelValues','PixelIdxList');
objs=size(props,1);
mask=false(size(gray));
if objs>0
coords=reshape([props.Centroid], [2 objs])';
for obj=1:objs
    tempdata=zeros(size(props(obj).Image));
    tempdata(props(obj).Image)=gray(props(obj).PixelIdxList);
    levels=unique(tempdata);
    if numel(levels)>q
        levels=levels(round([1:q]*numel(levels)/q));
    end
    fgratio=zeros(numel(levels),1);
    for leveliter=1:numel(levels)
        level=levels(leveliter);
        submask=tempdata>=level;
        fgratio(leveliter)=sum(submask(:));
    end
    ratiodiff=abs((fgratio/numel(props(obj).PixelIdxList)) - ratio);
    [tval,tind]=min(ratiodiff);
    submask=tempdata>=levels(tind);
    mask(props(obj).PixelIdxList)=submask(props(obj).Image);
end
end

