function imageintensity(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file
tic
imdir=getinput(cf,'images');
maskdir=getinput(cf,'mask');
imorigdir=getinput(cf,'names');
imdirlist=getinput(cf,'masklist');

outfile=getoutput(cf,'out');
outdir=getoutput(cf,'edges');
erodewidth=getparameter(cf,'erodeWidth','float');
erodelengthproportion=getparameter(cf,'isRatio','boolean');
colprefix=getparameter(cf,'prefix','string');
connectivity=getparameter(cf,'connectivity','float');
percentile=getparameter(cf,'percentile','float');
texturescale=getparameter(cf,'textureScale','float');

if all(connectivity~=[4 8])
    writeerror(cf,'Connectivity must be 4 or 8.')
    return
end

if ~all([percentile<100 percentile>0])
    writeerror(cf,'Percentile must be between 0 and 100')
    return
end
if ~all([texturescale<100 percentile>0])
    writeerror(cf,'Texture scale must be between 0 and 100')
    return
end

imglist=getimagelist(imdir);
masklist=getimagelist(maskdir);
if numel(imorigdir)<2
    imorigdir=maskdir;
    origimglist=masklist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Intensity image and name list directories have different number of images')
    return
end
if numel(imglist)~=numel(masklist)
    writeerror(cf,'Intensity and mask image directories have different number of images')
    return
end
mkdir(outdir);

writeout.columnheads={'RowId','File','Object','Mean','Median','StDev', ...
    'Sum',sprintf('Low %g%%',percentile),sprintf('High %g%%',percentile),...
    'Edge Mean','Edge Median','Edge StDev','Edge Sum','Margination','Margination Difference',...
    'Full Mean','Full Median','Full StDev','Full Sum',...
    'HAngular 2 Moment','HContrast','HCorrelation','HVariation',...
    'HInverse Difference Moment','HSum Average','HSum Variance',...
    'HSum Entropy','HEntropy','HDifference Variance','HDifference Entropy',...
    'HInformation Measure of Correlation 1','HInformation Measure of Correlation 2',...
    'GaborX0','GaborX45','GaborX90','GaborX135', 'GaborMax','X','Y'
    };
writeout.columnheads=cellfun(@(x) [colprefix x], writeout.columnheads,'UniformOutput',false);
for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    idmask=[maskdir filesep masklist{file}];
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    [dataout,eroded]=imagesinglefeatures(cf,id,idmask,erodewidth,erodelengthproportion,percentile,[imdirlist filesep masklist{file}],connectivity,texturescale);
    imwrite(logical(eroded),[outdir filesep imglist{file} pngstr]);
    dataout=num2csvcell(dataout);
    objs=size(dataout,1);
    dataoutname=repmat(origimglist(file),[objs 1]);
    dataoutname=[ strcat(dataoutname,'_',dataout(:,1)) dataoutname];  % Add the name_obj  id string
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile,writeout);
end

close all
toc
end
