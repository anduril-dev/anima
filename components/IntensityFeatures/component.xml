<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>IntensityFeatures</name>
    <version>2.3</version>
    <doc>Extracts the intensity features from segmented image mask and gray scale image.
    
     Example:
    <p><b>Sources:</b>
    <table><tr><td><img src="input1.jpg"/></td><td><img src="input2.jpg"/></td></tr></table></p>
    <b>Outputs:</b><br />
    <img src="output.png"/><br />
    <table border="1">
<tr>
<td  valign="bottom"  align="left" >File</td>
<td  valign="bottom"  align="left" >Object</td>
<td  valign="bottom"  align="left" >Mean</td>
<td  valign="bottom"  align="left" >Median</td>
<td  valign="bottom"  align="left" >StDev</td>
<td  valign="bottom"  align="left" >Sum</td>
<td  valign="bottom"  align="left" >Low 1%</td>
<td  valign="bottom"  align="left" >High 1%</td>
<td colspan="2"  valign="bottom"  align="left" >Margination</td>
</tr>
<tr>
<td  valign="bottom"  align="left" >cells.tif_test.png</td>
<td  valign="bottom"  align="right" >1</td>
<td  valign="bottom"  align="right" >0.09380</td>
<td  valign="bottom"  align="right" >0.09412</td>
<td  valign="bottom"  align="right" >0.00702</td>
<td  valign="bottom"  align="right" >68.0039</td>
<td  valign="bottom"  align="right" >0.07424</td>
<td  valign="bottom"  align="right" >0.10598</td>
<td  valign="bottom"  align="right" >1.02744</td>

</tr>
<tr>
<td  valign="bottom"  align="left" >cells.tif_test.png</td>
<td  valign="bottom"  align="right" >2</td>
<td  valign="bottom"  align="right" >0.54362</td>
<td  valign="bottom"  align="right" >0.56078</td>
<td  valign="bottom"  align="right" >0.10123</td>
<td  valign="bottom"  align="right" >155.475</td>
<td  valign="bottom"  align="right" >0.25861</td>
<td  valign="bottom"  align="right" >0.74115</td>
<td  valign="bottom"  align="right" >1.27216</td>

</tr>
<tr>
<td  valign="bottom"  align="left" >cells.tif_test.png</td>
<td  valign="bottom"  align="right" >3</td>
<td  valign="bottom"  align="right" >0.69176</td>
<td  valign="bottom"  align="right" >0.69804</td>
<td  valign="bottom"  align="right" >0.18622</td>
<td  valign="bottom"  align="right" >419.204</td>
<td  valign="bottom"  align="right" >0.31355</td>
<td  valign="bottom"  align="right" >1</td>
<td  valign="bottom"  align="right" >1.58960</td>

</tr>
<tr>
<td  valign="bottom"  align="left" >cells.tif_test.png</td>
<td  valign="bottom"  align="right" >4</td>
<td  valign="bottom"  align="right" >0.58782</td>
<td  valign="bottom"  align="right" >0.62353</td>
<td  valign="bottom"  align="right" >0.16344</td>
<td  valign="bottom"  align="right" >269.224</td>
<td  valign="bottom"  align="right" >0.21538</td>
<td  valign="bottom"  align="right" >0.86276</td>
<td  valign="bottom"  align="right" >1.44734</td>

</tr>
<tr>
<td  valign="bottom"  align="left" >cells.tif_test.png</td>
<td  valign="bottom"  align="right" >5</td>
<td  valign="bottom"  align="right" >0.71412</td>
<td  valign="bottom"  align="right" >0.74118</td>
<td  valign="bottom"  align="right" >0.17285</td>
<td  valign="bottom"  align="right" >492.027</td>
<td  valign="bottom"  align="right" >0.34481</td>
<td  valign="bottom"  align="right" >0.98437</td>
<td  valign="bottom"  align="right" >1.53338</td>

</tr>
<tr>
<td  valign="bottom"  align="left" >cells.tif_test.png</td>
<td  valign="bottom"  align="right" >6</td>
<td  valign="bottom"  align="right" >0.42481</td>
<td  valign="bottom"  align="right" >0.41176</td>
<td  valign="bottom"  align="right" >0.12128</td>
<td  valign="bottom"  align="right" >379.780</td>
<td  valign="bottom"  align="right" >0.22320</td>
<td  valign="bottom"  align="right" >0.71771</td>
<td  valign="bottom"  align="right" >1.44711</td>

</tr>
</table>

Haralick texture feature extraction algorithm: Haralick et al. (1973),
"Textural Features for Image Classification.",% IEEE Transaction on Systems,
Man, Cybernetics, SMC-3(6):610-621, with GPL licensed CellProfiler implementation, 
https://svn.broadinstitute.org/CellProfiler/trunk/CellProfiler_old_matlab/Modules/
(Jones TR, Kang IH, Wheeler DB, Lindquist RA, Papallo A, Sabatini DM, 
 Golland P, Carpenter AE (2008) CellProfiler Analyst: data exploration 
 and analysis software for complex image-based screens. 
 BMC Bioinformatics 9(1):482/doi: 10.1186/1471-2105-9-482. PMID: 19014601 PMCID: PMC2614436)
    </doc>
    <author email="ville.rantanen@helsinki.fi">Ville Rantanen</author>
    <category>Image Analysis</category>
    <launcher type="matlab">
        <argument name="file" value="execute.m" />
        <argument name="source" value="imageintensity.m" />
        <argument name="source" value="imagesinglefeatures.m" />
        <argument name="source" value="CalculateGabor.m" />
        <argument name="source" value="CalculateHaralick.m" />
    </launcher>
    <requires>Matlab</requires>
    <inputs>
        <input name="images" type="ImageList"><doc>A directory with grayscale image files.</doc>
        </input>
        <input name="mask" type="ImageList"><doc>A directory with BW mask image files.</doc>
        </input>
        <input name="names" type="ImageList" optional="true"><doc>A directory with the original image files, for naming the table. If omitted, names from "mask" are used.</doc>
        </input>
        <input name="masklist" type="MaskList" optional="true"><doc>A MaskList source for the objects. If given, the mask input is not used for objects, but must be given for file names.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="edges" type="ImageList"><doc>A directory with BW mask image files with the erosion mask edge.</doc></output>
        <output name="out" type="CSV"><doc>List of feature values.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="connectivity" type="int" default="4"><doc>Mask object connectivity. 4 or 8. Does not apply if <var>masklist</var> input is used</doc> </parameter>
        <parameter name="erodeWidth" type="float" default="0"><doc>Erosion width to calculate margination in pixels or percentage of object length. Erosion width can be negative, which means the object will be dilated.</doc> </parameter>
        <parameter name="isRatio" type="boolean" default="false"><doc>Eroding width is expressed in percentage of object width instead absolute pixels.</doc> </parameter>
        <parameter name="prefix" type="string" default=""><doc>Prefix string for data headers in the result table. Used when reading the same images with multiple parameters and then joining the results.</doc> </parameter>
        <parameter name="percentile" type="float" default="1"><doc>Percentile to read from object intensities. Value 1 equals 1% and 99% will be measured as Low %1 and High 1%.</doc> </parameter>
        <parameter name="textureScale" type="float" default="1"><doc>Scaling factor for texture features</doc> </parameter>
    </parameters>
</component>


