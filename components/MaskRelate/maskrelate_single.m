function [dataout,imout]=maskrelate_single(cf,parentid,childid,overlap,emptyParents)

parentmask=im2double(imread(parentid));
if size(parentmask,3)~=1
    writeerror(cf,[parentid ' Source file not gray scale'])
    return
end
childmask=im2double(imread(childid));
if size(childmask,3)~=1
    writeerror(cf,[childid ' Source file not gray scale'])
    return
end

if (or(size(childmask,1)~=size(parentmask,1),size(childmask,2)~=size(parentmask,2)))
    disp(size(childmask))
    disp(size(parentmask))
    writeerror(cf,[childid ' and ' parentid ' not same dimensions'])
    return
end

parentlabel=bwlabel(parentmask,4);
parents=max(parentlabel(:));
childlabel=bwlabel(childmask,4);
children=max(childlabel(:));

imout=zeros([size(parentmask) 3]);
imout(:,:,1)=parentmask;
imout(:,:,2)=childmask;
dataout=zeros(0,6);
if children>0
    dataout=zeros(children,6);
    for c=1:children
        this.parent=parentlabel(childlabel==c);
        switch overlap
            case 'any'
                this.parentid=mode(this.parent(this.parent>0));
                if isnan(this.parentid)
                    this.parentid=0;
                end
            case 'discard'
                parentids=unique(this.parent);
                if numel(parentids)==1
                    this.parentid=parentids;
                else
                    this.parentid=0;
                end
            otherwise
                this.parentid=mode(this.parent);
        end

        if this.parentid==0
            this.parentCoverage=0;
            this.childCoverage=0;
            this.intersectionCoverage=0;
        else
            this.childArea=sum(sum( childlabel==c ));
            this.parentArea=sum(sum( parentlabel==this.parentid ));
            this.joinedArea=sum(sum( childlabel==c | parentlabel==this.parentid ));
            this.intersectionArea=sum(sum( childlabel==c & parentlabel==this.parentid ));
            this.parentCoverage=this.intersectionArea / this.parentArea;
            this.childCoverage=this.intersectionArea / this.childArea;
            this.intersectionCoverage=this.intersectionArea / this.joinedArea;
        end
        
        dataout(c,:)=[c c this.parentid this.parentCoverage this.childCoverage this.intersectionCoverage];
    end
else
    c=0;
end
if emptyParents
% Check for missing parents => add the number of each parent that doesn't yet exist in the data
    parentlist=1:parents;
    existlist=unique(dataout(:,3));
    missing_parents=parentlist(~ismember(parentlist,existlist));
    zeroes=repmat(0,[numel(missing_parents) 1]);
    dataout=[dataout; c+(1:numel(missing_parents))' zeroes missing_parents' zeroes zeroes zeroes];
end
