function maskrelate(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file

tic
parentdir=getinput(cf,'parent');
childdir=getinput(cf,'child');
imorigdir=getinput(cf,'names');

outfile=getoutput(cf,'out');
outdir=getoutput(cf,'overlap');
method=lower(getparameter(cf,'method','string'));

if any(strcmp({'most','any','discard'},method))
   disp(['Using method: ' method]);
else
    writeerror(cf,['No overlap method: ' method]);
    return
end

emptyParents=getparameter(cf,'emptyParents','boolean');

parentlist=getimagelist(parentdir);
childlist=getimagelist(childdir);
if numel(imorigdir)<2
    origparentdir=parentdir;
    origparentlist=parentlist;
    origchilddir=childdir;
    origchildlist=childlist;
else
    origparentlist=getexoticimagelist(imorigdir);
    origchildlist=origparentlist;
end

if numel(parentlist)==0
    writeerror(cf,'No images in source directory')
    return
end
if numel(parentlist)~=numel(childlist)
    writeerror(cf,'Source directories have different number of images')
    return
end

mkdir(outdir);
writeout.columnheads={'ChildId','ParentId','File','Row','Child','Parent','ParentCoverage','ChildCoverage','OverlapCoverage'};
for file=1:numel(parentlist)
    parentid=[parentdir filesep parentlist{file}];
    childid=[childdir filesep childlist{file}];
    disp(['Read image: ' parentlist{file}]);
    disp(['       and: ' childlist{file}  ' (' num2str(file) '/' num2str(numel(parentlist)) ')']);
    [dataout,imout]=maskrelate_single(cf,parentid,childid,method,emptyParents);
    dataout=num2csvcell(dataout);
    objs=size(dataout,1);
    dataoutname=repmat(origparentlist(file),[objs 1]);
    poutname=repmat(origparentlist(file),[objs 1]);
    coutname=repmat(origchildlist(file),[objs 1]);
    dataoutname=[ strcat(coutname,'_',dataout(:,2)) ...
        strcat(poutname,'_',dataout(:,3)) dataoutname];  % Add the name_obj  id string
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile,writeout);
    if (strcmpi(origparentlist{file}((end-2):end),'png'))
        imwrite(imout,fullfile(outdir,origparentlist{file}));
    else
        imwrite(imout,fullfile(outdir,[origparentlist{file} '.png']));
    end
end
close all
toc
end

