function mapped=mapstack(imap, srcdir) 
  stack=readstack(srcdir);
  
  imap(imap<1)=1;
  imap(imap>size(stack,3))=size(stack,3);
  
  [y,x]=meshgrid(1:size(imap,1), 1:size(imap,2));
  mapIndex=sub2ind(size(stack),x,y,imap);
  mapped=stack( mapIndex );
  %~ mapped=zeros(size(imap));
  %~ for x=1:size(mapped,2)
    %~ for y=1:size(mapped,1)
      %~ mapped(y,x)=stack(y,x, imap(y,x) );
    %~ end
  %~ end
end

