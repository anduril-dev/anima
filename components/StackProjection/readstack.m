function stack=readstack(srcdir)
% Read a folder of images as stack: make sure output is grayscale.

  stackfilesdir=natsort(getimagelist(srcdir));
  stack=[];
  for filenr=1:numel(stackfilesdir)
    if numel(stack)==0
      stack=max(im2double(imread([srcdir filesep stackfilesdir{filenr}])),[],3);
      z=1;
    end 
    stack(:,:,z)=max(im2double(imread([srcdir filesep stackfilesdir{filenr}])),[],3);
    z=z+1;
  end
