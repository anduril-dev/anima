
addpath('lib')
addpath('../../lib/matlab/')
try
  cf=readcommandfile(commandfile); 
  outDir=getoutput(cf,'out');
  mapDir=getoutput(cf,'map');
  mappedDir=getoutput(cf,'mapped');
  mkdir(outDir);
  mkdir(mapDir);
  mkdir(mappedDir);
  inDir=getinput(cf,'in');
  inFiles=getimagelist(inDir);
  inBase=natsort(inFiles); 
  inBase=inBase{1};
  inFiles=numel(inFiles);
  isArray=inputdefined(cf,'toMap');
  if isArray
    inArray=readarray(cf,'toMap');
    outArray=inArray;
    for arr=1:size(inArray.data,1)
      arrFiles=getimagelist(inArray.data{arr,2});
      if inFiles ~= numel(arrFiles)
        writeerror(cf,['Image counts do not match: ' inArray.data{arr,2}]);
        error(['Image counts do not match: ' inArray.data{arr,2}]);
      end
      mkdir([mappedDir filesep inArray.data{arr,1}]);
      outArray.data{arr,2}=[mappedDir filesep inArray.data{arr,1}];
    end
    writearray(cf,'mapped',outArray);
  end
  dilateValue=getparameter(cf,'dilate','float');
  blurValue=getparameter(cf,'blur','float');
  method=getparameter(cf,'method','string');

  disp(['Reading input stack. Slices: ' num2str(inFiles)]);
  stack=readstack(inDir);
  dilator=strel('disk',abs(dilateValue),0);
  for z=1:size(stack,3)
    if dilateValue>0
      stack(:,:,z)=imdilate(stack(:,:,z),dilator);
    end
    if dilateValue<0
      stack(:,:,z)=imerode(stack(:,:,z),dilator);
    end
    if dilateValue~=0
        disp([num2str(z) '/' num2str(size(stack,3))])
    end
  end
  
  doMap=0;
  switch method
    case 'maximum'
      [values,iIndex]=max(stack,[],3);
      doMap=1;
    case 'mean'
      values=mean(stack,3);
    case 'median'
      values=median(stack,3);
    case 'minimum'
      [values,iIndex]=min(stack,[],3);
      doMap=1;
    otherwise
      writeerror(cf,'Method was not recognized')
      return
  end
  
  if doMap
    if blurValue ~= 0
        iIndex=round(imgaussfilt(iIndex,blurValue));
    end
    iIndex( iIndex<1 ) = 1;
    iIndex( iIndex>inFiles ) = inFiles;
    imwrite(mat2gray(iIndex), [mapDir filesep inBase '.png']);
    values=mapstack(iIndex, inDir);
  end
  imwrite(values, [outDir filesep inBase '.png']);
  % Map the array
  if doMap
    if isArray
    for arr=1:size(inArray.data,1)
      disp(['Mapping Array: ' inArray.data{arr,1}])
      values=mapstack(iIndex, inArray.data{arr,2});
      imwrite(values, [mappedDir filesep inArray.data{arr,1} filesep inBase '.png']);
    end
    end
  end
  
catch me 
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;

