
source "$ANDURIL_HOME/lang/bash/functions.sh"

FARSIGHTPATH=$( readlink -f ../../lib/Farsight )

export PATH=$PATH:$FARSIGHTPATH/bin

# make the list separator a rowchange
IFS=$'\n' 

indir=$( getinput in )

maskdir=$( getoutput mask )
perimdir=$( getoutput perimeter )

SNbin=$( getparameter binary )
magickbin="convert"
timeout=$( getparameter timeout )
continueNext=$( [ $( getparameter continueNext ) = true ] && echo 1 || echo 0 )

params="high_sensitivity	 :	$( [ $( getparameter highSensitivity ) = true ] && echo 1 || echo 0 )
LoG_size	         :	30
min_scale	         :	$( getparameter minScale )
max_scale	         :	$( getparameter maxScale )
xy_clustering_res	 :	$( getparameter xyClusteringRes )
z_clustering_res	 :	$( getparameter zClusteringRes )
finalize_segmentation	 :	$( [ $( getparameter finalizeSegmentation ) = true ] && echo 1 || echo 0 )
sampling_ratio_XY_to_Z   :	$( getparameter samplingRatio )
Use_Distance_Map	 :	$( [ $( getparameter UseDistanceMap ) = true ] && echo 1 || echo 0 )
refinement_range	 :	$( getparameter refinementRange )"

tempdir=$( gettempdir )

CONVERT=$( which $magickbin )
if ( "$CONVERT" -version | grep -i imagemagick )
    then writelog $( "$CONVERT" -version )
else
    writeerror "ImageMagick $magickbin not found. ( $CONVERT )"
    exit 1
fi
SN=$( which $SNbin )
if ( "$SN" | grep -i usage1 ); then 
    writelog "segment_nuclei found."
    [[ -f "$FARSIGHTPATH/VERSION" ]] && writelog $( cat "$FARSIGHTPATH/VERSION" )
else
    writeerror "FARSight segment_nuclei not found. ( $SNbin )"
    exit 1
fi

[[ "$timeout" = "0" ]] || {
    TIMEOUT=$( which timeout 2> /dev/null )
}
# list files
if [ -d "$indir" ]
then infiles=( $( ls -1 "$indir" ) ) || echo "error reading input dir" >> "$errorfile"
fi

# create directories
mkdir "${maskdir}" || writeerror "error creating directory ${maskdir}"
mkdir "${perimdir}" || writeerror "error creating directory ${perimdir}"

writelog "$params"
# loop over images
for (( f=0; f<${#infiles[@]}; f++ ))
    do writelog "$(( $f+1 )) / ${#infiles[@]} : ${infiles[$f]}"
    # Convert to 8bit grayscale
    "$CONVERT" "${indir}/${infiles[$f]}" -type GrayScale -colorspace Gray -verbose PNG8:"$tempdir"/gray.png
    echo "$params" > "${tempdir}/segment_params.txt"
    [[ -z "$TIMEOUT" ]] && {
        "$SN" "${tempdir}/gray.png" "${tempdir}/labels.tif" "${tempdir}/segment_params.txt" >> "${tempdir}/log"
        EC=$?
    }
    [[ -z "$TIMEOUT" ]] || { 
        $TIMEOUT -s 9 $timeout "$SN" "${tempdir}/gray.png" "${tempdir}/labels.tif" "${tempdir}/segment_params.txt" >> "${tempdir}/log"
        EC=$?
    }
    # Segmentation fails
    [[ $EC -eq 124 ]] && writelog "Timeout of $timeout reached"
    [[ $EC -eq 139 ]] && writelog "Timeout of $timeout reached"
    [[ -f "${tempdir}/labels.tif" ]] || {
      [[ $continueNext -eq 1 ]] && {
        "$CONVERT" "${tempdir}/gray.png" -fx 0 +compress "${tempdir}/labels.tif"
         writelog "Segmenting \"${infiles[$f]}\" produced zero result, creating empty mask. Process exited with $EC"
      }
    }
    [[ -f "${tempdir}/labels.tif" ]] || {
         writeerror "Segmenting \"${infiles[$f]}\" failed!"
         exit 1
    }
    "$CONVERT" "${tempdir}/labels.tif" -morphology EdgeIn Disk:1 -threshold 0 "${perimdir}/${infiles[$f]}" 2>> "$logfile"
    "$CONVERT" -compose Multiply \( "${tempdir}/labels.tif" -threshold 0 \) \
            \( "${perimdir}/${infiles[$f]}" -negate \) -composite "${maskdir}/${infiles[$f]}"

    rm -f "${tempdir}/"*dat
    rm -f "${tempdir}/"*tif
    rm -f "${tempdir}/"*png
    if [ ! -f "${maskdir}/${infiles[$f]}" ]
    then writeerror "Can not find result of \"${infiles[$f]}\" !"
         exit 1
    fi
done

