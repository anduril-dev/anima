function dataout=imagecorrfunc(cf,id1,id2,idmask,origimglist,file,method,outdir);

dataout=nan(1,5);

gray1=im2double(imread(id1));
gray2=im2double(imread(id2));
if size(gray1,3)~=1
    writeerror(cf,['Source gray file not gray scale: ' id1])
    return
end
if size(gray2,3)~=1
    writeerror(cf,['Source gray file not gray scale: ' id2])
    return
end
if (sum(size(gray1)==size(gray2))~=2)
    writeerror(cf,['Channel 1 image and channel 2 image not of same size ' id1 ' vs. ' id2])
    return
end
if (idmask==0)
    mask=ones(size(gray1));
else
    mask=im2double(imread(idmask));
    if size(mask,3)~=1
        writeerror(cf,['Source mask file not gray scale: ' idmask])
        return
    end
    if (sum(size(gray1)==size(mask))~=2)
        writeerror(cf,['Channel 1 image and mask image not of same size ' id1 ' vs. ' idmask])
        return
    end
end

props=regionprops(bwlabel(mask,4),'FilledImage','Centroid','BoundingBox');
%morph=struct([]);
objs=size(props,1);

if objs>0
    switch method
    case 'image/pixel'
        objnums=1;
        objvals1=gray1(mask==1)*255;
        objvals2=gray2(mask==1)*255;
        correlations=imagecorrelations(objvals1,objvals2);
        create_graph(objvals1/255,objvals2/255,outdir,origimglist{file},method);
    case 'image/object'    
        objnums=1;
        coords=reshape([props.Centroid], [2 objs])';
        objvals1=zeros(objs,1);
        objvals2=zeros(objs,1);
        for obj=1:objs
            cds=ceil(props(obj).BoundingBox);  % bounding box coordinates (x,y,w,h)
            fill=double(props(obj).FilledImage);
            fill(fill==0)=nan;
            objimg1=fill.*gray1(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
            objimg2=fill.*gray2(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
            objvals1(obj)=mean(objimg1(~isnan(objimg1))*255);
            objvals2(obj)=mean(objimg2(~isnan(objimg2))*255);
        end
        correlations=imagecorrelations(objvals1,objvals2);
        create_graph(objvals1/255,objvals2/255,outdir,origimglist{file},method);
    case 'objects/pixel'
        objnums=(1:objs)';
        coords=reshape([props.Centroid], [2 objs])';
        correlations=zeros(objs,5);
        for obj=1:objs
            cds=ceil(props(obj).BoundingBox);  % bounding box coordinates (x,y,w,h)
            fill=double(props(obj).FilledImage);
            fill(fill==0)=nan;
            objimg1=fill.*gray1(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
            objimg2=fill.*gray2(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
            objvals1=objimg1(~isnan(objimg1))*255;
            objvals2=objimg2(~isnan(objimg2))*255;
            correlations(obj,:)=imagecorrelations(objvals1,objvals2);
        end
        objvals1=gray1(mask==1);
        objvals2=gray2(mask==1);
        create_graph(objvals1,objvals2,outdir,origimglist{file},method);
    otherwise
        writeerror(cf,'Method not recognized. ')
        return
    end    
    dataout=[objnums correlations];
end

end %Function (main)

function corrs=imagecorrelations(values1,values2)
% values1 and values2 0.0-255.0 intensity values, but double format.
% Ref: http://support.svi.nl/wiki/ColocalizationTheory

    corrs=zeros(1,5);
    mean1=mean(values1);
    mean2=mean(values2);

    pearson=sum( (values1-mean1).*(values2-mean2) ) / sqrt( sum( (values1-mean1).^2 ) * sum( (values2-mean2).^2 ) ); 
    corrs(1)=pearson;

    %% manders'
    k1=sum(values1.*values2)/sum(values1.^2);
    k2=sum(values1.*values2)/sum(values2.^2);

    corrs(2)=k1;
    corrs(3)=k2;

    %% overlap
    overlap=sum(values1.*values2) / sqrt(sum(values1.^2) * sum(values2.^2));
    corrs(4)=overlap;

    %% mutual information
    corrs(5)=mutual_information(values1,values2);

end

function I=mutual_information(A,B)
    %% mutual information

    %   jfd, 15-11-2006
    %        01-09-2009, added case of non-double images
    %        24-08-2011, speed improvements by Andrew Hill
    L=256;
    A=double(A); 
    B=double(B); 
     
    na = hist(A(:),L); 
    na = na/sum(na);

    nb = hist(B(:),L); 
    nb = nb/sum(nb);

    n2 = hist2(A,B,L); 
    n2 = n2/sum(n2(:));

    I=sum(minf(n2,na'*nb)); 

end

function create_graph(v1,v2,outdir,filename,method)
    h=figure(1); set(h,'Visible','off'); %set(h,'PaperOrientation','landscape'); 
    %title(stitle)
    set(h,'PaperUnits','points'); 
    set(h,'PaperPositionMode','auto')
    %set(h,'PaperOrientation','landscape');
    
    hold off
    ax1=gca;
    set(ax1,'FontSize',22)
    %set(ax1,'Xtick',linspace(0.5,(binx)*4+0.5,5));
    %set(ax1,'XtickLabel', num2str(linspace(xmin,xmax,5)',2))
    %set(ax1,'Ytick',linspace(.5,(biny)*4+0.5,5));
    %set(ax1,'YtickLabel', num2str(linspace(ymin,ymax,5)',2))
    set(ax1,'LineWidth',1.5);
    plot(v1,v2,'.k');
    axis([0 1 0 1]);
    ylabel('Channel 2');
    xlabel('Channel 1');
    fformat='png';
    
    caption='Pixel intensities from two channels';
    writefigurelatex(outdir,h,[filename '_plot.' lower(fformat)],fformat,caption);
    close(h);

end

function y=minf(pab,papb)
    I=find(papb(:)>1e-12 & pab(:)>1e-12); % function support 
    y=pab(I).*log2(pab(I)./papb(I));
end

function n=hist2(A,B,L) 
    %HIST2 Calculates the joint histogram of two images or signals
    %
    %   n=hist2(A,B,L) is the joint histogram of matrices A and B, using L
    %   bins for each matrix.
    %
    %   See also MI, HIST.

    %   jfd, 15-11-2006, working
    %        27-11-2006, memory usage reduced (sub2ind)
    %        22-10-2008, added support for 1D matrices
    %        01-09-2009, commented specific code for sensorimotor signals
    %        24-08-2011, speed improvements by Andrew Hill

    ma=min(A(:)); 
    MA=max(A(:)); 
    mb=min(B(:)); 
    MB=max(B(:));

    % Scale and round to fit in {0,...,L-1} 
    A=round((A-ma)*(L-1)/(MA-ma+eps)); 
    B=round((B-mb)*(L-1)/(MB-mb+eps)); 
    n=zeros(L); 
    x=0:L-1; 
    for i=0:L-1 
        n(i+1,:) = histc(B(A==i),x,1); 
    end
end
