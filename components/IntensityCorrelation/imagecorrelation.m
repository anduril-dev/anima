function imagecorrelation(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file
tic
imdir1=getinput(cf,'image1');
imdir2=getinput(cf,'image2');
maskdir=getinput(cf,'mask');
imorigdir=getinput(cf,'names');
outdir=getoutput(cf,'plot');
method=getparameter(cf,'method','string');

if numel(maskdir)<2
    usemask=0;
else
    usemask=1;
end
outfile=getoutput(cf,'out');

imglist1=getimagelist(imdir1);
imglist2=getimagelist(imdir2);
masklist=getimagelist(maskdir);
if numel(imorigdir)<2
    imorigdir=imdir1;
    origimglist=imglist1;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist1)~=numel(origimglist)
    writeerror(cf,'Intensity image and original name list directories have different number of images')
    return
end
if numel(imglist1)~=numel(imglist2)
    writeerror(cf,'Channel 1 image and channel 2 image directories have different number of images')
    return
end
if usemask
    if numel(imglist1)~=numel(masklist)
        writeerror(cf,'Channel 1 image and mask image directories have different number of images')
        return
    end
end
mkdir(outdir);

% methods: 'image/pixel', 'image/object', 'objects/pixel'
switch method
    case 'image/pixel'
        disp('Method: Image correlation per pixels')
    case 'image/object'
        disp('Method: Image correlation per object mean')
        if (~usemask)
            writeerror(cf,'Method requires mask image.')
            return
        end
    case 'objects/pixel'
        disp('Method: Object correlation per pixel')
        if (~usemask)
            writeerror(cf,'Method requires mask image.')
            return
        end
    otherwise
        writeerror(cf,'Method not recognized. ')
        return
    
end

writeout.columnheads={'RowId','File','Object','Pearsons','Manders 1','Manders 2', ...
    'Overlap','MutualInformation'};
for file=1:numel(imglist1)
    id1=[imdir1 filesep imglist1{file}];
    id2=[imdir2 filesep imglist2{file}];
    if usemask
        idmask=[maskdir filesep masklist{file}];
    else
        idmask=0;
    end
    disp(['Reading: ' imglist1{file} ', ' imglist2{file} ' (' num2str(file) '/' num2str(numel(imglist1)) ')']);
    dataout=num2csvcell(imagecorrfunc(cf,id1,id2,idmask,origimglist,file,method,outdir));
    objs=size(dataout,1);
    dataoutname=repmat({origimglist{file}},[objs 1]);
    dataoutname=[ strcat(dataoutname,'_',dataout(:,1)) dataoutname];
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile, writeout);
end

close all
toc
end
