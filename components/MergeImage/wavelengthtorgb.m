function rgb=wavelengthtorgb(WL)
% convert wavelength, letter RGBCMYK or hexadecimal 24bit value in to double RGB triplet
%


switch length(WL)
    case 1
        valuetype='letter';
    case 3
        valuetype='wavelength';
    case 6
        valuetype='hex';
    otherwise
        error('anduril:ParameterError',['"' WL '" not recognized color']);    
end

switch valuetype
    case 'wavelengthOLD'
    % Wavelength conversion:
    % Code from FORTRAN program:  RGB VALUES FOR VISIBLE WAVELENGTHS   by Dan Bruton (astro@tamu.edu)
    %
    %      This program can be found at
    %      http://www.physics.sfasu.edu/astro/color.html
        WL=getvariabletype(WL,'float');
        if and(WL>380, WL<440)
            R = -1*(WL-440)/(440-380);
            G = 0;
            B = 1;
        elseif and(WL>=440,WL<490)
            R = 0;
            G = (WL-440)/(490-440);
            B = 1;
        elseif and(WL>=490,WL<510)
            R = 0;
            G = 1;
            B = -1*(WL-510)/(510-490);
        elseif and(WL>=510,WL<580)
            R = (WL-510)/(580-510);
            G = 1;
            B = 0;
        elseif and(WL>=580,WL<645)
            R = 1;
            G = -1*(WL-645)/(645-580);
            B = 0;
        elseif and(WL>=645,WL<780)
            R = 1;
            G = 0;
            B = 0;
        else
            R=0;
            G=0;
            B=0;
        end
        rgb=[R,G,B];
    case 'wavelength'
        WL=getvariabletype(WL,'float');
        % My beautified version of  WL to RGB
        % following http://en.wikipedia.org/wiki/CIE_1931_color_space#Color_matching_functions
        % but with simple gaussians
        if (WL>780)
            rgb=[0 0 0];
            return
        end
        B=normpdf(0:780, 440, 60)';
        G=normpdf(0:780, 548, 60)';
        R=normpdf(0:780, 640, 60)' + 0.25*normpdf(0:780, 380, 35)';
        cm=[R G B];
        %mmax=0.72*max(cm,[],2);
        %gmax=max(mmax);
        gmax=0.004700869870732;
        cm=cm./gmax;
        rgb=min(cm(WL,:),1);
        
    case 'letter'
        switch upper(WL)
            case 'R'
                rgb=[1 0 0];
            case 'G'
                rgb=[0 1 0];
            case 'B'
                rgb=[0 0 1];
            case 'C'
                rgb=[0 1 1];
            case 'M'
                rgb=[1 0 1];
            case 'Y'
                rgb =[1 1 0];
            case 'K'
                rgb=[0 0 0];
            case 'W'
                rgb=[1 1 1];
            otherwise
                error('anduril:ParameterError',['"' WL '" not recognized color']);
        end
    case 'hex'
        rgb=hex2dec({WL(1:2),WL(3:4),WL(5:6)})'/255;
end
