function imagergbmerge(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file
tic
ind=1;
imdir{1}='NA';
if inputdefined(cf,'in')
    if strcmp(getparameter(cf,'colors','string'),'')
        writeerror(cf,'"colors" parameter must be defined when using array input')
        return
    end
    arraytable=readarray(cf,'in');
    arraydirs=getcellcol(arraytable,'File');
    arraycolors=textscan(getparameter(cf,'colors','string'),'%s','Delimiter',',');
    arraycolors=arraycolors{1};
    %arraycolors=cellfun(@(x) getvariabletype(strtrim(x),'string'),arraycolors,'UniformOutput',false);
    if numel(arraycolors)~=numel(arraydirs)
        writeerror(cf,'Number of array inputs does not match with colors parameter elements')
    end
    for src=1:numel(arraydirs)
        imdir{ind}=arraydirs{src};
        color{ind}=arraycolors{src};
        ind=ind+1;
    end
end
stretch=getparameter(cf,'stretch','boolean');
fformat=getparameter(cf,'format','string');
fformatlen=numel(fformat)-1;

outdir=getoutput(cf,'out');
mkdir(outdir);

images=zeros(numel(imdir),1);
for list=1:numel(imdir)
    imglist{list}=getimagelist(imdir{list});
    images(list)=numel(imglist{list});
end
if any(images~=images(1))
    writeerror(cf,'One or more of the input folders contain different number of images');
    return
end

for file=1:numel(imglist{1})
    out=0;
    for list=1:numel(imdir)
        id=[imdir{list} filesep imglist{list}{file}];
%        writelog(cf,['Read image: ' imglist{list}{file}]);
        gray=im2double(imread(id));
        dimstr=sprintf('%dx',size(gray));
        disp(['Read image: ' imglist{list}{file} ' ' dimstr(1:(end-1)) ' Color: ' color{list} ' (' num2str(file) '.' num2str(list) '/' num2str(numel(imglist{list})) ')']);
        
        if ndims(gray)==3
            if size(gray,3)==4
                gray=gray(:,:,1:3);
            end
            if size(gray,3)==2
                gray=repmat(gray(:,:,1),[1 1 3]);
            end
        end

        if stretch
            if numel(unique(gray(:)))==1
                gray(:)=0;
                disp(['Source completely flat!: ' imglist{list}{file}])
            else
                gray=gray-min(gray(:));
                gray=gray./max(gray(:));
            end
        end

        if ndims(gray)==2
            gray=repmat(gray,[1 1 3]);
        end
        [h w c]=size(gray);

        if list==1
            out=zeros([h w 3]);
        end
        
        if strncmp(color{list},'colormap:',9)
        % Use colormap transformation
            try
                gray=gray2ind(rgb2gray(gray),256);
                out=out+ind2rgb(gray, [0 0 0; eval([color{list}(10:end) '(254)'])]);
            catch colorme
                writeerror(cf,['Error: Could not use colormap: ' color{list}(10:end)])
                disp(['Error: Could not use colormap: ' color{list}(10:end)])
            end
        else
        % Use color letter(/hex) of wavelength
            colval=reshape(wavelengthtorgb(color{list}),[1 1 3]);
            try
                out=out+gray.*repmat(colval,[h w 1]);
            
            catch plusme
                writeerror(cf,'Error: most likely Image sizes do not match')
                writeerror(cf,id)
                disp('Error: most likely Image sizes do not match')
                disp(id)
            end    
        end        
    end
    if strcmpi(imglist{1}{file}(end-(fformatlen):end),fformat)
        extstr='';
    else
        extstr=['.' fformat];
    end
    imwrite(out,[outdir filesep imglist{1}{file} extstr])
end

toc
end
