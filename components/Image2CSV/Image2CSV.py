#!/usr/bin/python
import sys,os
import anduril.imagetools as ait
from anduril.args import *
import anduril

param_bitDepth=8

def write_gray_no_mask(writer,filename,im):
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            writer.writerow({'File': filename, 'X':x, 'Y':y, 'Intensity':im[y,x]})

def write_color_no_mask(writer,filename,im):
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            try:
                foo_iter = iter(im[y,x])
                (R,G,B)=im[y,x]
            except:
                (R,G,B)=(im[y,x],im[y,x],im[y,x])
            writer.writerow({'File': filename, 'X':x, 'Y':y, 'R':R, 'G':G, 'B':B})

def write_gray_with_mask(writer,filename,im,mask,noIncludeMask):
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            if (mask_im[y,x]==1):
                intVal=im[y,x]
            else:
                if noIncludeMask:
                    continue
                intVal='NA'
            writer.writerow({'File': l,
                             'X':x,
                             'Y':y,
                             'Intensity':intVal})

def write_color_with_mask(writer,filename,im,mask,noIncludeMask):
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            if (mask_im[y,x]==1):
                try:
                    foo_iter = iter(im[y,x])
                    (R,G,B)=im[y,x]
                except:
                    (R,G,B)=(im[y,x],im[y,x],im[y,x])
            else:
                if noIncludeMask:
                    continue
                (R,G,B)=('NA','NA','NA')
            writer.writerow({'File': l,
                             'X':x,
                             'Y':y,
                             'R':R,
                             'G':G,
                             'B':B})

imagelist=ait.get_imagelist(input_in)
if input_mask!=None:
    masklist=ait.get_imagelist(input_mask)
if color:
    fieldnames=['File','X','Y','R','G','B']
    flatten=0
else:
    fieldnames=['File','X','Y','Intensity']
    flatten=1
writer=anduril.TableWriter(out,fieldnames=fieldnames)
for i,l in enumerate(imagelist):
    write_log( "%s %d/%d"% (l, i+1, len(imagelist)) )
    im=ait.load_image( os.path.join( input_in, l ),flatten=flatten, keeptype=not param_convertFloat)
    #*( 2**param_bitDepth -1)
    if input_mask!=None:
        mask_im=ait.load_image( os.path.join( input_mask, masklist[i] ),flatten=1)
        if (mask_im.shape[0]!=im.shape[0] and mask_im.shape[1]!=im.shape[1]):
            write_error('Mask and Image are of different size.')
            sys.exit(1)

    if input_mask==None:
        if color:
            write_color_no_mask(writer,l,im)
        else:
            write_gray_no_mask(writer,l,im)
    else:
        if color:
            write_color_with_mask(writer,l,im,mask,not param_includeNonMask)
        else:
            write_gray_with_mask(writer,l,im,mask,not param_includeNonMask)
