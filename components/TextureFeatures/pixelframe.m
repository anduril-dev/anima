function [allx,ally]=pixelframe(frames,np)
% PIXELFRAME  Plot ellipse feature frame on bitmap
%     + FRAME(1:2)   center
%     + FRAME(3:5)   S11, S12, S22 such that ELLIPSE = {x: x' inv(S) x = 1}.
%

% AUTORIGHTS
% Copyright (C) 2007-10 Andrea Vedaldi and Brian Fulkerson
%
% This file is part of VLFeat, available under the terms of the
% GNU GPLv2, or (at your option) any later version.
% Edited for 

% number of vertices drawn for each frame
%np        = 40 ;
 
[D,K] = size(frames) ;

frames    = frame2oell(frames) ;
thr = linspace(0,2*pi,np) ;

% allx and ally are nan separated lists of the vertices describing the
% boundary of the frames
allx = nan*ones(1, np*K+(K-1)) ;
ally = nan*ones(1, np*K+(K-1)) ;

% vertices around a unit circle
Xp = [cos(thr) ; sin(thr) ;] ;
for k=1:K
  % frame center
	xc = frames(1,k) ;
	yc = frames(2,k) ;
  % frame matrix
    A = reshape(frames(3:6,k),2,2) ;
  % vertices along the boundary
    X = A * Xp ;
    X(1,:) = X(1,:) + xc ;
    X(2,:) = X(2,:) + yc ;
  % store 
	allx((k-1)*(np+1) + (1:np)) = round(X(1,:)) ;
	ally((k-1)*(np+1) + (1:np)) = round(X(2,:)) ;
end
end % function
% --------------------------------------------------------------------
function eframes = frame2oell(frames)
% FRAMES2OELL  Convert generic frame to oriented ellipse
%   EFRAMES = FRAME2OELL(FRAMES) converts the frames FRAMES to
%   oriented ellipses EFRAMES. This is useful because many tasks are
%   almost equivalent for all kind of regions and are immediately
%   reduced to the most general case.

%
% Determine the kind of frames
%
[D,K] = size(frames) ;

eframes = zeros(6,K) ;

%
% Do converison
%
eframes(1:2,:) = frames(1:2,:) ;
eframes(3:6,:) = mapFromS(frames(3:5,:)) ;

end    

% --------------------------------------------------------------------
function A = mapFromS(S)
% --------------------------------------------------------------------
% Returns the (stacking of the) 2x2 matrix A that maps the unit circle
% into the ellipses satisfying the equation x' inv(S) x = 1. Here S
% is a stacked covariance matrix, with elements S11, S12 and S22.

tmp = sqrt(S(3,:)) + eps ;
A(1,:) = sqrt(S(1,:).*S(3,:) - S(2,:).^2) ./ tmp ;
A(2,:) = zeros(1,length(tmp));
A(3,:) = S(2,:) ./ tmp ;
A(4,:) = tmp ;

end