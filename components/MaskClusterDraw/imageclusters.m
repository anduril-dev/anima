function imageclusters(cf)
%
%
% cf = Anduril command file
tic
imdir=getinput(cf,'images');
maskdir=getinput(cf,'mask');
origdir=getinput(cf,'names');
clustfile=getinput(cf,'clusters');

outdir=getoutput(cf,'out');

imglist=getimagelist(imdir);
masklist=getimagelist(maskdir);
if strcmp(origdir,'')
    origlist=masklist;
    origdir=maskdir;
else
    origlist=getexoticimagelist(origdir);
end

clustcolname=getparameter(cf,'clusterId','string');
filecolname=getparameter(cf,'fileId','string');
objectcolname=getparameter(cf,'objectId','string');
usecolor=getparameter(cf,'useColor','boolean');
fill=getparameter(cf,'fill','boolean');
altwid=getparameter(cf,'alternateWidth','boolean');
trail=getparameter(cf,'leaveTrail','boolean');
colorfunc=getparameter(cf,'colorFunction','string');

if numel(imglist)>0
if numel(imglist)~=numel(masklist)
    writeerror(cf,'Intensity and mask image directories have different number of images')
    return
end
end
mkdir(outdir);

clusterdata=readcsvcell(clustfile,0,char(9));

files=getcellcol(clusterdata,filecolname,'string');
origclusters=getcellcol(clusterdata,clustcolname,'guess');
objects=getcellcol(clusterdata,objectcolname,'float');

uniqclust=unique(origclusters)';
remap=0;
if isnumeric(uniqclust)
    if all(uniqclust==1:numel(uniqclust))
        clusters=origclusters;
    else
        remap=1;
    end
else
    remap=1;
end
if remap
    clusters=zeros(size(origclusters));
    if isnumeric(uniqclust)
        for uc=1:numel(uniqclust)
            clusters(origclusters==uniqclust(uc))=uc;
        end
    else
        for uc=1:numel(uniqclust)
            clusters(strcmp(origclusters,uniqclust(uc)))=uc;
        end
    end
end

u=numel(uniqclust);
cmap=eval(colorfunc);
cmap2=[cmap; 0 0 0];

for file=1:numel(masklist)
    idorig=origlist{file};
    idmask=[maskdir filesep masklist{file}];
    % Read mask image
    mask=(im2double(imread(idmask)));
    
    if ndims(mask)>2
        writeerror(cf,'Mask image dimensions exceed 2 (not BW image)');
    end
    imout=zeros(size(mask));

    if numel(imglist)>0
        idimage=[imdir filesep imglist{file}];
        img=im2double(imread(idimage));
        if ndims(img)<3
            img=repmat(img,[1 1 3]);
        end
    else
        img=zeros([size(mask) 3]);
    end
    if ~usecolor
        img=rgb2gray(img);
    end
    disp(['Read image: ' masklist{file} ' (' num2str(file) '/' num2str(numel(masklist)) ')'])
    labels=bwlabel(mask,4);
    filerows=strcmp(files, idorig);
    if sum(filerows)==0
        writelog(cf,['File not found in list: ' idorig]);
    end
    fileclusters=clusters(filerows);
    fileobjects=objects(filerows);
    objs=max(labels(:));
    if trail
        % do not erase past
        if file==1
            labelclass=zeros(size(mask));
        end
    else
        labelclass=zeros(size(mask));
    end
    for o=1:numel(fileobjects)
        if fill
            masktemp=labels==(fileobjects(o));
        else
            if altwid
                masktemp=objperim(labels==(fileobjects(o)),fileclusters(o));
            else
            masktemp=objperim(labels==(fileobjects(o)),0);
            end
        end
        labelclass(masktemp==1)=fileclusters(o);
    end
    if usecolor
        labelclass=uint8(labelclass);
        labelsout=im2double(label2rgb(labelclass,(cmap),'k'));
        
        classlocations=repmat(labelclass>0,[1 1 3]);
        labelsout(classlocations==0)=img(classlocations==0);
        imwrite(im2double(labelsout),[outdir filesep masklist{file}]);
    else
        labelsout=logical(labelclass);
        labelsout(labelsout==0)=img(labelsout==0);
        imwrite(im2double(labelsout),[outdir filesep masklist{file}]);
    end
    
end
close all

toc
end

function perim=objperim(mask,c)
%returns a thick or thin perimeter depending on alternating integer c.
    if mod(c,2)==0
        perim=bwperim(mask);
    else
        perim=imdilate(bwperim(mask),[1 1;1 1]);
    end
end
