addpath('../../lib/matlab/')
try
    cf=readcommandfile(commandfile);  
%-------------------------------------

imdir=getinput(cf,'images');
imorigdir=getinput(cf,'names');
datafile=getinput(cf,'lines');

label=getparameter(cf,'lCol','string');
elabel=getparameter(cf,'eCol','string');
xlabels=getparameter(cf,'xCols','string');
ylabels=getparameter(cf,'yCols','string');

outfile=getoutput(cf,'out');

data=readcsvcell(datafile);
files=getcellcol(data,label,'string');
edges=getcellcol(data,elabel,'float');
xlabels=textscan(xlabels,'%s','Delimiter',',');
ylabels=textscan(ylabels,'%s','Delimiter',',');
coords=floor([getcellcol(data,xlabels{1}{1},'float') getcellcol(data,ylabels{1}{1},'float') getcellcol(data,xlabels{1}{2},'float') getcellcol(data,ylabels{1}{2},'float')]);

imglist=getimagelist(imdir);
if ~inputdefined(cf,'names')
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end
if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Intensity image and origdir list directories have different number of images')
    return
end

writeout.columnheads={'RowId','File',elabel,'Mean','Median','StDev', ...
    'Sum','Fit X2','Fit X1','Fit X0','Fit error'};

warning('off','MATLAB:polyfit:PolyNotUnique');

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    gray=im2double(imread(id));
    if size(gray,3)~=1
        writeerror(cf,['Source gray file not gray scale: ' id])
        return
    end
    imsize=size(gray);
    imrows=strcmp(origimglist{file},files);
    imseg=coords(imrows,:);
    imedges=edges(imrows,:);
    numedges=size(imseg,1);
    feats=zeros(numedges,9);
    for seg=1:numedges
        accuracy=1+max(abs(imseg(seg,1)-imseg(seg,3)),abs(imseg(seg,2)-imseg(seg,4)));
        Xvec=floor(linspace(imseg(seg,1),imseg(seg,3),accuracy));
        Yvec=floor(linspace(imseg(seg,2),imseg(seg,4),accuracy));
        Ivec=sub2ind(imsize,Yvec,Xvec);
        segdata=gray(Ivec); Xseg=1:numel(Ivec);
        feats(seg,1)=imedges(seg);
        feats(seg,2)=mean(segdata);
        feats(seg,3)=median(segdata);
        feats(seg,4)=std(segdata);
        feats(seg,5)=sum(segdata);
        [P,S]=polyfit(Xseg,segdata,2);
        Yseg=polyval(P,Xseg);
        rmserr=sqrt(mean((segdata-Yseg).^2));
        feats(seg,6)=P(1);
        feats(seg,7)=P(2);
        feats(seg,8)=P(3);
        feats(seg,9)=rmserr;
    end
    dataout=num2csvcell(feats);
    dataoutname=repmat(origimglist(file),[numedges 1]);
    dataoutname=[ strcat(dataoutname,'_',dataout(:,1)) dataoutname];  % Add the name_obj  id string
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile,writeout);

end


%-------------------------------------

catch me 
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;  





