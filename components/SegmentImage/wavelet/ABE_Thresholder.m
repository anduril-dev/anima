function [output] = ABE_Thresholder(input,noiseSigma,sigmaMultiplier)

% [output] = ABE_Thresholder(input,noiseSigma,sigmaMultiplier)
% Optional input: sigmaMultiplier (default = 3)
%
% Amplitude-scale-invariant Bayes Estimator
%
% Author: Dirk Padfield, GE Global Research, padfield@research.ge.com
%
% Reference: 
% "Wavelet-Based Image Estimation: An Empirical Bayes Approach Using
% Jeffrey's Noninformative Prior."

if( nargin < 3 )
    sigmaMultiplier = 3;
end

input(input == 0) = eps;
numerator = (input.^2 - sigmaMultiplier*noiseSigma.^2);
numerator(numerator < 0) = 0;
output = numerator./(input);

