function [outImage,detailImagesThresholded,detailImages,approximationImages] = ATrousWaveletBlobDetection(inImage,detailImagesToUse,backgroundMask,sigmaMultiplier)

% [outImage,detailImagesThresholded,detailImages,approximationImages] = ATrousWaveletBlobDetection(inImage,detailImagesToUse,backgroundMask,sigmaMultiplier)
% Optional inputs: detailImagesToUse (a vector), backgroundMask (positive
% values for background and zero for foreground), sigmaMultiplier (default = 3)
%
% This function decomposes the input image using a trous wavelets.  Then,
% using a noise estimate calculated from a representative background patch,
% it thresholds the wavelet coefficients.  Finally, it combines several
% levels to reduce the noise.
%
% Author: Dirk Padfield, GE Global Research, padfield@research.ge.com
%
% Reference: 
% Padfield, D., Rittscher, J., and Roysam, B., 
% "Coupled minimum-cost flow cell tracking for high-throughput quantitative analysis," 
% Medical Image Analysis 15(4), 650-668 (2011) 

inImage = double(inImage);

if( nargin < 2 || isempty(detailImagesToUse))
    detailImagesToUse = [5 6];
end
if( nargin < 3 || isempty(backgroundMask))
    sortedPixels = sort(inImage(:));
    threshold = sortedPixels(round(0.05*numel(inImage)));
    backgroundMask = zeros(size(inImage));
    backgroundMask(inImage <= threshold) = 1;
end
if( nargin < 4 )
    sigmaMultiplier = 3;
end

backgroundMask = logical(backgroundMask);

% Compute the number of levels from the desired detail images.
numberOfLevels = max(detailImagesToUse) + 1;

% Find the a trous wavelets.
[detailImages,approximationImages] = ATrousWavelets(inImage,numberOfLevels);

detailImagesThresholded = zeros(size(detailImages),class(detailImages));
for level = 2:numberOfLevels
    % Threshold detail image W.
    detailImage = detailImages(:,:,level-1);
    noiseSigma = std(detailImage(backgroundMask));
    
    [detailImagesThresholded(:,:,level-1)] = ABE_Thresholder(detailImages(:,:,level-1),noiseSigma,sigmaMultiplier);
    
    % This step gets rid of all negative coefficients.  This is helpful    % because these show up as bands when two channels are multiplied
    % together.
    detailImagesThresholded(detailImagesThresholded < 0) = 0;
end

prodImage = prod(detailImagesThresholded(:,:,detailImagesToUse),3);
outImage = prodImage > 0;