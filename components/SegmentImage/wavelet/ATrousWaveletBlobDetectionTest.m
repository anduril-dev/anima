% ATrousWaveletBlobDetectionTest
%
% Test for the ATrousWaveletBlobDetection
%
% Author: Dirk Padfield, GE Global Research, padfield@research.ge.com
%
% Reference: 
% Padfield, D., Rittscher, J., and Roysam, B., 
% "Coupled minimum-cost flow cell tracking for high-throughput quantitative analysis," 
% Medical Image Analysis 15(4), 650-668 (2011) 

inImage = imread('ATrousWaveletBlobDetectionTest.tif');
detailImagesToUse = 3:7;
segmentedImage = ATrousWaveletBlobDetection(inImage,detailImagesToUse);
figure; imagesc(inImage); colormap(gray);
hold on; contour(segmentedImage,[0,1],'r'); hold off;