This directory contains the segmentation code for the algorithm presented in the paper:

Padfield, D., Rittscher, J., and Roysam, B., Coupled minimum-cost flow cell tracking for high-throughput quantitative analysis," Medical Image Analysis 15(4), 650-668 (2011).

Additional processing is carried out in that reference for improved segmentation, but the code in this directory forms the core of the segmentation algorithm.

A test is provided as the file ATrousWaveletBlobDetectionTest.m to demonstrate how to run the code.  This file can be run directly from the Matlab command line.

For questions, please contact Dirk Padfield, GE Global Research, padfield@research.ge.com