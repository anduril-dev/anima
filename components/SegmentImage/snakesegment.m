function [masks,perims,idxs]=snakesegment(im,seeddir,alp,bet,gam,kap,wl,we,wt,iterations,mina,maxa)
% some good values for small objects
%alp=0.4; % tension
%bet=0.15; % rigidity
%gam=.5; % step size
%kap=0.45; % energy term
%wl=0.8; % weight line
%we=0.2; % weight edge
%wt=0.4; % weight terminal energy
%iterations=115; % ...

%im=mat2gray(filter_function(im,0.5));
%seed=[X Y alpha lx ly];  lx=radius, alpha=0

if maxa==0
    maxa=numel(im);
end
[height,width] = size(im);
seed=getimagelist(seeddir);
boundaries=cell([numel(seed),1]);
for s=1:numel(seed)
    B=bwboundaries(imread([seeddir filesep num2str(s) '.png']),4,'noholes');    
    boundaries{s}=B{1};   
end

masks=false([height width]);
perims=masks;
[imX,imY]=meshgrid(1:width,1:height);
warning('off','MATLAB:inpolygon:ModelingWorld');
idxs=cell(numel(boundaries),1);
for s=1:numel(boundaries)
    try
        [Xsnake,Ysnake]=snakeiterate(im,boundaries{s}(:,2),boundaries{s}(:,1),alp,bet,gam,kap,wl,we,wt,iterations);
        Ysnake=floor(Ysnake);
        Xsnake=floor(Xsnake);
        xl=min(Xsnake); xh=max(Xsnake);
        yl=min(Ysnake); yh=max(Ysnake);
        masksnake=inpolygon(imX(yl:yh,xl:xh),imY(yl:yh,xl:xh),(Xsnake),(Ysnake));
        area=sum(masksnake(:));
        if (area > mina) && (area < maxa)
            %imshow(PIXsnaked)
            [Ymask,Xmask]=find(masksnake);
            maskidx=sub2ind([height width],yl+Ymask-1,xl+Xmask-1);
            idxs{s}=maskidx;
            periidx=sub2ind([height width],Ysnake,Xsnake);
            masks(maskidx)=true;
            perims(periidx)=true;
        end
    catch cold
        cold.message
    end
    
end
idxs(cellfun(@isempty,idxs))=[];
%hold off;

%maskshow=zeros([height width]);
%for layer=1:numel(masks)
%    maskshow=maskshow+masks{layer}*0.25;
%end
%imshow(maskshow)

% mask=zeros(size(im));
% idx=sub2ind(size(im),floor(Ysegment(:)),floor(Xsegment(:)));
% idx=idx(~isnan(idx));
% mask(idx)=1;
% 
% imwrite(im+mask,['snake1' filesep imfile]);

%dlmwrite(['snake1' filesep imfile '.txt'], round([Xsegment Ysegment]), 'delimiter',char(9),'precision','%d');

% 
%imshow(im);
%hold on
%for r=1:size(Xbar,1)
%    plot(Xsegment(r,:),Ysegment(r,:),'b');
%end
%hold off;
% now 676 seconds
% 4475 seconds

