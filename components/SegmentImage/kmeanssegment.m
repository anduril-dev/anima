function [clusters,perim]=kmeanssegment(im,c,fillholes,clearborders,mina,maxa)

[h,w,colors]=size(im);
im=reshape(im, h*w, colors);

[clusters,centers]=kmeans(im,c,'Replicates',3,'Start','sample');

[sortedcents,sortidx]=sortrows(centers);
clusterssort=zeros(size(clusters));
for cent=1:c
    clusterssort(clusters==sortidx(cent))=cent;
end

clusters=reshape(clusterssort,[h w]);
clustermask=zeros([h w c-1]);
perim=false([h w]);
for cent=2:c
    mask=clusters==cent;
    mask=ridofwrongsize(mask,mina,maxa);

    if fillholes
       mask=imfill(mask,'holes'); 
    end
    if clearborders
        mask=imclearborder(mask);
    end
    clustermask(:,:,cent)=mask*(cent-1);
    perim=or(perim,bwperim(mask,4));
end
clusters=max(clustermask,[],3);
clusters=clusters-min(clusters(:));
clusters=clusters/max(clusters(:));
%clusters=mat2gray(clusters);%(clusters-1)/(c-1);

