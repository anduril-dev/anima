function mask=cellsegm(id, params)

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);

addpath([ folder '/cellSegm/mscripts' ]);
addpath([ folder '/cellSegm/mclasses' ]);
addpath([ folder '/cellSegm/mfunctions' ]);
addpath([ folder '/cellSegm/mex' ]);
addpath([ folder '/cellSegm/mex/maxflow' ]);

id_cal='/home/vhrantan/BTSync/SBL/projects/CellSegmPackage/calibration.xml';

config = CellXConfiguration.readXML(id_cal);
config.setIdPrecisionRate(0);

config.check();
fileSet = CellXFileSet(1, id);
seg = CellXSegmenter(config, fileSet);
seg.run();
segmentedCells = seg.getDetectedCells();
mask = zeros(size(seg.image));
% make sure objects are not overlapping. Anima handles 4-connectivity only
% for backwards compatibility.
eroder=strel('rectangle', [2 2]);
for nsc=1:numel( segmentedCells )
    object = zeros(size(seg.image));
    cellPixelInd = segmentedCells(nsc).cellPixelListLindx;
    object(cellPixelInd) = 1;
    object = imerode(object, eroder);
    mask = mask + object;
end







