function mask=correlationsegment(data1,data2,correction)
% mask=correlationsegment(data1,data2,corr)
% data is a gray scale image

q=96;
levels=flipud(unique(data1));
if numel(levels)>q
    levels=levels(round([1:q]*numel(levels)/q));
end
fit=polyfit(data1(:),data2(:),1)
corrlist=ones(numel(levels),1);
for leveliter=1:numel(levels)
    level1=levels(leveliter);
    level2=fit(1)*level1 + fit(2);
    mask1=data1<level1;
    mask2=data2<level2;
    combmask=and(mask1,mask2);
    corrR=corrcoef(data1(combmask),data2(combmask));
    try
        corrlist(leveliter)=abs(corrR(2));
    catch nocorr
        continue
    end
end
[zerocorr,zeroindex]=min(corrlist);
level1=min(max(levels(zeroindex)*correction,0),1)
level2=min(max(fit(1)*level1 + fit(2),0),1)
zerocorr
mask1=im2bw(data1,level1);
mask2=im2bw(data2,level2);
mask=and(mask1,mask2);
