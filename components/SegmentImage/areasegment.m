function mask=areasegment(data,ratio)
% bwout=example1(data,ratio)
% data is a gray scale image
% ratio is the 0-1 value that the mask foreground should cover.

%% force method is surely slower, but it gives absolutely the correct value. (fminbound doesnt)
% uses fminbnd to find the threshold to use.

data=mat2gray(data)*511;

fgratio=zeros(512,1);
for t=0:511
    mask=data>=t;
    fgratio(t+1)=sum(mask(:));
end
ratiodiff=abs((fgratio/numel(data)) - ratio);

[tval,tind]=min(ratiodiff);
mask=data>=tind-1;


%    testdata=data*;
%    thr_min=fminbnd(@(x) findratio(testdata,x,ratio),0,65535,optimset('TolX',1,'Display','off'));
%    mask=im2bw(data,thr_min/65535);
%
%end
%function ratiodiff=findratio(data,threshold,goalratio)
%    mask=data>=threshold;
%    fgratio=sum(mask(:))/numel(mask);
%    ratiodiff=abs(fgratio-goalratio);
%end