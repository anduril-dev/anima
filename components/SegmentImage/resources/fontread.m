%Helper function to extract the characters from the image.
% remember to add additional ' to the ' character... :)

im=mat2gray(im2double(imread('smallfont.png')));
[h w]=size(im);
ascii=32;
im=logical(im2bw(imcomplement(im)));
fid=fopen('fonttext.txt','w');
for y=1:6:h
for x=1:4:w
    c=im(y:y+5,x:x+3);
    %imshow(c);drawnow;pause(0.001)
    %disp(char(ascii));
    stringout=['case ''' char(ascii) ''', cout = ['];
    for cy=1:6
        stringout=[stringout num2str(c(cy,:)) '; '];
    end
    stringout=[stringout '];'];
    disp(stringout)
    fprintf(fid,'%s\n',stringout);
    ascii=ascii+1;
end
end
fclose(fid);


