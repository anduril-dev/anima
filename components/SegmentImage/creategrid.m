function mask=creategrid(im,n)
% Create a grid of nxn squares with a single pixel spacing

    pattern=padarray( ones(n,n), [1 1],0,'pre' );
    mask=padarray(pattern,size(im)-n-1,'circular','post' );
