
source "$ANDURIL_HOME/bash/functions.sh"

pipesrc=$( getinput pipeline )
location=$( getparameter path )
piperun=$( getoutput pipelineDebug )
tempdir=$( gettempdir )

for ((i=1; i<10; i++))
do inname="inf"${i}
   outname="outf"${i}
   parname="par"${i}
   declare $inname=$( getinput in${i} )
   declare $outname=$( getoutput out${i} )
   declare $parname=$( getparameter param${i} )
   mkdir "${!outname}"
done

CELLPROFILER=${location}/CellProfiler.py
if [ ! -f "$CELLPROFILER" ]
then
    msg="CellProfiler not found in: $CELLPROFILER"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi
cp "$pipesrc" "$tempdir"/pipeline.cp

sed -e 's,@param1@,'"$par1"',g' \
    -e 's,@param2@,'"$par2"',g' \
    -e 's,@param3@,'"$par3"',g' \
    -e 's,@param4@,'"$par4"',g' \
    -e 's,@param5@,'"$par5"',g' \
    -e 's,@param6@,'"$par6"',g' \
    -e 's,@param7@,'"$par7"',g' \
    -e 's,@param8@,'"$par8"',g' \
    -e 's,@param9@,'"$par9"',g' \
    -e 's,@in1@,'"$inf1"',g' \
    -e 's,@in2@,'"$inf2"',g' \
    -e 's,@in3@,'"$inf3"',g' \
    -e 's,@in4@,'"$inf4"',g' \
    -e 's,@in5@,'"$inf5"',g' \
    -e 's,@in6@,'"$inf6"',g' \
    -e 's,@in7@,'"$inf7"',g' \
    -e 's,@in8@,'"$inf8"',g' \
    -e 's,@in9@,'"$inf9"',g' \
    -e 's,@out1@,'"$outf1"',g' \
    -e 's,@out2@,'"$outf2"',g' \
    -e 's,@out3@,'"$outf3"',g' \
    -e 's,@out4@,'"$outf4"',g' \
    -e 's,@out5@,'"$outf5"',g' \
    -e 's,@out6@,'"$outf6"',g' \
    -e 's,@out7@,'"$outf7"',g' \
    -e 's,@out8@,'"$outf8"',g' \
    -e 's,@out9@,'"$outf9"',g' \
    "$tempdir"/pipeline.cp > "$piperun"
echo python $CELLPROFILER -b -c -r -i "$inf1" -o "$outf1" -p "$piperun"
python $CELLPROFILER -b -c -r -i "$inf1" -o "$outf1" -p "$piperun"
errcode=$?
#some heuristic error handling:  find which output folders are being used and check they contain files
for ((i=1; i<10; i++))
do outname="outf"${i}
   if ( grep "@out${i}@" "$tempdir"/pipeline.cp > /dev/null )
   then fc=$( ls "${!outname}" | wc --lines )
        echo "folder $i file count: $fc" >> $logfile
        echo $slsep >> $logfile
        if [ $fc -eq 0 ]
        then echo "NO files in output port folder"$i"!" >> $errorfile
             echo $slsep >> $errorfile
        fi
   fi
done
exit $errcode
