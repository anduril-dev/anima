
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
BFPATH=$( readlink -f ../../lib/bftools )
export PATH=$PATH:$BFPATH

cat ../../doc/logo/logo.ansi.color || true

# make the list separator a newline
IFS=$'\n' 
BF="showinf"

iscmd $BF || exit 1
echo -n "BFConvert: " >> "${logfile}"
"$BF" -version >> "${logfile}"

# list files
if [ -d "$input_in" ]
then infiles=( $( ls -1 "$input_in" | grep -i "$parameter_filter" ) ) || echo "error reading input dir" >> "$errorfile"
fi

# create directories
mkdir "${output_ome}" || writeerror "error creating directory ${output_ome}"
mkdir "${output_raw}" || writeerror "error creating directory ${output_raw}"

# loop over images
for (( f=0; f<${#infiles[@]}; f++ ))
    do writelog "$(( $f+1 )) / ${#infiles[@]} : ${infiles[$f]}"
    newname="${infiles[$f]%.*}".xml
    "$BF" -ascii -nopix -novalid -omexml-only "${input_in}/${infiles[$f]}" > "${output_ome}/${newname}"
    newname="${infiles[$f]%.*}".txt
    "$BF" -ascii -nopix "${input_in}/${infiles[$f]}" > "${output_raw}/${newname}"
done

#~ cd "${outdir}"
#~ [[ "$( ls -A )" ]] || {
    #~ writeerror "Output folder is empty"
    #~ exit 1
#~ }
