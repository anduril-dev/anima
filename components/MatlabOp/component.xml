<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>MatlabOp</name>
    <version>1.2</version>
    <doc>
      Executes a Matlab script that generates an image directory from given 
      image directories. You can either modify a single source of images or 
      combine up to five sources. The source directories must contain the same number
      of images. The source images can be referenced as <var>im1</var>
      and <var>im2</var> (etc.) in the script. The component expects the 
      resulting image to be in <var>imout</var> and/or <var>imout2</var>
      variables. If you want to generate images without sources, 
      specify the parameter <var>files</var>.
      
      If <var>tableout</var> variable is present, it will be written as a csv file <var>table</var>
      output port. Note the special struct format of the variable! (see testcases).
      <p>Example:
      <table><tr><th>Sources</th><th>Output</th></tr>
      <tr><td><img src="input1.jpg"/> <img src="input2.jpg"/><br/>
              Source script: "<var>imout</var>=xor(<var>im1</var>,<var>im2</var>);"</td>
          <td valign="top"><img src="output.jpg"/></td></tr>
      </table></p>
      
      Tips and examples in the <a href="http://code.google.com/p/anduril-imaging/wiki/component_ImageMatlabOperation" target="_BLANK">Wiki</a>.
    </doc>
    <author email="ville.rantanen@helsinki.fi">Ville Rantanen</author>
    <category>External</category>
    <category>Image Processing</category>
    <launcher type="Matlab">
        <argument name="file" value="execute.m" />
        <argument name="source" value="imageoperation.m" />        
    </launcher>
    <requires>Matlab</requires>
    <inputs>
        <input name="command" type="MatlabSource" optional="true">
            <doc>Matlab script file to operate the images with. The string parameter will override this input.</doc>
        </input>
        <input name="table" type="CSV" optional="true">
            <doc>Optional CSV file to read. Will appear as <var>table</var>.</doc>
        </input>
        <input name="in1" type="ImageList" optional="true">
            <doc>Source images 1 (in1). The output images will be named after this input folder, or if not defined, after the first array input folder.</doc>
        </input>
        <input name="in2" type="ImageList" optional="true">
            <doc>Source images 2 (in2)</doc>
        </input>
        <input name="in3" type="ImageList" optional="true">
            <doc>Source images 3 (in3)</doc>
        </input>
        <input name="in4" type="ImageList" optional="true">
            <doc>Source images 4 (in4)</doc>
        </input>
        <input name="in5" type="ImageList" optional="true">
            <doc>Source images 5 (in5)</doc>
        </input>
        <input name="inArray" type="ImageList" optional="true" array="true"><doc>An array of directories with image files. (ina1, ina2, ...)</doc> </input>
        <input name="file1" type="ImageList" optional="true">
            <doc>Source image 1 (file1). This folder should contain only one image. The same 
            image is used in each iteration of <var>dir1</var>.</doc>
        </input>
        <input name="file2" type="ImageList" optional="true">
            <doc>Source image 2 (file2). See input <var>file1</var>.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="out" type="ImageList">
            <doc>Result images (out)</doc>
        </output>
        <output name="out2" type="ImageList">
            <doc>Result images (out2)</doc>
        </output>
        <output name="tableOut" type="CSVList">
            <doc>Result tables (tableout)</doc>
        </output>
    </outputs>
      <parameters>
        <parameter name="script" type="string" default="">
            <doc>Matlab script string to operate the images with. This parameter overrides the input script file.</doc>
        </parameter>
        <parameter name="param1" type="string" default="">
            <doc>Value of this parameter will be assigned to param1 variable</doc>
        </parameter>
        <parameter name="param2" type="string" default="">
            <doc>Value of this parameter will be assigned to param2 variable</doc>
        </parameter>
        <parameter name="param3" type="string" default="">
            <doc>Value of this parameter will be assigned to param3 variable</doc>
        </parameter>
        <parameter name="param4" type="string" default="">
            <doc>Value of this parameter will be assigned to param4 variable</doc>
        </parameter>
        <parameter name="param5" type="string" default="">
            <doc>Value of this parameter will be assigned to param5 variable</doc>
        </parameter>
        <parameter name="param6" type="string" default="">
            <doc>Value of this parameter will be assigned to param6 variable</doc>
        </parameter>
        <parameter name="param7" type="string" default="">
            <doc>Value of this parameter will be assigned to param7 variable</doc>
        </parameter>
        <parameter name="param8" type="string" default="">
            <doc>Value of this parameter will be assigned to param8 variable</doc>
        </parameter>
        <parameter name="param9" type="string" default="">
            <doc>Value of this parameter will be assigned to param9 variable</doc>
        </parameter>
        <parameter name="param10" type="string" default="">
            <doc>Value of this parameter will be assigned to param10 variable</doc>
        </parameter>
        <parameter name="files" type="float" default="0">
            <doc>If there are no input folders, this number is used as the number of files generated.
            The resulting files will be named file1[extension], file2[extension], ... Use the variable <var>file</var> in the script
            to get the file number. Remember to use <var>extension</var> when using this parameter.</doc>
        </parameter>
        <parameter name="extension" type="string" default="">
            <doc>String to be added after the output filenames. e.g. ".png" to change the image format</doc>
        </parameter>
    </parameters>
</component>


