library(componentSkeleton)
library(EBImage)
library(CRImage)

execute <- function(cf) {
    method     <- tolower(get.parameter(cf,'method',type="string", allowed.values=c("Otsu","Phansalkar")))
    minShape   <- get.parameter(cf,'minArea',type="int")
    maxShape   <- get.parameter(cf,'maxArea',type="int")
    failureRegion  <- get.parameter(cf,'failureRegion',type="int")
    numWindows <- get.parameter(cf,'numWindows',type="int")
    
    image.list <- dir(get.input(cf,'in'))
    mask.folder <- get.output(cf,'mask')
    perim.folder <- get.output(cf,'perimeter')
    table.output <- get.output(cf,'features')
    dir.create(perim.folder)
    dir.create(mask.folder)
    
    tmp<-get.temp.dir(cf)
    setwd(tmp)
    
    i<-1
    features.all<-matrix(ncol=43)
    for ( base in image.list ) {
        print( paste( base, i, '/',length(image.list)) )
        i<-i+1
        filename<- paste(get.input(cf,'in'),base,sep="/")
        if (grepl("png$",base, ignore.case=T)) { 
            base.out<-base
        } else {
            base.out<-paste(base,'png',sep='.')
        }
        values <- tryCatch({ 
              segmentImage(
                filename=filename,
                maxShape=maxShape,
                minShape=minShape,
                failureRegion=failureRegion,
                threshold=method,
                numWindows=numWindows)
                }, error=function(e) { return(NA) } )
        if (is.na(values)) {
            tmp_img <- readImage(filename)[,,1]*0
            writeImage( tmp_img, paste(perim.folder,base.out,sep="/"), 'png')
            writeImage( tmp_img, paste(mask.folder,base.out,sep="/"), 'png')
            next
        }
        if (nrow(values[[3]])==0) {
            writeImage( values[[2]], paste(perim.folder,base.out,sep="/"), 'png')
            writeImage( values[[2]], paste(mask.folder,base.out,sep="/"), 'png')
        } else { 
            plain.mask<-values[[2]]
            plain.mask[ plain.mask>0 ] <- 1
            perim<-paintObjects(values[[2]], Image(0,dim(values[[2]]),'Gray'),col="#ffffff")
            reduced.mask <- plain.mask - perim
            
            writeImage( perim, paste(perim.folder,base.out,sep="/"), 'png')
            writeImage( reduced.mask, paste(mask.folder,base.out,sep="/"), 'png')
            
            features<-cbind(paste(base.out,values[[3]][,1],sep="_"),base.out, values[[3]])
            colnames(features)[1]<-"RowId"
            colnames(features)[2]<-"File"
           features.all<-rbind(features.all, features)
        }
        if (file.exists("imgB.jpg")) { file.remove("imgB.jpg") }
    }
    rownames(features.all)<-c()
    features.all<-features.all[-1,]
    CSV.write(table.output, features.all)
    return(0)
}

main(execute)
