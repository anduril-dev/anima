from component_skeleton.main import main
import fijijython
import sys,os,csv
from ij import IJ
from ij.process import ImageProcessor
from ij.process import ImageStatistics as IS
from ij.plugin.frame import RoiManager
from ij.measure import ResultsTable
from ij.plugin.filter import ParticleAnalyzer
from ij.plugin.filter import Analyzer
from ij.measure import Measurements as M
from java.lang import Double
import ij.Prefs

measurements = M.AREA | M.AREA_FRACTION | M.CENTER_OF_MASS | M.CENTROID | M.CIRCULARITY | M.ELLIPSE | M.FERET | M.INTEGRATED_DENSITY | M.KURTOSIS | M.MAX_STANDARDS | M.MEAN | M.MEDIAN | M.MIN_MAX | M.MODE | M.PERIMETER | M.RECT | M.SKEWNESS | M.STD_DEV
ij.Prefs.blackBackground = True

def execute(cf):
    # Read parameters etc
    minArea = cf.get_parameter('minArea', 'int')
    minCirc = cf.get_parameter('minCirc', 'float')
    fourConnected = cf.get_parameter('fourConnected', 'boolean')
    doInvert=cf.get_parameter('invert', 'boolean')
    
    dirout = cf.get_tempdir()
    csvout= cf.get_output('out')
    
    # Get listings of images in the input directories
    images = cf.get_input('images')
    mask = cf.get_input('mask')
    l_images=fijijython.get_imagelist(images)
    l_mask=fijijython.get_imagelist(mask)
    if len(l_images) is not len(l_mask):
        raise Exception("Input folders do not have the same number of images")
        
    # Iterate over the all the image files in the directory listings 
    for i, (img, msk),in enumerate(zip(l_images, l_mask)):
        # read image files
        imgI = IJ.openImage(os.path.join(images, img))
        mskI = IJ.openImage(os.path.join(mask, msk))
        print("%s %d/%d"% (img, i+1,len(l_images)))
        # run the script supplied by user
        # Make sure mask is a thresholded image
        IJ.run(mskI, "8-bit", "");
        IJ.setThreshold(mskI,127, 255)
        IJ.run(mskI, "Convert to Mask", "")
        if doInvert:
            IJ.run(mskI, "Invert", "")

        ip = mskI.getProcessor()
        options = IS.MIN_MAX  
        stats = IS.getStatistics(ip, options, mskI.getCalibration())  

        if stats.max>0.0:

            # Add segments from mask image to ROI Manager
            options = ParticleAnalyzer.ADD_TO_MANAGER
            if fourConnected:
                options |= ParticleAnalyzer.FOUR_CONNECTED
            pa = ParticleAnalyzer(options, 0, None, minArea, Double.POSITIVE_INFINITY, minCirc, 1.0)
            pa.analyze(mskI)

            # Collect measurements from input image into output table
            roim = RoiManager.getInstance()
            rtable = ResultsTable()
            analyzer = Analyzer(imgI, measurements, rtable)
            for roi in roim.getRoisAsArray():
              imgI.setRoi(roi)
              stats = imgI.getStatistics(measurements)
              analyzer.saveResults(stats, roi)

            filename = os.path.join(dirout, img)
            rtable.saveAs(filename + ".txt") # .txt extension gives tab separated fields
            roim.runCommand("Delete")

            # Initialize headers
            reader = csv.reader(open(filename + ".txt",'rb'),delimiter='\t')
            for row in reader:
                break
            header=[]
            header.extend(['File','RowId','Object'])
            header.extend(row)
            header.remove(' ')
            # open reader and writer    
            read_file=open(filename + ".txt",'rb')
            write_file=open(filename + ".csv",'wb')
            reader = csv.DictReader(read_file,delimiter='\t')    
            writer = csv.DictWriter(write_file,header,dialect="excel-tab")
            writer.writerow(dict(zip(header,header)))
            unique_pos=[]
            object_no=0
            for row in reader:
                # remove sometimes occurring duplicates (when fourconnected)
                new_pos='x'.join([ row['X'], row['Y'], row['Area'] ])
                if new_pos in unique_pos:
                    continue
                unique_pos.append(new_pos)
                object_no+=1
                row['File']=img
                row['RowId']=img + '_%d' % object_no
                row['Object']=str(object_no)
                del row[' ']
                writer.writerow(row)
            read_file.close()
            write_file.close()
            os.remove(filename+ ".txt")

    # After finishing with each image, combine csv
    first_csv=True
    csv_writer=csv.writer(open(csvout,'wb'),dialect="excel-tab")
    for c in sorted(os.listdir(dirout)):
        if not c.endswith(".csv"):
            continue
        reader=csv.reader(open(os.path.join(dirout,c),'rb'),dialect="excel-tab")
        if not first_csv:
            reader.next()
        csv_writer.writerows(reader)
        first_csv=False
            
    return 0

main(execute)

