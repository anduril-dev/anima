
source "$ANDURIL_HOME/lang/bash/functions.sh"
BFPATH=$( readlink -f ../../lib/bftools )
export PATH=$PATH:$BFPATH

echo '         Welcome to Anima
       ▀██████░███░▄          
        ▀██████░███░██████▄         
         ███████░██░█████░░▄       
          ███████░░░███░░████      
          ▀█████████░█░██████      
           ██████████░███████      
           ███████░░█░████▀        
           ▀████░░████░██▀         
            ▀██░███████░▀▄         
                 ▀▀▀▀   ▀█         
                          ▀▄       
                            █      
         ▄▀▄  █▄ █ █ █▄ ▄█  ▄▀▄   ▄▀   
        █ ▀▀█ █ ▀█ █ █ ▀ █ █▀▀ █ █   
                                █  
                              ▄▀    
                             █     
                             █     
                            ▀  
' 

# make the list separator a newline
IFS=$'\n' 

indir=$( getinput in )
outdir=$( getoutput out )

BF="bfconvert"

switches=$( getparameter switches ) 
infilter=$( getparameter filter )
namestring=$( getparameter name )
toPNG=$( getparameter png )
forceGray=$( getparameter gray )
multiplier=$( getparameter multiplier )
iscmd $BF || exit 1
echo -n "BFConvert: " >> "${logfile}"
"$BF" -version >> "${logfile}"
convert -version >> "${logfile}"
# list files
if [ -d "$indir" ]
then infiles=( $( ls -1 "$indir" | grep -i "$infilter" ) ) || echo "error reading input dir" >> "$errorfile"
fi

# create directories
mkdir "${outdir}" || writeerror "error creating directory ${outdir}"

# loop over images
for (( f=0; f<${#infiles[@]}; f++ ))
    do writelog "$(( $f+1 )) / ${#infiles[@]} : ${infiles[$f]}"
    newbase="${infiles[$f]%.*}"
    newname=$( echo $namestring | sed -e "s,%o,${newbase},g" -e "s,$,.tif," )
    eval "$BF" -nooverwrite $switches \"${indir}/${infiles[$f]}\" \"${outdir}\"/\"${newname}\" 2>&1 | iconv -f utf-8 -f ascii -c | tee -a "${logfile}"
done

cd "${outdir}"
[[ "$( ls -A )" ]] || {
    writeerror "Output folder is empty"
    exit 1
}
if [ $forceGray = "true" ]
then writelog "Forcing grayscale"
     iscmd "convert" || exit 1
     for f in $( ls )
     do convert "$f" -verbose -separate \
          -background black -compose plus -flatten "$f".miff 2>&1 | tee -a "${logfile}"
        convert "$f".miff "$f" 2>&1 >> "${logfile}"
        rm "$f".miff
     done
fi

if [ $( echo "$multiplier==1.0" | bc ) -eq 0 ]
then writelog "Multiplying"
     iscmd "mogrify" || exit 1
     for f in $( ls )
     do mogrify -verbose -evaluate multiply $multiplier "$f" 2>&1 | tee -a "${logfile}"
     done
fi

# convert to PNG if requested
if [ $toPNG = "true" ]
then writelog "Converting to PNG"
    cd "${outdir}"
    iscmd "convert" || exit 1
    for f in $( ls )
    do convert "$f" -verbose "${f%tif}png" 2>&1 | tee -a "${logfile}"
       [[ -e "${f%tif}png" ]] || {
           writeerror "could not convert file $f"
           exit 1
       }
       rm "$f"
    done
fi
