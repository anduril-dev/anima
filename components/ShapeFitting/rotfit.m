function [data, response, direction, scale]=rotfit(gray,shape,scalemin,scalesteps,rotsteps,symmetry);

steps=scalesteps*rotsteps;
[h w] = size(gray);
scale_space = zeros(h,w,steps); % [h,w] - dimensions of image, steps - number of levels in scale space
data=zeros(1,4);
if rotsteps==1
    angles=0;
else
    angles=linspace(0,symmetry,rotsteps+1);
end
if scalesteps==1
    scales=1;
else
    scales=linspace(1,scalemin,scalesteps);
end
shapesum=sum(shape(:));
%scalemap=zeros([rotsteps scalesteps]);
oldstep=100;
for s = 1:scalesteps
    shapec=imresize(shape,scales(s),'bicubic');
for r = 1:rotsteps
    newstep=round(100*(1 - ((s-1)*rotsteps+r)/steps));
    if newstep~=oldstep
        disp(['s' num2str(s) ':r' num2str(r) '   ' num2str(newstep) '%'])
    end
    oldstep=newstep;
    shapea=imrotate(shapec,angles(r),'bicubic','loose');
    shapea=shapesum*shapea/sum(shapea(:));
    tmp = imfilter(gray,shapea,'replicate','same');
    scale_space(:,:,r+(s-1)*rotsteps) = tmp;
%    scalemap(r,s)=r+(s-1)*rotsteps;
end
end
[max_response, max_index]=max(scale_space,[],3);
data(1,1)=min(max_response(:));
data(1,2)=max(max_response(:));
response=mat2gray(max_response);
data(1,3)=0;
data(1,4)=symmetry;
direction=(1+mod(max_index-1,rotsteps))/rotsteps;
scale=scales(ceil(max_index/rotsteps));

clear scale_space;
