function imagesegment(cf)
%
% cf = Anduril command file

imdir=getinput(cf,'images');
shapedir=getinput(cf,'shape');
responsedir=getoutput(cf,'location');
directiondir=getoutput(cf,'orientation');
scaledir=getoutput(cf,'scale');
mkdir(responsedir);
mkdir(scaledir);
mkdir(directiondir);
outcsv=getoutput(cf,'limits');
imorigdir=getinput(cf,'names');

rotsteps=getparameter(cf,'rotationSteps','float');
symmetry=getparameter(cf,'rotationSymmetry','float');
scalesteps=getparameter(cf,'scaleSteps','float');
scalemin=getparameter(cf,'scaleMin','float');

tic
imglist=getimagelist(imdir);
if numel(imorigdir)<2
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end
shapelist=getimagelist(shapedir);
shape=im2double(imread([shapedir filesep shapelist{1}]));

writeout.columnheads={'File' 'Min response' 'Max response' 'Min angle' 'Max angle'};

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    gray=im2double(imread(id));
    if ndims(gray)==2
        if strcmpi('.png',imglist{file}(end-3:end))
            ext='';
        else
            ext='.png';
        end
        [data,response, direction, scale]=rotfit(gray,shape,scalemin,scalesteps,rotsteps,symmetry);
        imwrite(response, [responsedir filesep imglist{file} ext]);
        imwrite(direction, [directiondir filesep imglist{file} ext]);
        imwrite(scale, [scaledir filesep imglist{file} ext]);
        writeout.data=[imglist(file) num2csvcell(data)];
        appendcsv(outcsv,writeout);
    else
        disp([ imglist{file} ' image is not gray scale!'])
    end
end
toc
end
