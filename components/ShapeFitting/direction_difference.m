function d=direction_difference(im,steps,ci,fun)
% calculates directional difference values from a NxM or 1xN array. ci points the
% index of "center" point which is used to calculate local difference
% steps indicates where the angle is rotated "over", e.g. 360. 
% fun is the value calculated from the differences, e.g. fun=@mean

imd=abs(im-im(ci));
imd2=min(imd,steps-imd);
imd2(ci)=[];
d=fun(imd2);




