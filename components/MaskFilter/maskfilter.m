function maskfilter(cf)
% cf = Anduril command file

tic
maskdir=getinput(cf,'inMask');
origdir=getinput(cf,'names');
objectfile=getinput(cf,'objects');

outdir=getoutput(cf,'mask');
outperim=getoutput(cf,'perimeter');

filecolname=getparameter(cf,'fileId','string');
objectcolname=getparameter(cf,'objectId','string');

masklist=getimagelist(maskdir);
if strcmp(origdir,'')
    origlist=masklist;
    origdir=maskdir;
else
    origlist=getexoticimagelist(origdir);
end

if numel(masklist)==0
    writeerror(cf,'No images in source directory')
    return
end
if numel(masklist)~=numel(origlist)
    writeerror(cf,'Source directories have different number of images')
    return
end

mkdir(outdir);
mkdir(outperim);

objectdata=readcsvcell(objectfile,0,char(9));

files=getcellcol(objectdata,filecolname,'string');
objects=getcellcol(objectdata,objectcolname,'float');

for file=1:numel(masklist)
    idorig=origlist{file};
    idmask=[maskdir filesep masklist{file}];
    mask=(im2double(imread(idmask)));
    if ndims(mask)>2
        writeerror(cf,'Mask image dimensions exceed 2 (not BW image)');
    end
    disp(['Read image: ' masklist{file} ' (' num2str(file) '/' num2str(numel(masklist)) ')'])
    filerows=strcmp(files, idorig);
    if sum(filerows)==0
        writelog(cf,['File not found in list: ' idorig]);
    end
    outmask=logical(ismember(bwlabel(mask,4),objects(filerows)));
    imwrite(outmask,[outdir filesep masklist{file}]);
    imwrite(logical(bwperim(outmask,4)),[outperim filesep masklist{file}]);
end
toc
end

