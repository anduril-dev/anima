library(componentSkeleton)
library(alphahull)

execute <- function(cf){
    output.file <- get.output(cf,'out')

    dcols<-get.parameter(cf, 'cCols')
    lcol<-get.parameter(cf, 'lCol')
    alpha<-get.parameter(cf,'alpha','float')
    data <- CSV.read(get.input(cf, 'in'))
    dcolumns <- split.trim(dcols, ',')
    if (identical(dcolumns, '*')) dcolumns <- colnames(data)
    
    uniqs <- sort(t(unique(data[,lcol,drop=FALSE])))

    header<-cbind('RowId',lcol,'Edge',t(paste(dcolumns,'1',sep='')),t(paste(dcolumns,'2',sep='')))
    outdata<-matrix(ncol=7,nrow=0)
    colnames(outdata)<-header
    for (u in 1:length(uniqs) ) {
        timenow<-as.integer(format(Sys.time(),'%s'))
        datas<-data[which(data[,lcol]==uniqs[u]),dcolumns,drop=FALSE]
        obj<-ahull(datas,alpha=alpha)
        objects<-1:nrow(obj$ashape.obj$edges)
        edges<-obj$ashape.obj$edge
        outdata<-rbind(outdata,cbind(paste(uniqs[u],objects,sep='_'),uniqs[u],objects,edges[,3:6,drop=FALSE]))
        timespent<-as.integer(format(Sys.time(),'%s'))-timenow
        print(paste(uniqs[u],timespent,'s'))
    }
    CSV.write(output.file, outdata)
}


main(execute)
