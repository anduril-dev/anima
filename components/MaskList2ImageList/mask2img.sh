source "$ANDURIL_HOME/bash/functions.sh"
export_command

mkdir "${output_out}"

IFS=$'\n'
for dir in $( ls "${input_in}" ); 
do echo "$dir"
   for file in $( ls "${input_in}"/"${dir}" ) 
   do i=$( echo $file | tr -d -c [:digit:] )
      cp "${input_masklist}"/"${dir}"/"${file}" "${output_out}"/"${i}_${dir}"
   done
done

