function imagesummary(cf)
%
% cf = Anduril command file
tic
imdir=getinput(cf,'gray');
maskdir=getinput(cf,'mask');
imorigdir=getinput(cf,'names');
if numel(maskdir)<2
    withmask=0;
else
    withmask=1;
end

outfile=getoutput(cf,'out');

percentile=getparameter(cf,'percentile','float');
colprefix=getparameter(cf,'prefix','string');

imglist=getimagelist(imdir);
if withmask
    masklist=getimagelist(maskdir);
end
if numel(imorigdir)<2
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Intensity image and name list directories have different number of images')
    return
end
if withmask
    if numel(imglist)~=numel(masklist)
        writeerror(cf,'Intensity and mask image directories have different number of images')
        return
    end
end

writeout.columnheads={'File','Format','FileSize','Bitdepth','Width','Height', 'Area','Centroid X','Centroid Y'};
if withmask
    writeout.columnheads=[writeout.columnheads {'FG mean','FG median','FG stDev','FG sum','FG min','FG max',...
    sprintf('FG %g percentile',percentile),sprintf('FG %g percentile',100-percentile), 'FG objects', 'FG area'}];
    writeout.columnheads=[writeout.columnheads {'BG mean','BG median','BG stDev','BG sum','BG min','BG max',...
    sprintf('BG %g percentile',percentile),sprintf('BG %g percentile',100-percentile), 'BG objects', 'BG area','FG/total area'}];
else
    writeout.columnheads=[writeout.columnheads {'Mean','Median','StDev','Sum','Min','Max',...
    sprintf('%g percentile',percentile),sprintf('%g percentile',100-percentile)}];
end
writeout.columnheads=cellfun(@(x) [colprefix x], writeout.columnheads,'UniformOutput',false);
for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    gray=im2double(imread(id));
    
    if size(gray,3)~=1
        writeerror(cf,['Source gray file not gray scale: ' id])
        return
    end
    dataout=cell(1,numel(writeout.columnheads));
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    metadata=imfinfo(id);
    c=1;
    dataout{c}=['"' origimglist{file} '"']; c=c+1;
    dataout{c}=['"' metadata.Format '"']; c=c+1;
    dataout{c}=sprintf('%g',metadata.FileSize); c=c+1;
    dataout{c}=sprintf('%g',metadata.BitDepth); c=c+1;
    dataout{c}=sprintf('%g',metadata.Width); c=c+1;
    dataout{c}=sprintf('%g',metadata.Height); c=c+1;
    dataout{c}=sprintf('%g',metadata.Height*metadata.Width); c=c+1; % Area
    %centroid x 
    [Xcoords,Ycoords]=meshgrid(1:metadata.Width, 1:metadata.Height);
    dataout{c}=sprintf('%g',sum(sum(gray.*Xcoords))/sum(gray(:))); c=c+1;
    %centroid y
    dataout{c}=sprintf('%g',sum(sum(gray.*Ycoords))/sum(gray(:))); c=c+1;
    
    if withmask==0
        dataout{c}=sprintf('%g',mean(gray(:))); c=c+1;
        dataout{c}=sprintf('%g',median(gray(:))); c=c+1;
        dataout{c}=sprintf('%g',std(gray(:))); c=c+1;
        dataout{c}=sprintf('%g',sum(gray(:))); c=c+1;
        dataout{c}=sprintf('%g',min(gray(:))); c=c+1;
        dataout{c}=sprintf('%g',max(gray(:))); c=c+1;
        dataout{c}=sprintf('%g',prctile(gray(:),percentile)); c=c+1;
        dataout{c}=sprintf('%g',prctile(gray(:),100-percentile)); c=c+1;
     
    else
        idmask=[maskdir filesep masklist{file}];
        mask=im2double(imread(idmask));
        dataout{c}=sprintf('%g',mean(gray(mask==1))); c=c+1; 
        dataout{c}=sprintf('%g',median(gray(mask==1))); c=c+1;
        dataout{c}=sprintf('%g',std(gray(mask==1))); c=c+1;
        dataout{c}=sprintf('%g',sum(gray(mask==1))); c=c+1;
        dataout{c}=sprintf('%g',min(gray(mask==1))); c=c+1;
        dataout{c}=sprintf('%g',max(gray(mask==1))); c=c+1;
        dataout{c}=sprintf('%g',prctile(gray(mask==1),percentile)); c=c+1;
        dataout{c}=sprintf('%g',prctile(gray(mask==1),100-percentile)); c=c+1;
        labels=bwlabel(mask,4);
        dataout{c}=sprintf('%g',max(labels(:))); c=c+1; % Mask objects
        dataout{c}=sprintf('%g',sum(sum(mask==1))); c=c+1; % Mask Area
        
        dataout{c}=sprintf('%g',mean(gray(mask==0))); c=c+1;
        dataout{c}=sprintf('%g',median(gray(mask==0))); c=c+1;
        dataout{c}=sprintf('%g',std(gray(mask==0))); c=c+1;
        dataout{c}=sprintf('%g',sum(gray(mask==0))); c=c+1;
        dataout{c}=sprintf('%g',min(gray(mask==0))); c=c+1;
        dataout{c}=sprintf('%g',max(gray(mask==0))); c=c+1;
        dataout{c}=sprintf('%g',prctile(gray(mask==0),percentile)); c=c+1;
        dataout{c}=sprintf('%g',prctile(gray(mask==0),100-percentile)); c=c+1;
        labels=bwlabel(imcomplement(mask),4);
        dataout{c}=sprintf('%g',max(labels(:))); c=c+1;% Mask objects
        dataout{c}=sprintf('%g',sum(sum(mask==0))); c=c+1;% non-Mask Area
        dataout{c}=sprintf('%g',sum(sum(mask==1))/numel(mask)); % Mask Area/total area

        
    end
    writeout.data=dataout;
    appendcsv(outfile,writeout)
    
end
close all
toc
end
