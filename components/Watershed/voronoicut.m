function [mask,lines]=voronoicut(gray,seed)

props=regionprops(bwlabel(seed,4),'Centroid');
centers=reshape([props.Centroid],[2,numel(props)])';
lines=false(size(seed));
try
    [vx,vy]=voronoi(centers(:,1),centers(:,2));
    [h,w]=size(lines);
    vx=floor(vx);
    vy=floor(vy);
    for vert=1:size(vx,2)
        % pixels needed to draw the line
        pixels=3*max(abs(vx(1,vert)-vx(2,vert)),abs(vy(1,vert)-vy(2,vert)));
        pix_x=floor(linspace(vx(1,vert),vx(2,vert),pixels));
        pix_y=floor(linspace(vy(1,vert),vy(2,vert),pixels));
        pix_x=min(w,pix_x); pix_x=max(1,pix_x);
        pix_y=min(h,pix_y); pix_y=max(1,pix_y);
        ind=sub2ind([h w],pix_y,pix_x);
        lines(ind)=true;
    end
catch
    
end
lines=bwmorph(lines,'thin',inf);
mask=imcomplement(lines).*gray;
