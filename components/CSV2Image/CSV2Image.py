#!/usr/bin/python
import sys
import anduril
from anduril.args import *
#import component_skeleton.main
import os
import scipy.misc
import numpy as n
import csv
import anduril.imagetools as imgtool
#import component_skeleton.ImageTools as imgtool
import re
from PIL import Image

def hsv2rgb(arr):
    """HSV to RGB color space conversion.

    References
    [0] http://www.nullege.com/codes/show/src@scikits.image-0.2.2@scikits@image@color@colorconv.py/299/numpy.swapaxes
    [1] http://en.wikipedia.org/wiki/HSL_and_HSV

    """
    hi = n.floor(arr[:,:,0] * 6)
    f = arr[:,:,0] * 6 - hi
    p = arr[:,:,2] * (1 - arr[:,:,1])
    q = arr[:,:,2] * (1 - f * arr[:,:,1])
    t = arr[:,:,2] * (1 - (1 - f) * arr[:,:,1])
    v = arr[:,:,2]

    hi = n.dstack([hi, hi, hi]).astype(n.uint8) % 6
    out = n.choose(hi, [n.dstack((v, t, p)),
                         n.dstack((q, v, p)),
                         n.dstack((p, v, t)),
                         n.dstack((p, q, v)),
                         n.dstack((t, p, v)),
                         n.dstack((v, p, q))])
    return out

def check_names(name, checklist):
    if (name not in checklist):
            msg="Column \""+name+"\" not found."
            print(msg)
            write_error(msg)
            sys.exit(1)

def pixel_draw(inFile, fileName, imin, imax, height, width, colored, xCol,yCol,fCol,vCol, toAlpha):

    values=[]
    coords=[]
    
    reader = csv.DictReader(open(inFile,'rb'),delimiter='\t')
    for row in reader:
        if row.get(fCol)==fileName or fileName=="":
            if vCol!='':
                if row.get(vCol)!='NA':
                    values.append( float(row.get(vCol)) )
                    coords.append((int(float(row.get(xCol))),int(float(row.get(yCol)))))
            else:
                values.append( float(1) )
                coords.append((int(float(row.get(xCol))),int(float(row.get(yCol)))))

    coords=n.asarray(coords,dtype=n.uint16)
    values=n.asarray(values,dtype=n.float)
    # Normalization to 1-65535, nonexisting will be alpha=0
    if vCol!='':
        if (imin==imax):
            values=values-values.min()
            values=(1+(65534*values/values.max()))
        else:
            values=values-imin
            values=values/imax
            values[values>1]=1
            values[values<0]=0
            values=1+values*65534
    else:
        values=1+values*65534
    
    # In case of empty CSV file, try to create a black image of given dimensions
    if coords.shape[0] is not 0:
        if height==0:
            height=coords[:,1].max()+1
            inY=n.ones(coords.shape[0])
        else:
            inY=coords[:,1]<height
        if width==0:
            width=coords[:,0].max()+1
            inX=n.ones(coords.shape[0])
        else:
            inX=coords[:,0]<width

    counter=n.zeros( (height,width) )
    im=n.zeros( (height,width) )
    alpha=n.zeros( (height,width) )

    for c in range(coords.shape[0]):
        x=coords[c,0]
        y=coords[c,1]
        
        if (inX[c] and inY[c]):
            v=values[c]
            im[y,x]=im[y,x]+v
            alpha[y,x]=65535
            # change alpha to visible if point used.
            counter[y,x]=counter[y,x]+1

    im[counter>0]=im[counter>0]/counter[counter>0]
    if toAlpha:
        ima=n.zeros( (height,width,2) )
        ima[:,:,0]=im
        ima[:,:,1]=alpha
        ima=n.nan_to_num(ima).astype('uint16')
    else:
        ima=n.nan_to_num(im).astype('uint16')

    if param_color:
        imhsv=n.ones( (height,width,3)).astype('float')
        imhsv[:,:,0]=im.copy().astype('float')/65535
        # values not preset are colored black. (and transparent)
        imhsv[:,:,2]=n.where((alpha==65535),1,0)
        if toAlpha:
            ima=n.zeros((height,width,4)).astype('uint16')
            ima[:,:,3]=alpha
            ima[:,:,0:3]=(hsv2rgb(imhsv)*65535).astype('uint16')
        else:
            ima=(hsv2rgb(imhsv)*65535).astype('uint16')
    
    return (ima,  counter)

def line_draw(inFile, fileName, imin, imax, height, width, colored, xCol,yCol,fCol,vCol, toAlpha):
    values=[]
    coords=[]
    reader = csv.DictReader(open(inFile,'rb'),delimiter='\t')
    for row in reader:
        if row.get(fCol)==fileName:
            if vCol!='':
                values.append( float(row.get(vCol)) )
            else:
                values.append( float(1) )
            
            coords.append((float(row.get(xCol[0])),
                           float(row.get(xCol[1])),
                           float(row.get(yCol[0])),
                           float(row.get(yCol[1]))
                         ))

    coords=n.asarray(coords,dtype=n.float)
    values=n.asarray(values,dtype=n.float)
    # Normalization to 1-65535, nonexisting will be alpha=0
    if vCol!='':
        if (imin==imax):
            values=values-values.min()
            values=(1+(65534*values/values.max()))
        else:
            values=values-imin
            values=values/imax
            values[values>1]=1
            values[values<0]=0
            values=1+values*65534
    else:
        values=1+values*65534
    
    # In case of empty CSV file, try to create a black image of given dimensions
    if coords.shape[0] is not 0:
        if height==0:
            height=max( coords[:,2].max()+1, coords[:,3].max()+1 )
            inY=n.ones(coords.shape[0])
        else:
            inY=[ 1 for (y1,y2) in zip( coords[:,2], coords[:,3]) if y1<height and y2<height ]
        if width==0:
            width=max( coords[:,0].max()+1, coords[:,1].max()+1 )
            inX=n.ones(coords.shape[0])
        else:
            inX=[ 1 for (x1,x2) in zip( coords[:,0], coords[:,1]) if x1<width and x2<width ]

    counter=n.zeros( (height,width) )
    im=n.zeros( (height,width) )
    alpha=n.zeros( (height,width) )

    for c in range(coords.shape[0]):
        x1=coords[c,0]
        x2=coords[c,1]
        y1=coords[c,2]
        y2=coords[c,3]
        
        if (inX[c] and inY[c]):
            v=values[c]
            dx=abs( x1 - x2 )
            dy=abs( y1 - y2 )
            nPix=max(dx,dy)+1
            lPix=( [ int(round(x)) for x in n.linspace( x1,x2,nPix ) ],
                   [ int(round(y)) for y in n.linspace( y1,y2,nPix ) ] )
            for pix in zip(*lPix):
                im[pix[1],pix[0]]=im[pix[1],pix[0]]+v
                alpha[pix[1],pix[0]]=65535
                # change alpha to visible if point used.
                counter[pix[1],pix[0]]=counter[pix[1],pix[0]]+1
    
    im[counter>0]=im[counter>0]/counter[counter>0]
    if toAlpha:
        ima=n.zeros( (height,width,2) )
        ima[:,:,0]=im
        ima[:,:,1]=alpha
        ima=n.nan_to_num(ima).astype('uint16')
    else:
        ima=n.nan_to_num(im).astype('uint16')

    if param_color:
        imhsv=n.ones( (height,width,3)).astype('float')
        imhsv[:,:,0]=im.copy().astype('float')/65535
        imhsv[:,:,2]=n.where((alpha==65535),1,0)
        # values not preset are colored black. (and transparent)
        if toAlpha:
            ima=n.zeros((height,width,4)).astype('uint16')
            ima[:,:,3]=alpha
            ima[:,:,0:3]=(hsv2rgb(imhsv)*65535).astype('uint16')
        else:
            ima=(hsv2rgb(imhsv)*65535).astype('uint16')
    
    return (ima,counter)
    

inFile = input_in
imin = iMin
imax = iMax
#vCol_name = re.sub('[^-a-zA-Z0-9_.() ]+', '_', vCol)
#outFile = re.sub('%v', vCol_name, fileName)
xCol = [x.strip() for x in xCol.split(",")]
yCol = [y.strip() for y in yCol.split(",")]
fCol = fCol.strip()

outDir = output_out
freqDir = output_frequency
os.mkdir(outDir)
os.mkdir(freqDir)
# read CSV file
reader = csv.DictReader(open(inFile,'rb'),delimiter='\t')
for row in reader:
    break
# ^ make sure fieldnames are initialized...

if vCol != "":
    check_names(vCol, reader.fieldnames)
for xName in xCol:
    check_names(xName, reader.fieldnames)
for yName in yCol:
    check_names(yName, reader.fieldnames)
if fCol != "":
    check_names(fCol, reader.fieldnames)
    uniqFiles=[]
    reader = csv.DictReader(open(inFile,'rb'),delimiter='\t')
    for row in reader:
        if row[fCol] not in uniqFiles:
            uniqFiles.append( row[fCol] )
else:
    write_log( "fCol empty:  Using static image name." )
    uniqFiles=[""]

for i,fileName in enumerate(uniqFiles):
    write_log( "%s %d/%d"% (fileName, i+1, len(uniqFiles)) )
    height=param_height
    width=param_width
    if height==0 and width==0 and input_reference!=None:
        if os.path.isfile( os.path.join(input_reference, fileName ) ):
            ref_im= Image.open(os.path.join(input_reference, fileName ))
            (width, height)=ref_im.size
    if param_method.lower() == "pixel":
        (ima, counter) = pixel_draw(inFile, fileName, imin, imax, height, width, color, xCol[0],yCol[0],fCol,vCol, alpha)
    else:
        (ima, counter) =  line_draw(inFile, fileName, imin, imax, height, width, color, xCol   ,yCol   ,fCol,vCol, alpha)
    if fileName=="":
        fileName=re.sub('[^-a-zA-Z0-9_.() ]+', '_', vCol)+".png"
    imgtool.save_png(os.path.join(outDir,fileName),
                ima,grayscale=(not color),bitdepth=16,alpha=alpha)

    imgtool.save_png(os.path.join(freqDir,fileName),
                counter.astype('uint8'),grayscale=True,bitdepth=8,alpha=False)

