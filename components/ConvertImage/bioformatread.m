function bioformatread(cf)
%
% cf = Anduril command file

%javaaddpath(regexprep(mfilename('fullpath'),'bioformatread$','loci_tools.jar'));
imdir=getinput(cf,'in');
outdir=getoutput(cf,'out');
outarray=getoutput(cf,'outArray'); arrayout=createarray();
outfile=getoutput(cf,'list');
givenbitdepth=getparameter(cf,'depth','float');
postfix=getparameter(cf,'group','string');
allchannels=getparameter(cf,'allChannels','boolean');
extractmetadata=getparameter(cf,'metadata','boolean');
ch=getparameter(cf,'ch','float');
series=getparameter(cf,'series','float');
%namestring='@ONAME@.@OEXT@_S@S@_C@C@_@GROUP@.png'
namestring=getparameter(cf,'nameString','string');

if numel(postfix)==0
    postfix='""';
end

mkdir(outdir);
tic
imglist=getexoticimagelist(imdir);
idlist=cellfun(@(x) [imdir filesep x],imglist,'UniformOutput',false);
arraystart=numel(imglist);
if inputdefined(cf,'inArray')
    arrayinput=readarray(cf,'inArray');
    imglist=[imglist; getcellcol(arrayinput,'Key')];
    idlist=[idlist; getcellcol(arrayinput,'File')];
end
disp(['found ' num2str(numel(imglist)) ' images'])


csvout.columnheads={'Original','File','Ch','Series','Group'};
csvout.data=cell(0);
metadata.columnheads=cell(0);
metadata.data=cell(0);
filepad=num2str(numel(num2str(numel(imglist))));
for file=1:numel(imglist)
    filenr=num2str(file,['%0' filepad 'd']);
    csvfiledata=cell(0);
    %id=[imdir filesep imglist{file}];
    id=idlist{file};
    [oname,oext]=namesplit(imglist{file});

    if sum(strcmpi(id((end-2):end), {'zvi','lsm','lif','sld','c01'}))>0
        imagetype=0; % exotic file
    elseif sum(strcmpi(id((end-3):end), {'.tif','tiff'}))>0
        imagetype=2; % tiff file
    elseif sum(strcmpi(id((end-3):end), {'.gif'}))>0
        imagetype=3; % gif file
    else % all the other image formats
        imagetype=1; % other file
    end
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    if series==-1
        allchannels=true; % incase allchannels=false but all series are still wanted
    end
    if imagetype~=0
        imageinfo=imfinfo(id);
    end
    if allchannels
        if imagetype==0
            serieslist=(1:getexoticseries(id));
            seriespad=num2str(numel(num2str(getexoticseries(id))));
        elseif or(imagetype==2,imagetype==3)
            serieslist=(1:numel(imageinfo));
            seriespad=num2str(numel(num2str(numel(imageinfo))));
        else % all the other image formats
            serieslist=1;
            seriespad='1';
        end
    else
       serieslist=series;
       seriespad='1';
    end
    if series==-1
        allchannels=false;
    end
    for seriesnow=serieslist
        %loopseries=['_S' num2str(seriesnow,['%0' seriespad 'd'])];
        loopseries=num2str(seriesnow,['%0' seriespad 'd']);
        if ch==-1
            allchannels=true; % in case allchannels=false but all channels are still wanted
        end
        if allchannels
            if imagetype==0
                channellist=(1:getexoticimages(id,seriesnow));
            elseif imagetype==2
                channellist=1:(imageinfo(seriesnow).SamplesPerPixel);
            elseif imagetype==3
                channellist=1:3;
            else % all the other image formats
                channellist=1:(imageinfo.SamplesPerPixel);
            end
            channelpad=num2str(numel(num2str(numel(channellist))));
        else
            channellist=ch;
            channelpad='1';
        end
        if ch==-1
            allchannels=false; % in case allchannels=false but all channels are still wanted
        end
        for chnow=channellist
            meta=cell(0);
            %loopch=['_C' num2str(chnow,['%0' channelpad 'd'])];
            loopch=num2str(chnow,['%0' channelpad 'd']);
            if imagetype==0
                [result,meta]=exoticread(id,chnow,seriesnow,givenbitdepth);
            elseif imagetype==2 % TIFF
                result=im2double(imread(id,seriesnow));
                result=result(:,:,chnow);
            elseif imagetype==3 % GIF
                [result,cmapping]=imread(id,seriesnow);
                result=ind2rgb(result,cmapping);
                result=result(:,:,chnow);
            else % all the other image formats
                result=im2double(imread(id));
                result=result(:,:,chnow);
            end
            writefilename=namereplace(namestring, oname,oext,loopseries,loopch,postfix,filenr);
            if sum(strcmpi(writefilename((end-3):end), {'.png'}))>0
                imwrite(result,[outdir filesep writefilename],'bitdepth',16);
            else
                imwrite(result,[outdir filesep writefilename]);
            end            
            csvfiledata=[csvfiledata;{imglist{file}, writefilename,num2str(chnow),num2str(seriesnow)},postfix;];
            arrayout.data=[arrayout.data; {writefilename, [outdir filesep writefilename]}];
            if extractmetadata
                metadata=metadatareorder(metadata,meta);
            end
        end
    end
    
    csvout.data=[csvout.data;csvfiledata];
end
if extractmetadata
    csvout.columnheads=[csvout.columnheads,metadata.columnheads];
    csvout.data=[csvout.data,metadata.data];
end

appendcsv(outfile,csvout);
writearray(cf,'outArray',arrayout);
toc
end


function [base,ext]=namesplit(fullpath)
%% return the file name in two parts: base and extension
    pidx=findstr(fullpath,filesep);
    if numel(pidx)==0
        % fullpath was not a full path
        basename=fullpath;
    else
        basename=fullpath(pidx(end)+1:end);
    end
    eidx=findstr(basename,'.');
    if numel(eidx)==0
        % file has no extension
        ext='';
        base=basename;
    else
        ext=basename(eidx(end)+1:end);
        base=basename(1:eidx(end)-1);
    end
end

function replaced=namereplace(format, oname,oext,s,c,pf,n)
%% returns the file name string with tags replaced
    replaced=strrep(format,'@ONAME@',oname);
    replaced=strrep(replaced,'@OEXT@',oext);
    replaced=strrep(replaced,'@S@',s);
    replaced=strrep(replaced,'@C@',c);
    replaced=strrep(replaced,'@GROUP@',pf);
    replaced=strrep(replaced,'@N@',n);
end

function data=metadatareorder(data,newdata)
%% order data rows in the same order as a previous head
    if numel(newdata)==0
        % no new data provided
        return
    end
    if numel(data.columnheads)==0
        % No old header provided, probably the first run in a loop.
        data.columnheads=newdata(:,1)';
        data.data=newdata(:,2)';
        return
    end
    cols=size(data.data,2);
    data.data=[data.data; repmat({'NA'},[1 cols])];
    rows=size(data.data,1);
    for i=1:numel(newdata(:,1))
        index=find(strcmp(newdata(i,1), data.columnheads));
        if numel(index)==0
            cols=1+cols;
            data.columnheads=[data.columnheads newdata(i,1)];
            data.data=[data.data, repmat({'NA'}, [row-1 1])];
            index=cols;
        end
        data.data(rows,index)=newdata(i,2);
    end
end
