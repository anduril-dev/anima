function imagelocalmax(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file

imdir=getinput(cf,'in');
imorigdir=getinput(cf,'names');

outdir=getoutput(cf,'mask');
outfile=getoutput(cf,'centers');
band=round(getparameter(cf,'lowpass','float'));
lowint=getparameter(cf,'minInt','float');
peaksize=round(getparameter(cf,'maximaSize','float'));
cntsize=round(getparameter(cf,'centerSize','float'));

if and(band > 0,mod(band,2)==0)
    band=band+1;
end
if mod(peaksize,2)==0
    peaksize=peaksize+1;
end
if mod(cntsize,2)==0
    cntsize=cntsize+1;
end

mkdir(outdir);
imglist=getimagelist(imdir);
if numel(imorigdir)<2
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end
if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Source directories have different number of images')
    return
end
csvout.columnheads={'RowId','File','Object','X','Y','Brightness','Gyration radius2'};
for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    gray=im2double(imread(id));
    if size(gray,3)~=1
        writeerror(cf,'Source file not gray scale')
        return
    end
    mask=false(size(gray));
    if band==0
        bp=gray;
    else
        bp=bpass(gray,1,band);
    end
    pk=pkfnd(bp,lowint,peaksize);
    cnt=cntrd(bp,pk,cntsize);
    objs=[];
    if size(cnt,1)>0
        spots=floor([cnt(:,2) cnt(:,1)]);
        spots(spots==0)=1;
    
        inds=sub2ind(size(mask),spots(:,1), spots(:,2));
        mask(inds)=1;
        objs=(1:size(cnt,1))';
    end
        
    imwrite(mask,[outdir filesep imglist{file}])
    
    cntout=[objs cnt];
    % Rescale intensities
    if size(cntout,1)>0
        cntout(:,4)=cntout(:,4)/255;
    end
    cntoutname=repmat({origimglist{file}},[size(cnt,1) 1]);
    if numel(cnt)>0
        cntout=num2csvcell(cntout);
        cntoutname=[strcat('"',cntoutname,'_',cntout(:,1),'"') strcat('"',cntoutname,'"')]; 
        csvout.data=[cntoutname cntout];
    else
        csvout.data={};
    end
    appendcsv(outfile, csvout);
end

end
